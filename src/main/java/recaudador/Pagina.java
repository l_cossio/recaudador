package recaudador;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.Random;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Selenide.$;

public abstract class Pagina {

    public void seleccionarOpcionPorTexto(WebElement element, String seleccion) {
        Select vlr_Elemento = new Select(element);
        vlr_Elemento.selectByVisibleText(seleccion);
    }

    public void seleccionarOpcionPorTextoParcial(WebElement elemento, String textoParcial) {
        elemento.click();
        elemento.sendKeys(textoParcial);
        elemento.sendKeys(Keys.ENTER);
    }

    public void seleccionarOpcionPorIndice(WebElement element, int indice){
        Select vlr_Elemento = new Select(element);
        vlr_Elemento.selectByIndex(indice);
    }

    public void seleccionarOpcionAleatoria(WebElement element){
        Select select = new Select(element);
        int cantidadDeOpciones = select.getOptions().size();
        Random random = new Random();

        do {
            int seleccion = random.nextInt(cantidadDeOpciones);
            select.selectByIndex(seleccion);
            select = new Select(element);
        } while (select.getFirstSelectedOption().getText().isEmpty() ||
                 select.getFirstSelectedOption().getText().equals(""));
    }

    /**
     * Elige una opcion aleatoria del lista-valor
     * @param element debe ser un lista-valor
     */
    public void seleccionarOpcionAleatoria(By element){
        Select select = new Select($(element));
        int cantidadDeOpciones = select.getOptions().size();

        // Hay valores con texto?
        if (cantidadDeOpciones == 1 && !$(element).getAttribute("title").matches("[^\\s]*")) {
            // no selecciona nada
            return;
        }

        Random random = new Random();

        do {
            int seleccion = random.nextInt(cantidadDeOpciones);
            $(element).selectOption(seleccion);
        } while (selectionIsEmpty(element));
    }

    /*
    !$(element).getAttribute("title").matches("[^\\s]*") ||
                  $(element).has(not(attribute("title")))
    */

    private boolean selectionIsEmpty(By element) {
        String text = $(element).getSelectedText();
        return text.isEmpty() || text.equals("Seleccione un valor");
    }


}
