package recaudador;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Properties;
import java.util.Random;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;

public class Base {

    public WebDriver driver;
    public String url;
    public String userName;
    public String password;
    public String userRecoveryPass;
    public String answerRecoveryPass;
    public String userRecoveryPassIN;
    public String answerRecoveryPassIN;
    public String env;

    public WebDriver initializerDriver() throws IOException {

        Properties prop = new Properties();

        env = getSystemProperty("env", "TAB");

        FileInputStream fis = new FileInputStream("src\\test\\dataBank\\properties\\Base." + env +".properties");

        prop.load(fis);

        userName = prop.getProperty("userName");
        password = prop.getProperty("password");
        url = prop.getProperty("url");
        userRecoveryPass = prop.getProperty("userRecoveryPass");
        answerRecoveryPass = prop.getProperty("answerRecoveryPass");
        userRecoveryPassIN = prop.getProperty("userRecoveryPassIN");
        answerRecoveryPassIN = prop.getProperty("answerRecoveryPassIN");

        String browser = getSystemProperty("browser", "firefox");

        switch (browser) {
            case "chrome":

                System.setProperty("webdriver.chrome.driver", "src\\test\\dataBank\\drivers\\chromedriver.exe");
                driver = new ChromeDriver();

                break;
            case "firefox":

                System.setProperty("webdriver.gecko.driver", "src\\test\\dataBank\\drivers\\geckodriver.exe");
                System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE,"true");
                System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE,"/dev/null");
                driver = new FirefoxDriver();

                break;
            case "internetExplorer":

                System.setProperty("webdriver.ie.driver", "src\\test\\dataBank\\drivers\\IEDriverServer.exe");
                driver = new InternetExplorerDriver();

                break;
            case "edge":

                System.setProperty("webdriver.edge.driver", "src\\test\\dataBank\\drivers\\edgedriver.exe");
                driver = new EdgeDriver();

                break;
            case "opera":

                System.setProperty("webdriver.opera.driver", "src\\test\\dataBank\\drivers\\operadriver.exe");
                driver = new OperaDriver();

                break;
        }

        driver.manage().window().maximize();

        return driver;

    }

    protected String getSystemProperty(String propertyName, String defaultValue) {
        String property;
        if (System.getProperty(propertyName) == null) {
            property = defaultValue;
        } else property = System.getProperty(propertyName);
        return property;
    }

    /**
     *  Retorna un registro aleatorio de un archivo CSV, considerando que el archivo no tiene título,
     *  y puede o no usar comillas como delimitadores
     * @param filename
     * @return
     * @throws IOException
     */
    public static String[] getRegistroAleatorioDeCSV(String filename) throws IOException {
        return splitCsvLine(getRandomRow(filename));
    }
    private static String[] splitCsvLine(String line){
        return line.split(
                "(?x)   " + // Ignore whitespaces on this regex for readability
                        ",          " +   // Split on comma
                        "(?=        " +   // Followed by
                        "  (?:      " +   // Start a non-capture group
                        "    [^\"]* " +   // 0 or more non-quote characters
                        "    \"     " +   // 1 quote
                        "    [^\"]* " +   // 0 or more non-quote characters
                        "    \"     " +   // 1 quote
                        "  )*       " +   // 0 or more repetition of non-capture group (multiple of 2 quotes will be even)
                        "  [^\"]*   " +   // Finally 0 or more non-quotes
                        "  $        " +   // Till the end  (This is necessary, else every comma will satisfy the condition)
                        ")          "     // End look-ahead
        );
    }
    public static String getRandomRow(String filename) throws IOException {
        // Accesar a una posición aleatoria en el archivo
        RandomAccessFile raf = new RandomAccessFile(filename, "r");
        Random random = new Random();
        long length = raf.length();
        long offset = random.longs(1, 0, length).toArray()[0];
        raf.seek(offset);

        // Buscar el  primer salto de línea hacia atrás o hasta llegar al inicio
        char ch;
        do{
            if (offset == 0) {
                break;
            }

            ch = (char) raf.readByte();
            if (ch == '\n') {
                break;
            }

            raf.seek(--offset);
        } while (ch != '\n');

        // retornar línea
        String linea = raf.readLine();
        raf.close();
        return linea;
    }

    public void infiniteLoop() {
        while (true) {
            System.out.println("..");
        }
    }
}
