package fusor;

import com.github.javafaker.Faker;

import java.util.Locale;

/**
 * Version: 1.0
 * Nombre: Catastro
 * Descripción: Sección de {@code Fusor} para la creación de claves catastrales
 * Autor: Luis Cossío Ramírez
 * Fecha de creacion: 12/01/2021.
 * Modificado por: N/A
 * Fecha de modificación: N/A
 */

public class Catastro {

    private Fusor fusor;
    private Faker faker;

    public String claveCatastralExistente;
    public String claveCatastralCompleta;

    /** Arreglo de 10 strings que contienen cada uno de los segmentos de la clave*/
    public String[] claveCatastralSegmentada;

    /**
     * Crea un objeto Catastro y genera al mismo tiempo datos aleatorios para sus atributos.
     * @param fusor El fusor que utilizará para la generación de datos
     */
    protected Catastro(Fusor fusor) {
        this.fusor = fusor;
        this.faker = new Faker(new Locale(fusor.path + "/catastro"), fusor.seed);
        claveCatastralExistente = genClaveCatastralExistente();
        claveCatastralCompleta = genClaveCatastralCompleta();
        claveCatastralSegmentada = claveCatastralCompleta.split("-");
    }

    private String genClaveCatastralExistente() {
        return faker.resolve("clave.clave_completa_existente");
    }
    private String genClaveCatastralCompleta() {
        return faker.numerify(faker.resolve("clave.clave_completa"));
    }
}
