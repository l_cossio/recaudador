package fusor;

import com.github.javafaker.Faker;
import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class PersonaFisica {

    private final Fusor fusor;
    private final Faker faker;

    public Date birthdate;
    public Date deathdate;

    public String nombre;
    public String primerApellido;
    public String segundoApellido;
    public String tercerApellido;
    public String nombreCompleto;
    public String sexo;
    public String estadoCivil;
    public String fechaNacimiento;
    public String fechaDefuncion;
    public String rfcAlfa;
    public String rfcFecha;
    public String rfcHomo;
    public String rfc;
    public String curp;
    public String estatus;
    public String email;

    protected PersonaFisica(Fusor fusor) {

        this.fusor = fusor;
        this.faker = new Faker(new Locale("fusor/default/persona"), fusor.seed);

        nombre = genNombre();
        primerApellido = genApellido();
        segundoApellido = genApellido();
        tercerApellido = genTercerApellido(0.3);
        nombreCompleto = genNombreCompleto();
        sexo = genSexo();
        estadoCivil = genEstadoCivil();
        fechaNacimiento = genFechaNacimiento();
        rfcAlfa = genRfcAlfa();
        rfcFecha = genRfcFecha();
        rfcHomo = genRfcHomo();
        rfc = genRfc();
        curp = genCurp();
        estatus = genEstatus();
        email = genEmail();
    }

    /**
     * @return un nombre aleatorio
     */
    private String genNombre(){
        return faker.resolve("persona_fisica.nombre");
    }

    /**
     * @return un nombre aleatorio
     */
    private String genApellido(){
        return faker.resolve("persona_fisica.apellido");
    }

    /**
     * Genera un tercer apellido
     * @param chance [0,1) es la probabilidad de que sí salga un tercer apellido
     * @return un apellido o un String vacío
     */
    private String genTercerApellido(double chance) {
        double randomNumber = faker.number().randomDouble(7,0,1);
        if (randomNumber < chance) {
            return genApellido();
        }
        return "";
    }

    /**
     * @return la concatenación de nombres y apellidos
     */
    private String genNombreCompleto() {
        return String.join(" ", nombre, primerApellido, segundoApellido, tercerApellido);
    }

    /**
     * @return sexo como M o F
     */
    private String genSexo(){
        return faker.resolve("persona_fisica.sexo");
    }

    /**
     * @return estado civil como N, C, S, o V
     */
    private String genEstadoCivil(){
        return faker.resolve("persona_fisica.estado_civil");
    }

    /**
     * Genera la fecha de nacimiento y la guarda en la variable de clase {@code birthdate}
     * @return la fecha de nacimiento con formato dd/mm/aa
     */
    private String genFechaNacimiento(){
        String pattern = "dd/MM/yy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

        birthdate = faker.date().birthday(18,80);

        return simpleDateFormat.format(birthdate);
    }

    private String genFechaDefuncion(){
        String pattern = "dd/MM/yy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

        deathdate = faker.date().between(birthdate, new Date());

        return simpleDateFormat.format(deathdate);
    }

    /**
     * @return Las primeras 4 letras del RFC
     */
    private String genRfcAlfa(){

        // Primeros dos caracteres del primer apellido
        String alpha = StringUtils.stripAccents(primerApellido.substring(0,2));

        // Primer caracter del segundo apellido
        alpha += StringUtils.stripAccents(segundoApellido.substring(0,1));

        // Primer caracter del primer nombre
        alpha += StringUtils.stripAccents(nombre.substring(0,1));

        return alpha.toUpperCase();
    }

    /**
     * @return Los dígitos que representan a la fecha en el RFC
     */
    private String genRfcFecha(){
        String pattern = "yyMMdd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

        return simpleDateFormat.format(birthdate);
    }

    /**
     * @return Los dígitos que representan a la homoclave en el RFC
     */
    private String genRfcHomo(){
        return faker.regexify("[0-9A-Z]{3}");
    }
    private String genRfc(){
        // Concatena las secciones individuales
        return rfcAlfa + rfcFecha + rfcHomo;
    }

    /**
     * @return Una curp utilizando los datos generados para el RFC
     */
    private String genCurp(){
        // Tomar la primera parte de lo que ya tenemos del RFC
        String curp = rfcAlfa + rfcFecha;

        // Obtener lo restante de regexify (aleatorio)
        String remainingPattern = "[HM][A-Z]{2}[A-Z]{3}[0-9][0-9]";
        curp += faker.regexify(remainingPattern);

        return curp;
    }

    /**
     * @return estatus activo o inactivo
     */
    private String genEstatus(){
        return faker.resolve("persona_fisica.estatus");
    }

    private String genEmail() {
        return String.format("%s.%s%s@%s",
                StringUtils.stripAccents(nombre.substring(0,1)),
                StringUtils.stripAccents(primerApellido),
                faker.numerify("##"),
                faker.resolve("internet.free_email")
        ).toLowerCase(Locale.ROOT);
    }
}

// GAGA810522MCBSICV1

// AYSF751227HTEIIE53
// BEGR970716HCHSMS06