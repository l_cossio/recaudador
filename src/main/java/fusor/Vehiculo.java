package fusor;

import com.github.javafaker.Faker;

import java.util.Locale;

public class Vehiculo {

    private final Fusor fusor;
    private final Faker faker;

    public String matricula;
    public String serie;

    protected Vehiculo(Fusor fusor) {
        this.fusor = fusor;
        this.faker = new Faker(new Locale(fusor.path + "/vehiculo"), fusor.seed);

        matricula = genMatricula();
        serie = genSerie();
    }

    private String genMatricula() {
        return faker.bothify(faker.resolve("placa.matricula")).toUpperCase();
    }

    private String genSerie() {
        return faker.regexify(faker.resolve("placa.serie")).toUpperCase();
    }

}
