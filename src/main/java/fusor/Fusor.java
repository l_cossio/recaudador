package fusor;

import java.util.Random;

/**
 * Version: 1.0
 * Nombre: Fusor
 * Descripción: Herramienta para crear datos falsos y usarlos para pruebas
 * Autor: Luis Cossío Ramírez
 * Fecha de creación: 12/01/2021.
 * Modificado por: N/A
 * Fecha de modificación: N/A
 */

public class Fusor {

    protected final Random seed;

    protected String path;

    // Constructores

    public Fusor() {
        this("default", new Random());
    }

    public Fusor(String cliente) {
        this(cliente, new Random());
    }

    public Fusor(Random seed) {
        this("default", seed);
    }

    public Fusor(String cliente, Random seed) {
        this.path = "fusor/" + cliente.toLowerCase();
        this.seed = seed;
    }

    // Modulos disponibles

    public Catastro catastro(){
        try {
            return new Catastro(this);
        } catch (RuntimeException ignored) {
            return new Catastro(new Fusor());
        }
    }

    public PersonaFisica personaFisica() {
        return new PersonaFisica(this);
    }

    public PersonaMoral personaMoral() {
        return new PersonaMoral(this);
    }

    public Direccion direccion() {
        try {
            return new Direccion(this);
        } catch (RuntimeException ignored) {
            return new Direccion(new Fusor());
        }
    }

    public Vehiculo vehiculo() {
        try {
            return new Vehiculo(this);
        } catch (RuntimeException ignored) {
            return new Vehiculo(new Fusor());
        }
    }
}
