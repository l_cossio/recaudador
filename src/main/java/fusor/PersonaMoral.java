package fusor;

import com.github.javafaker.Faker;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Version: 1.0
 * Nombre: Persona Moral
 * Descripción: Herramienta para crear datos de prueba para personas morales
 * Autor: Luis Cossío Ramírez
 * Fecha de creación: 12/01/2021.
 * Modificado por: N/A
 * Fecha de modificación: N/A
 */

public class PersonaMoral {

    private final Faker faker;
    private final Fusor fusor;

    private Date fechaCreacion;
    private Date fechaBaja;
    public String razonSocial;
    public String nombreComercial;
    public String rfcAlfa;
    public String rfcFecha;
    public String rfcHomo;
    public String rfc;
    public String fechaDeBaja;
    public String representanteLegal;
    public String estatus;

    protected PersonaMoral(Fusor fusor) {

        this.fusor = fusor;
        this.faker = new Faker(new Locale("fusor/default/persona"), fusor.seed);

        nombreComercial = genNombreComercial();
        razonSocial = genRazonSocial();
        rfcAlfa = genRfcAlfa();
        rfcFecha = genRfcFecha();
        rfcHomo = genRfcHomo();
        rfc = genRfc();
        fechaDeBaja = genFechaDeBaja();
        representanteLegal = genRepresentanteLegal();
        estatus = genEstatus();
    }

    // Generadores de datos

    /**
     * @return un nombre del dataset de personas
     */
    private String genNombreComercial() {
        return faker.resolve("persona_moral.nombre_comercial");
    }

    /**
     * @return una razón social del dataset de personas
     */
    private String genRazonSocial() {
        return nombreComercial + faker.resolve("persona_moral.sufijo");
    }

    /**
     * @return las primeras tres letras del RFC
     */
    private String genRfcAlfa() {
        return faker.letterify("???").toUpperCase();
    }

    /**
     * Crea y guarda un {@code Date} con la fecha de creación, a una antigüedad entre 0 y 200 años
     * @return la fecha en formato aammdd
     */
    private String genRfcFecha() {
        fechaCreacion = faker.date().birthday(0, 200);

        String pattern = "yyMMdd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

        return simpleDateFormat.format(fechaCreacion);
    }

    /**
     * @return los tres caracteres alfanuméricos de la homoclave
     */
    private String genRfcHomo() {
        return faker.regexify("[0-9A-Z]{3}");
    }

    /**
     * @return el rfc completo
     */
    private String genRfc() {
        // Concatena las secciones individuales
        return rfcAlfa + rfcFecha + rfcHomo;
    }

    /** Genera la fecha de baja
     * @return la fecha de baja, en el pasado, pero después de la fecha de creación
     */
    private String genFechaDeBaja() {

        fechaBaja = faker.date().between(fechaCreacion, new Date());

        String pattern = "yyMMdd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

        return simpleDateFormat.format(fechaBaja);
    }

    /**
     * @return un nombre completo aleatorio
     */
    private String genRepresentanteLegal() {
        return faker.resolve("persona_fisica.nombre_completo");
    }

    /**
     * @return estatus activo o inactivo
     */
    private String genEstatus() {
        return faker.resolve("persona_moral.estatus");
    }
}
