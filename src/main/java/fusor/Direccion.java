package fusor;

import com.github.javafaker.Faker;

import java.util.Locale;

public class Direccion {

    private Faker faker;

    public String pais;
    public String estado;
    public String municipio;
    public String localidad;
    public String codigoPostal;
    public String tipoAsentamiento;
    public String colonia;
    public String tipoCalle;
    public String calle;
    public String numeroExterior;
    public String letra;
    public String numeroInterior;
    public String extensionNumeroInterior;
    public String descripcionUbicacion;
    public String jmas;
    public String cfe;
    public String domicilio;

    /**
     * Crea un objeto Dirección y genera al mismo tiempo datos aleatorios para sus atributos.
     * @param fusor El fusor que utilizará para la generación de datos
     */
    protected Direccion(Fusor fusor) {

        // Preparar el Faker a utilizar
        String path = fusor.path + "/direccion";
        faker = new Faker(new Locale(path));

        // Generar los valores
        pais = genPais();
        estado = genEstado();
        municipio = genMunicipio();
        localidad = genLocalidad();
        codigoPostal = genCodigoPostal();
        tipoAsentamiento = genTipoAsentamiento();
        colonia = genColonia();
        tipoCalle = genTipoCalle();
        calle = genCalle();
        numeroExterior = genNumeroExterior();
        letra = genLetra();
        numeroInterior = genNumeroInterior();
        extensionNumeroInterior = genExtensionNumeroInterior();
        descripcionUbicacion = genDescripcionUbicacion();
        jmas = genJmas();
        cfe = genCfe();
        domicilio = genDomicilio();
    }


    /**
     * @return un pais del dataset correspondiente
     */
    private String genPais(){
        return faker.resolve("direccion.pais");
    }

    /**
     * @return un estado del dataset correspondiente
     */
    private String genEstado(){
        return faker.resolve("direccion.estado");
    }

    /**
     * @return un municipio del dataset correspondiente
     */
    private String genMunicipio(){
        return faker.resolve("direccion.municipio");
    }

    /**
     * @return una localidad del dataset correspondiente
     */
    private String genLocalidad(){
        return faker.resolve("direccion.localidad");
    }

    /**
     * @return un código postal del dataset correspondiente
     */
    private String genCodigoPostal(){
        return faker.numerify(faker.resolve("direccion.codigo_postal"));
    }

    /**
     * @return un tipo de asentamiento del dataset correspondiente
     */
    private String genTipoAsentamiento(){
        return faker.resolve("direccion.tipo_asentamiento");
    }

    /**
     * @return una colonia del dataset correspondiente
     */
    private String genColonia(){
        return faker.resolve("direccion.colonia");
    }

    /**
     * @return un tipo de calle del dataset correspondiente
     */
    private String genTipoCalle(){
        return faker.resolve("direccion.tipo_calle");
    }

    /**
     * @return una calle del dataset correspondiente
     */
    private String genCalle(){
        return faker.resolve("direccion.calle");
    }

    /**
     * @return un numero exterior del dataset correspondiente
     */
    private String genNumeroExterior(){
        return faker.numerify(faker.resolve("direccion.numero_exterior"));
    }

    /**
     * @return una letra del dataset correspondiente
     */
    private String genLetra(){
        return faker.regexify(faker.resolve("direccion.letra"));
    }

    /**
     * @return un numero interior del dataset correspondiente
     */
    private String genNumeroInterior(){
        return faker.letterify(faker.numerify(faker.resolve("direccion.numero_interior"))).toUpperCase();
    }

    /**
     * @return un una extensión del numero interior, del dataset correspondiente
     */
    private String genExtensionNumeroInterior(){
        return faker.regexify(faker.resolve("direccion.extension_numero_interior"));
    }

    /**
     * @return una descripción con fuzz del dataset correspondiente
     */
    private String genDescripcionUbicacion(){
        return faker.regexify(faker.resolve("direccion.descripcion_ubicacion"));
    }

    /**
     * @return una clave de medidor de agua del dataset correspondiente
     */
    private String genJmas(){
        return faker.numerify(faker.resolve("direccion.jmas"));
    }

    /**
     * @return una clave del medidor de electricidad del dataset correspondiente
     */
    private String genCfe(){
        return faker.regexify(faker.resolve("direccion.cfe"));
    }

    /**
     * @return un domicilio completo con calle, numero exterior y colonia
     */
    private String genDomicilio() {
        return String.format("%s %s, %s",calle, numeroExterior, colonia);
    }

}

