package recaudador.INCT1_catastro;

import com.github.javafaker.Faker;
import fusor.Catastro;
import fusor.Direccion;
import fusor.Fusor;
import junit.framework.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;
import recaudador.INAA1_administracionAplicacion.pageObjects.AltaPorNuevaDuplicidad;
import recaudador.INLO1_login.pageObjects.Login;
import recaudador.INLO1_login.pageObjects.Menu;
import recaudador.INCT1_catastro.PageObjects.AltaDePredio;
import recaudador.INCT1_catastro.PageObjects.DatosEntradaPadronCatastro;
import recaudador.INCT1_catastro.PageObjects.PadronCatastro;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/** Version: 1.0
 *  Nombre: Prueba de claves catastrales
 *  Descripción:
 *  Objetivos:
 *   - Entrar al portal
 *   - Entrar a Alta de Predios
 *   - Probar diferentes claves hasta que salga una válida
 *   - Pasar a Alta por nueva duplicidad
 *   - Llenar los campos de Ubicación
 *  Autor: Luis Cossío Ramírez
 *  Fecha de creación: 20/01/2021.
 *  Modificado por: N/A
 *  Fecha de modificación: N/A
 */

@Test(groups = "DGO")
public class TestCatastro extends DatosEntradaPadronCatastro {
    public WebDriver driver;
    Login objLogin;
    Menu objMenu;
    private PadronCatastro objPadronCatastro;
    private AltaDePredio objAltaDePredio;
    private WebDriverWait w;
    private Fusor fusor;
    private Faker faker;
    private AltaPorNuevaDuplicidad objAltaPorNuevaDuplicidad;

    @BeforeTest
    public void setup() throws IOException {

        //Se inicializa driver
        driver = initializerDriver();

        //se cargan variables para la prueba
        cargaVariablesCatastro();

        //se maximiza el navegador
        driver.manage().window().maximize();

        //Se crean los objetos de las clases a utilizar en las pruebas
        objLogin = new Login(driver);
        objMenu  = new Menu(driver);
        objPadronCatastro = new PadronCatastro(driver);
        objAltaDePredio = new AltaDePredio(driver);
        objAltaPorNuevaDuplicidad = new AltaPorNuevaDuplicidad(driver);
        fusor = new Fusor(cliente);
        faker = new Faker(new Locale("es-MX"));
        //Se ingresa a recaudador
        driver.get(url);

        //Se crea objeto de un explicit wait
        w = new WebDriverWait(driver,10);

        //Se espera a que el botón de inicio sesión se encuentre clickeable
        w.until(ExpectedConditions.elementToBeClickable(objLogin.btnInicioSesion()));
    }

    @Test(priority = 1)
    public void test_claveCatastroValida() {
        // Variables para usar el fusor
        Catastro catastro;
        String[] clave;
        Direccion direccion;

        // Hacer login
        objLogin.login(userName, password);

        // Espera a que se pueda dar click en Padrón de catastro.yml
        w.until(ExpectedConditions.elementToBeClickable(objMenu.getLnkPadronCatastro()));

        // Asegura que se encuentre en la pantalla correcta
        Assert.assertEquals("Menú", objMenu.getMenu());

        // Abre la sección de Padrón de catastro.yml
        objMenu.getLnkPadronCatastro().click();

        // Espera a que se pueda dar click en Alta del predio y se da click
        w.until(ExpectedConditions.elementToBeClickable(objPadronCatastro.getLnk_AltaDelPredio()));
        objPadronCatastro.clickAltaDelPredio();

        // Asegura que se encuentre en la pantalla correcta
        Assert.assertEquals("Alta al padrón de predio - Vigencia", objAltaDePredio.getTitulo());

        // Obtener los WebElements de los campos de texto
        List<WebElement> entradasDeTexto = objAltaDePredio.getEntradasDeTexto();

        // Asegura que solo sean 10 entradas (una por segmento de la clave)
        Assert.assertEquals(10, entradasDeTexto.size());

        // Comienza a probar claves, teniendo como máximo 100 intentos
        for (int i = 0; i < 100; i++) {

            // Genera una clave catastral
            catastro = fusor.catastro();
            clave = catastro.claveCatastralSegmentada;

            // Llena los campos con los segmentos catastrales
            objAltaDePredio.llenarSegmentosCatastrales(clave);

            // Click en Validar
            objAltaDePredio.clickValidar();

            // Cerrar la ventana emergente, en caso de que se presente
            if (objAltaDePredio.popUpIsPresented()) {
                w.until(ExpectedConditions.elementToBeClickable(objAltaDePredio.getBtnInfoPopupAceptar()));
                objAltaDePredio.clickAceptarInfoPopup();
            }

            // Es clave valida?
            w.until(ExpectedConditions.visibilityOf(objAltaDePredio.getImgValidacionCorrecta()));
            if (objAltaDePredio.estatusValidacion().equals("pt1:i29")) {
                // Es válida, terminar loop
                break;
            }

        }

        // Confirmar que es clave válida
        Assert.assertEquals("pt1:i29", objAltaDePredio.estatusValidacion());

    }

    @Test(priority = 2, enabled = false)
    public void llenarAltaPorNuevaDuplicidad(){

        // Seleccionar tipo de movimiento
        w.until(ExpectedConditions.elementToBeClickable(objAltaDePredio.getLst_tipoDeMovimiento()));
        objAltaDePredio.seleccionarTipoDeMovimiento("38 - ALTA POR NUEVA DUPLICIDAD");

        //// CONTINUAR A SIGUIENTE PANTALLA
        // Dar click en Siguiente
        objAltaDePredio.clickSiguiente();

        // Seleccionar tipo de propiedad
        w.until(ExpectedConditions.visibilityOf(objAltaPorNuevaDuplicidad.getLst_tipoDePropiedad()));
        objAltaPorNuevaDuplicidad.seleccionarTipoDePropiedadAleatoria();

        // Obtener datos de dirección
        Direccion direccion = fusor.direccion();

        // Llenar los campos
        objAltaPorNuevaDuplicidad.llenarCamposUbicacionUrbana(direccion.numeroExterior,
                                                    direccion.letra,
                                                    direccion.numeroInterior,
                                                    direccion.extensionNumeroInterior,
                                                    faker.phoneNumber().phoneNumber().replaceAll("\\s+", ""),
                                                    faker.phoneNumber().phoneNumber().replaceAll("\\s+", ""),
                                                    faker.phoneNumber().phoneNumber().replaceAll("\\s+", ""),
                                                    direccion.cfe,
                                                    direccion.jmas,
                                                    fusor.direccion().calle,
                                                    fusor.direccion().calle,
                                                    fusor.direccion().calle,
                                                    direccion.descripcionUbicacion
                                                    );

    }

    @AfterTest
    public void cerrarTodo() {
        driver.quit();
    }


}
