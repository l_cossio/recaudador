package recaudador.INCT1_catastro;

import com.github.javafaker.Faker;
import fusor.Direccion;
import fusor.Fusor;
import fusor.PersonaFisica;
import junit.framework.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import recaudador.INCT1_catastro.PageObjects.AltaContribuyente;
import recaudador.INLO1_login.pageObjects.Login;
import recaudador.INLO1_login.pageObjects.Menu;
import recaudador.INCT1_catastro.PageObjects.DatosEntradaPadronCatastro;
import recaudador.INCT1_catastro.PageObjects.PadronCatastro;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Locale;

/** Version: 1.0
 *  Nombre: Prueba de claves catastrales
 *  Descripción:
 *  Objetivos:
 *   - Entrar al portal
 *   - Entrar a Alta de Predios
 *   - Probar diferentes claves hasta que salga una válida
 *  Autor: Luis Cossío Ramírez
 *  Fecha de creación: 13/01/2021.
 *  Modificado por: N/A
 *  Fecha de modificación: N/A
 */

@Test(groups = "DGO")
public class TestAltaContribuyente extends DatosEntradaPadronCatastro {

    public WebDriver driver;
    private Login objLogin;
    private Menu objMenu;
    private AltaContribuyente objAltaContribuyente;
    private PadronCatastro objPadronCatastro;
    private WebDriverWait w;
    private Fusor fusor;
    private Faker faker;

    @BeforeTest
    public void setup() throws IOException {
        //Se inicializa driver
        driver = initializerDriver();

        //se cargan variables para la prueba
        cargaVariablesCatastro();

        //se maximiza el navegador
        driver.manage().window().maximize();

        //Se crean los objetos de las clases a utilizar en las pruebas
        objLogin = new Login(driver);
        objMenu  = new Menu(driver);
        objAltaContribuyente = new AltaContribuyente(driver);
        objPadronCatastro = new PadronCatastro(driver);
        fusor = new Fusor(cliente);
        faker = new Faker(new Locale("es-MX"));

        //Se ingresa a recaudador
        driver.get(url);

        //Se crea objeto de un explicit wait
        w = new WebDriverWait(driver,10);

        //Se espera a que el botón de inicio sesión se encuentre clickeable
        w.until(ExpectedConditions.elementToBeClickable(objLogin.btnInicioSesion()));
    }

    @Test
    public void registrarPersonaFisica() {

        // Variables para usar el fusor
        Fusor fusor = new Fusor(cliente);
        Direccion direccion;
        PersonaFisica persona;

        // Hacer INLO1_login
        objLogin.login(userName, password);

        // Espera a que se pueda dar click en Padrón de catastro
        w.until(ExpectedConditions.elementToBeClickable(objMenu.getLnkPadronCatastro()));

        // Asegura que se encuentre en la pantalla correcta
        Assert.assertEquals("Menú", objMenu.getMenu());

        // Abre la sección de Padrón de catastro
        objMenu.getLnkPadronCatastro().click();

        // Espera a que se pueda dar click en Alta del contribuyente y se da click
        w.until(ExpectedConditions.elementToBeClickable(objPadronCatastro.getLnkAltaContribuyente()));
        objPadronCatastro.clickAltaContribuyente();

        w.until(ExpectedConditions.elementToBeClickable(objAltaContribuyente.getRadiosTipoPersona().get(0)));
        objAltaContribuyente.selectPersonaFisica();

        objAltaContribuyente.clickCrearPersona();

        w.until(ExpectedConditions.elementToBeClickable(objAltaContribuyente.getCampoNombre()));

        // Generar los datos
        persona = fusor.personaFisica();

        // Fecha de nacimiento
        String patronFecha = "dd-MM-yyyy";
        SimpleDateFormat dateFormat = new SimpleDateFormat(patronFecha);

        //////// Introducir todos los datos
        objAltaContribuyente.llenarCamposPersonaFisica(
                persona.nombre,
                persona.primerApellido,
                persona.segundoApellido,
                persona.tercerApellido,
                persona.sexo,
                persona.estadoCivil,
                dateFormat.format(persona.birthdate),
                persona.rfcAlfa,
                persona.rfcFecha,
                persona.rfcHomo,
                persona.curp,
                "",
                "",
                ""
        );

        // Generar datos de direccion
        direccion = fusor.direccion();

        objAltaContribuyente.llenarCamposDireccion(
                direccion.calle,
                direccion.numeroExterior,
                direccion.numeroInterior,
                direccion.letra,
                direccion.tipoCalle,
                direccion.colonia,
                direccion.codigoPostal,
                direccion.localidad.replaceAll("[,.]", "" ),
                direccion.municipio.replaceAll("[,.]", "" ),
                direccion.estado,
                direccion.pais,
                faker.phoneNumber().phoneNumber().replaceAll(" ", ""),
                faker.phoneNumber().phoneNumber().replaceAll(" ", ""),
                persona.email,
                direccion.descripcionUbicacion
        );
    }

//    @Test
//    public void registrarPersonaMoral() {
//
//    }

    @AfterTest()
    public void cerrar(){
        driver.quit();
    }
}
