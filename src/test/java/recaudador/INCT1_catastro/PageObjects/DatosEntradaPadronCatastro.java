package recaudador.INCT1_catastro.PageObjects;

import recaudador.Base;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class DatosEntradaPadronCatastro extends Base {
    // Variables Catastro
    public String cliente;

    public void cargaVariablesCatastro() throws IOException {
        Properties prop = new Properties();
        FileInputStream fis = new FileInputStream
                ("src\\test\\java\\recaudador\\INCT1_catastro\\dataBank\\PadronCatastro.properties");

        prop.load(fis);
        cliente = getSystemProperty("env", "dgo");

    }
}
