package recaudador.INCT1_catastro.PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PadronCatastro {

    // VARIABLES
    WebDriver driver;

    public PadronCatastro(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    // ELEMENTOS

    @FindBy(linkText = "Asentamientos")
    WebElement lnk_Asentamientos;

    @FindBy(linkText = "Asignación de elementos")
    WebElement lnk_AsignacionDeElementos;

    @FindBy(linkText = "Bloques predios")
    WebElement lnk_BloquesPredios;

    @FindBy(linkText = "Características asentamientos")
    WebElement lnk_CaracteristicasAsentamientos;

    @FindBy(linkText = "Catálogo de características predios agrupación")
    WebElement lnk_CatalogoDeCaracteristicasPrediosAgrupacion;

    @FindBy(linkText = "Características valuación")
    WebElement lnk_CaracteristicasValuacion;

    @FindBy(linkText = "Segmentos catastrales")
    WebElement lnk_SegmentosCatastrales;

    @FindBy(linkText = "Segmentos catastrales ubicación")
    WebElement lnk_SegmentosCatastralesUbicacion;

    @FindBy(linkText = "Tipos avalúos")
    WebElement lnk_TiposAvaluos;

    @FindBy(linkText = "Tipos movimientos predios")
    WebElement lnk_TiposMovimientosPredios;

    @FindBy(linkText = "Tipos avalúos globales")
    WebElement lnk_TiposAvaluosGlobales;

    @FindBy(linkText = "Tipos movimientos usuarios")
    WebElement lnk_TiposMovimientosUsuarios;

    @FindBy(linkText = "Alta del predio")
    WebElement lnk_AltaDelPredio;

    @FindBy(linkText = "Alta contribuyente")
    WebElement lnk_AltaContribuyente;


    // METODOS


    public WebElement getLnk_AltaDelPredio() {
        return lnk_AltaDelPredio;
    }

    public void clickAltaDelPredio() {
        lnk_AltaDelPredio.click();
    }

    public WebElement getLnkAltaContribuyente() {
        return lnk_AltaContribuyente;
    }

    public void clickAltaContribuyente() {
        lnk_AltaContribuyente.click();
    }
}
