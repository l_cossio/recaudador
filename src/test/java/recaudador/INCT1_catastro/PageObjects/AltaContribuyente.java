package recaudador.INCT1_catastro.PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import recaudador.Pagina;

import java.util.List;

public class AltaContribuyente extends Pagina {

    private final WebDriver driver;
    private final WebDriverWait w;

    public AltaContribuyente(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);

        w = new WebDriverWait(driver, 3);
    }

//    ELEMENTOS WEB
    @FindBy(xpath = "//input[starts-with(@id, 'pt1:sor2:_')]")
    List<WebElement> radios_tipoPersona;

    @FindBy(css = "#pt1\\:cb6")
    WebElement boton_crearPersona;

    @FindBy(css = "#pt1\\:cb9")
    WebElement boton_guardar;

    @FindBy(css = "#pt1\\:cb2")
    WebElement boton_cancelar;

    @FindBy(css = "#pt1\\:it4\\:\\:content")
    WebElement txt_nombre;

    @FindBy(css = "#pt1\\:it5\\:\\:content")
    WebElement txt_primerApellido;

    @FindBy(css = "#pt1\\:it6\\:\\:content")
    WebElement txt_segundoApellido;

    @FindBy(css = "#pt1\\:it7\\:\\:content")
    WebElement txt_tercerApellido;

    @FindBy(css = "#pt1\\:soc1\\:\\:content")
    WebElement lst_sexo;

    @FindBy(css = "#pt1\\:soc2\\:\\:content")
    WebElement lst_estadoCivil;

    @FindBy(css = "#pt1\\:dateNacimiento\\:\\:content")
    WebElement txt_fechaDeNacimiento;

    @FindBy(css = "#pt1\\:it10\\:\\:content")
    WebElement txt_rfcAlfa;

    @FindBy(css = "#pt1\\:it11\\:\\:content")
    WebElement txt_rfcFecha;

    @FindBy(css = "#pt1\\:it12\\:\\:content")
    WebElement txt_rfcHomo;

    @FindBy(css = "#pt1\\:cb5")
    WebElement boton_generarRfc;

    @FindBy(css = "#pt1\\:it8\\:\\:content")
    WebElement txt_curp;

    @FindBy(css = "#pt1\\:dateDefuncion\\:\\:content")
    WebElement txt_fechaDeDefuncion;

    @FindBy(css = "#pt1\\:it16\\:\\:content")
    WebElement txt_identificadorPersonal1;

    @FindBy(css = "#pt1\\:it15\\:\\:content")
    WebElement txt_identificadorPersonal2;

    @FindBy(css = "#pt1\\:it36\\:\\:content")
    WebElement txt_calle;

    @FindBy(css = "#pt1\\:it38\\:\\:content")
    WebElement txt_numeroExterior;

    @FindBy(css = "#pt1\\:it39\\:\\:content")
    WebElement txt_numeroInterior;

    @FindBy(css = "#pt1\\:it37\\:\\:content")
    WebElement txt_letra;

    @FindBy(css = "#pt1\\:soc5\\:\\:content")
    WebElement lst_tipoDeCalle;

    @FindBy(css = "#pt1\\:it34\\:\\:content")
    WebElement txt_colonia;

    @FindBy(css = "#pt1\\:it40\\:\\:content")
    WebElement txt_codigoPostal;

    @FindBy(css = "#pt1\\:it33\\:\\:content")
    WebElement txt_localidad;

    @FindBy(css = "#pt1\\:it32\\:\\:content")
    WebElement txt_municipio;

    @FindBy(css = "#pt1\\:it31\\:\\:content")
    WebElement txt_estado;

    @FindBy(css = "#pt1\\:it29\\:\\:content")
    WebElement txt_pais;

    @FindBy(css = "#pt1\\:it41\\:\\:content")
    WebElement txt_telefono1;

    @FindBy(css = "#pt1\\:it42\\:\\:content")
    WebElement txt_telefono2;

    @FindBy(css = "#pt1\\:it43\\:\\:content")
    WebElement txt_email;

    @FindBy(css = "#pt1\\:it30\\:\\:content")
    WebElement txt_comentario;


    public WebElement getCampoNombre() {
        return txt_nombre;
    }

    public List<WebElement> getRadiosTipoPersona() {
        return radios_tipoPersona;
    }

    public void selectPersonaFisica() {
        radios_tipoPersona.get(0).click();
    }

    public void selectPersonaMoral() {
        radios_tipoPersona.get(1).click();
    }

    public void clickCrearPersona() {
        boton_crearPersona.click();
    }

    public void clickGuardar() {
        boton_guardar.click();
    }

    public void clickCancelar() {
        boton_cancelar.click();
    }

    public WebElement[] getTodosLosCamposDePersona() {
        return new WebElement[] {
                txt_nombre,
                txt_primerApellido,
                txt_segundoApellido,
                txt_tercerApellido,
                lst_sexo,
                lst_estadoCivil,
                txt_fechaDeNacimiento,
                txt_rfcAlfa,
                txt_rfcFecha,
                txt_rfcHomo,
                txt_curp,
                txt_fechaDeDefuncion,
                txt_identificadorPersonal1,
                txt_identificadorPersonal2
        };
    }

    public void clickGenerarRfc() {
        boton_generarRfc.click();
    }

    public void llenarCamposPersonaFisica(
            String nombre,
            String primerApellido,
            String segundoApellido,
            String tercerApellido,
            String sexo,
            String estadoCivil,
            String fechaNacimiento,
            String rfcAlfa,
            String rfcFecha,
            String rfcHomo,
            String curp,
            String fechaDefuncion,
            String identificador1,
            String identificador2
    ) {

        txt_nombre.clear();
        txt_nombre.sendKeys(nombre);

        txt_primerApellido.clear();
        txt_primerApellido.sendKeys(primerApellido);

        txt_segundoApellido.clear();
        txt_segundoApellido.sendKeys(segundoApellido);

        txt_tercerApellido.clear();
        txt_tercerApellido.sendKeys(tercerApellido);

        seleccionarOpcionPorTextoParcial(lst_sexo, sexo);
        seleccionarOpcionPorTextoParcial(lst_estadoCivil, estadoCivil);

        txt_fechaDeNacimiento.clear();
        txt_fechaDeNacimiento.sendKeys(fechaNacimiento);

        txt_rfcAlfa.clear();
        txt_rfcAlfa.sendKeys(rfcAlfa);

        txt_rfcFecha.clear();
        txt_rfcFecha.sendKeys(rfcFecha);

        txt_rfcHomo.clear();
        txt_rfcHomo.sendKeys(rfcHomo);

        txt_curp.clear();
        txt_curp.sendKeys(curp);

        txt_identificadorPersonal1.clear();
        txt_identificadorPersonal1.sendKeys(identificador1);

        txt_identificadorPersonal2.clear();
        txt_identificadorPersonal2.sendKeys(identificador2);
    }

    public void llenarCamposDireccion(
            String calle,
            String numeroExterior,
            String numeroInterior,
            String letra,
            String tipoDeCalle,
            String colonia,
            String codigoPostal,
            String localidad,
            String municipio,
            String estado,
            String pais,
            String telefono1,
            String telefono2,
            String email,
            String comentario
    ) {
        txt_calle.clear();
        txt_calle.sendKeys(calle);

        txt_numeroExterior.clear();
        txt_numeroExterior.sendKeys(numeroExterior);

        txt_numeroInterior.clear();
        txt_numeroInterior.sendKeys(numeroInterior);

        txt_letra.clear();
        txt_letra.sendKeys(letra);

        seleccionarOpcionPorTextoParcial(lst_tipoDeCalle, tipoDeCalle);

        txt_colonia.clear();
        txt_colonia.sendKeys(colonia);

        txt_codigoPostal.clear();
        txt_codigoPostal.sendKeys(codigoPostal);

        txt_localidad.clear();
        txt_localidad.sendKeys(localidad);

        txt_municipio.clear();
        txt_municipio.sendKeys(municipio);

        txt_estado.clear();
        txt_estado.sendKeys(estado);

        txt_pais.clear();
        txt_pais.sendKeys(pais);

        txt_telefono1.clear();
        txt_telefono1.sendKeys(telefono1);

        txt_telefono2.clear();
        txt_telefono2.sendKeys(telefono2);

        txt_email.clear();
        txt_email.sendKeys(email);

        txt_comentario.clear();
        txt_comentario.sendKeys(comentario);
    }
}
