package recaudador.INCT1_catastro.PageObjects;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import recaudador.Pagina;

import java.util.List;

/**
 * Page Object de Alta de Predio
 */
public class AltaDePredio extends Pagina {

    // ATRIBUTOS
    private final WebDriver driver;
    private final WebDriverWait w;

    public AltaDePredio(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);

        w = new WebDriverWait(driver, 3);
    }

    ///// ELEMENTOS WEB

    // Validacion clave catastral

    @FindBy(css = "#pt1\\:region2\\:0\\:soc1\\:\\:content")
    WebElement lst_tipoDeMovimiento;

    @FindBy(id = "pt1:cb3")
    WebElement btn_validar;

    @FindBy(id = "d1::msgDlg::cancel")
    WebElement btn_infoPopupAceptar;

    @FindBy(xpath = "//*[@title = 'validacionCorrecta']")
    WebElement img_validacionCorrecta;

    @FindBy(id = "pt1:cb2")
    WebElement btn_limpiar;

    @FindBy(css = "#d1\\:\\:msgDlg")
    WebElement pup_info;

    @FindBy(css = "#pt1\\:cb1")
    WebElement btn_siguiente;




    /**
     * campo = txt
     * lista = lst
     * select = vlr
     * boton = btn
     * label = lbl
     * popup = pup
     * imagen = img

     */
    /////METODOS

    // Getters

    public List<WebElement> getEntradasDeTexto() {
        return driver.findElements(By.xpath("//input[starts-with(@id,'pt1:its')]"));
    }

    public WebElement getImgValidacionCorrecta() {
        return img_validacionCorrecta;
    }

    public WebElement getBtn_limpiar() {
        return btn_limpiar;
    }

    public WebElement getBtnInfoPopupAceptar() {
        return btn_infoPopupAceptar;
    }

    public String getTitulo() {
        return driver.getTitle();
    }

    public WebElement getLst_tipoDeMovimiento() {
        return lst_tipoDeMovimiento;
    }

    // Acciones

    public void clickValidar() {
        btn_validar.click();
    }

    public void clickAceptarInfoPopup() {
        btn_infoPopupAceptar.click();
    }

    public String estatusValidacion() {
        return img_validacionCorrecta.getAttribute("id");
    }


    public boolean popUpIsPresented() {
        try {
            w.until(ExpectedConditions.visibilityOf(pup_info));
        } catch (TimeoutException e) {

        }
        return pup_info.isDisplayed();
    }

    public void clickSiguiente() {
        btn_siguiente.click();
    }



    public void seleccionarTipoDeMovimiento(String s) {
        seleccionarOpcionPorTexto(lst_tipoDeMovimiento, s);
    }

    public void llenarSegmentosCatastrales(String[] clave) {
        List<WebElement> entradasDeTexto = getEntradasDeTexto();
        int s = 0;
        for (WebElement entrada : entradasDeTexto) {
            w.until(ExpectedConditions.elementToBeClickable(entrada));
            entrada.clear();
            entrada.sendKeys(clave[s]);
            s++;
        }
    }
}
