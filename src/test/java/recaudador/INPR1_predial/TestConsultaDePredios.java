package recaudador.INPR1_predial;

import com.github.javafaker.Faker;
import fusor.Fusor;
import fusor.PersonaFisica;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;
import recaudador.INLO1_login.pageObjects.Login;
import recaudador.INLO1_login.pageObjects.Menu;
import recaudador.INPR1_predial.PageObjects.ConsultaDePredios;
import recaudador.INPR1_predial.PageObjects.ConsultaDePrediosDetalles;
import recaudador.INPR1_predial.PageObjects.DatosEntradaConsultaPredios;

import java.io.IOException;
import java.text.ParseException;
import java.util.Arrays;

@Test(groups = {"DGO"})
public class TestConsultaDePredios extends DatosEntradaConsultaPredios {

    WebDriver driver;
    Fusor fusor;
    Faker faker;
    ConsultaDePredios objConsultaDePredios;
    ConsultaDePrediosDetalles objDetalleDePredios;
    Login objLogin;
    Menu objMenu;


    @BeforeTest
    public void setup() throws IOException {

        cargaVariablesConsultaDePredios();

        faker = new Faker();
        fusor = new Fusor(cliente);
        driver = initializerDriver();
        driver.manage().window().maximize();
        objLogin = new Login(driver);
        objMenu = new Menu(driver);
        objConsultaDePredios = new ConsultaDePredios(driver);
        objDetalleDePredios = new ConsultaDePrediosDetalles(driver);
        driver.get(url);

        objLogin.login(userName, password);
    }

    @BeforeMethod
    public void ingresarAConsultaDePredios() {
        objMenu.clickMenuPredial();
        objConsultaDePredios.clickConsultaDePredios();
    }

    /**
     * Nombre: Consulta de predios por datos de predio.
     * Descripción: Consultar predios a través de los diferentes parámetros de Predio.
     * Objetivos de la prueba:
     *   - Consulta por clave catastral
     *   - Consulta por folio
     *   - Consulta por CURT
     *   - Consulta por tipo de propiedad
     *   - Consulta por tipo de predio
     * Autor: Luis Cossío Ramírez.
     * Fecha de creación: 12-02-2021.
     * Modificado por: N/A.
     * Fecha de modificación: N/A.
     */
    @Test (priority = 1)
    public void consultaPorPredio() {

        // Consulta por clave catastral
        String clave = fusor.catastro().claveCatastralExistente;
        objConsultaDePredios.consultaPorClaveCatastral(clave);

        // Consulta por folio
        String folio = faker.numerify("#######");
        objConsultaDePredios.consultaPorFolio(folio);
        objConsultaDePredios.clickRestablecer();

        // Consulta por CURT
        String curt = faker.regexify("[0-9]{21}");
        objConsultaDePredios.consultaPorCURT(curt);
        objConsultaDePredios.clickRestablecer();

        // Consulta por tipo de propiedad
        objConsultaDePredios.consultaAleatoriaPorPropiedad();
        objConsultaDePredios.clickRestablecer();

        // Consulta por tipo de predio
        objConsultaDePredios.consultaAleatoriaPorTipoPredio();
        objConsultaDePredios.clickRestablecer();

    }

    /**
     * Nombre: Consulta de predios por datos de domicilio.
     * Descripción: Consultar predios a través de los diferentes parámetros de Domicilio.
     * Objetivos de la prueba:
     *   - Consulta de domicilio depurado usando vialidad
     *   - Consulta de domicilio depurado usando codigo postal
     *   - Consulta de domicilio depurado usando asentamiento
     *   - Consulta de domicilio no depurado
     * Autor: Luis Cossío Ramírez.
     * Fecha de creación: 12-02-2021.
     * Modificado por: N/A.
     * Fecha de modificación: N/A.
     */

    @Test (priority = 2)
    public void consultaPorDomicilio() {

        // Consulta de domicilio depurado usando vialidad
        String vialidad = fusor.direccion().calle;
        objConsultaDePredios.consultaPorVialidad(vialidad);

        // Consulta de domicilio depurado usando código postal
        String codigoPostal = fusor.direccion().codigoPostal;
        objConsultaDePredios.consultaPorCodigoPostal(codigoPostal);

        // Consulta de domicilio depurado usando asentamiento
        String asentamiento = fusor.direccion().localidad;
        objConsultaDePredios.consultaPorAsentamiento(asentamiento);

        // Consulta de domicilio no depurado
        String domicilio = fusor.direccion().domicilio;
        objConsultaDePredios.consultaDomicilioNoDepurado(domicilio);
    }

    /**
     * Nombre: Consulta de predios por datos de Propietario.
     * Descripción: Consultar predios a través de los diferentes parámetros de Propietario.
     * Objetivos de la prueba:
     *   - Consulta de persona física usando nombre
     *   - Consulta de persona física usando primer apellido
     *   - Consulta de persona física usando segundo apellido
     *   - Consulta de persona física usando CURP
     *   - Consulta de persona física usando RFC
     *   - Consulta de persona moral usando razon social
     *   - Consulta de persona moral usando nombre comercial
     *   - Consulta de persona moral usando rfc
     * Autor: Luis Cossío Ramírez.
     * Fecha de creación: 12-02-2021.
     * Modificado por: N/A.
     * Fecha de modificación: N/A.
     */

    @Test (priority = 3)
    public void consultaPorPropietario() throws IOException {

        // Consulta de persona física usando nombre
        String nombre = fusor.personaFisica().nombre;
        objConsultaDePredios.consultaPorNombre(nombre);

        // Consulta de persona física usando primer apellido
        String primerApellido = fusor.personaFisica().primerApellido;
        objConsultaDePredios.consultaPorPrimerApellido(primerApellido);

        // Consulta de persona física usando segundo apellido
        String segundoApellido = fusor.personaFisica().segundoApellido;
        objConsultaDePredios.consultaPorSegundoApellido(segundoApellido);

        // Consulta de persona física usando CURP
        String curp = fusor.personaFisica().curp;
        objConsultaDePredios.consultaPorCURP(curp);

        // Consulta de persona física usando RFC
        PersonaFisica p = fusor.personaFisica();
        objConsultaDePredios.consultaPorRfcFisica(new String[]{p.rfcAlfa, p.rfcFecha, p.rfcHomo});

        // Consulta de persona moral usando razon social
        String razonSocial = faker.letterify("%?%");
        objConsultaDePredios.consultaPorRazonSocial(razonSocial);

        // Consulta de persona moral usando nombre comercial
        String nombreComercial = faker.letterify("%?%");
        objConsultaDePredios.consultaPorNombreComercial(nombreComercial);

        // Consulta de persona moral usando rfc
        String[] row = getRandomRow(datos_reales).split(",");
        objConsultaDePredios.consultaPorRfcMoral(Arrays.copyOfRange(row, 2,5));

    }

    @Test (priority = 4)
    public void consultasCombinadas() throws IOException {
        String razonSocial = faker.letterify("%?%");
        objConsultaDePredios.consultaPorTipoPredioYRazonSocial(razonSocial);

        String nombre = fusor.personaFisica().nombre;
        objConsultaDePredios.consultaPorTipoPropiedadYNombre(nombre);

        objConsultaDePredios.consultaPorTipoPredioYTipoVialidad();

        String[] row = getRandomRow(datos_reales).split(",");
        objConsultaDePredios.consultaPorPersonaMoralYCatastro(row[2],row[3],row[4], row[5]);
    }

    /**
     * Nombre: Consulta de predio y verificacion de datos.
     * Descripción: Consultar predios a través de los diferentes parámetros de Propietario.
     * Objetivos de la prueba:
     *   - Buscar un predio existente
     *   - Verificar clave catastral
     *   - Verificar nombre
     *   - Verificar calle
     *   - Verificar que tenga al menos un dato de movimiento
     *   - Verificar que el popup de valor total catastral coincida con el mostrado en la tabla
     * Autor: Luis Cossío Ramírez.
     * Fecha de creación: 03-03-2021.
     * Modificado por: N/A.
     * Fecha de modificación: N/A.
     */
    @Test (priority = 5)
    public void consultaDePrediosYDatos() throws IOException, InterruptedException, ParseException {
        // Buscar un predio existente
        String[] datos = getRandomRow(pefi_predios).split(",");
        objConsultaDePredios.consultaParaDatos(datos);
        objConsultaDePredios.clickSiguiente();

        // Verificar clave catastral
        objDetalleDePredios.validarClaveCatastral(datos[6]);

        // Verificar nombre
        String nombre = String.join(" ", Arrays.copyOfRange(datos, 0, 3));
        objDetalleDePredios.validarNombre(nombre);

        // Verificar calle
        objDetalleDePredios.validarCalle(datos[7] + " " + datos[8]);

        // Verificar que tenga al menos un dato de movimiento
        objDetalleDePredios.abrirPestanaObligaciones();
        objDetalleDePredios.validarNumeroDeMovimientos(1);

        // Verificar que el popup de valor total catastral coincida con el mostrado en la tabla
        objDetalleDePredios.validarValorCatastral();
    }

    @AfterMethod
    public void regresarAlInicio() {
        objConsultaDePredios.clickHome();
    }

    @AfterTest
    public void cerrar() {
//        driver.quit();
    }

}
