package recaudador.INPR1_predial.PageObjects;

import com.codeborne.selenide.*;
import com.github.javafaker.Faker;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import recaudador.Pagina;

import java.util.Locale;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

public class ConsultaDePredios extends Pagina {

    WebDriver driver;
    Faker faker;

    public ConsultaDePredios(WebDriver driver) {
        WebDriverRunner.setWebDriver(driver);
        this.driver = driver;
        Configuration.timeout = 20000; // millis
        Configuration.savePageSource = false;
        Configuration.holdBrowserOpen = true;
        faker = new Faker(new Locale("es-MX"));
    }

    // Entrada
    By lnk_consultaDePredios = new By.ByLinkText("Consulta de predios");
    By lnk_home = new By.ById("pt1:i1");

    // Predio
    By sec_predio = new By.ById("pt1:ph2::_afrDscl");
    By txt_claveCatastral = new By.ById("pt1:it4::content");
    By txt_folioReal = new By.ById("pt1:it5::content");
    By txt_CURT = new By.ById("pt1:it28::content");
    By chk_buscarPorClaveAnterior = new By.ById("pt1:sbc1::content");
    By txt_claveAnterior = new By.ById("pt1:it29::content");
    By lst_tipoPropiedad = new By.ById("pt1:soc5::content");
    By lst_tipoPredio = new By.ById("pt1:soc6::content");

    // Domicilio
    By sec_domicilio = new By.ById("pt1:pb3::_afrDscl");
    By rad_domicilioDepurado = new By.ById("pt1:sor3:_0");
    By rad_domicilioNoDepurado = new By.ById("pt1:sor3:_1");
    By lst_municipio = new By.ById("pt1:soc1::content");
    By lst_localidad = new By.ById("pt1:soc2::content");
    By lst_tipoAsentamiento = new By.ById("pt1:soc3::content");
    By txt_asentamiento = new By.ById("pt1:it7::content");
    By lst_tipoVialidad = new By.ById("pt1:soc4::content");
    By txt_vialidad = new By.ById("pt1:it11::content");
    By txt_codigoPostal = new By.ById("pt1:it19::content");
    By txt_numeroExterior = new By.ById("pt1:it20::content");
    By txt_numeroInterior = new By.ById("pt1:it21::content");
    By txt_letra = new By.ById("pt1:it22::content");
    By txt_extNumeroInterior = new By.ById("pt1:it23::content");
    By txt_domicilioNoDepurado = new By.ById("pt1:it24::content");

    // Propietario
    By sec_propietario = new By.ById("pt1:ph3::_afrDscl");
    By rad_personaFisica = new By.ById("pt1:sor2:_0");
    By rad_personaMoral = new By.ById("pt1:sor2:_1");
    By txt_nombre = new By.ById("pt1:it1::content");
    By txt_primerApellido = new By.ById("pt1:it2::content");
    By txt_segundoApellido = new By.ById("pt1:it3::content");
    By txt_rfcAlfaFisica = new By.ById("pt1:it9::content");
    By txt_rfcFechaFisica = new By.ById("pt1:it12::content");
    By txt_rfcHomoFisica = new By.ById("pt1:it13::content");
    By txt_CURP = new By.ById("pt1:it16::content");
    By txt_razonSocial = new By.ById("pt1:it6::content");
    By txt_nombreComercial = new By.ById("pt1:it8::content");
    By txt_rfcAlfaMoral = new By.ById("pt1:it15::content");
    By txt_rfcFechaMoral = new By.ById("pt1:it17::content");
    By txt_rfcHomoMoral = new By.ById("pt1:it26::content");

    // Botones
    By btn_buscar = new By.ById("pt1:cb1");
    By btn_restablecer = new By.ById("pt1:cb2");
    By btn_siguiente = new By.ById("pt1:cb4");

    // Tabla de resultados
    By div_resultados = new By.ById("pt1:t3::db");
    By tbl_resultados = new By.ById("//div[@id=pt1:t3::db]/table");
    By img_mensajesError = new By.ByXPath("//*[contains(@title,'Error')]");
    By img_mensajesInformacion = new By.ByXPath("//*[contains(@title,'Información')]");

    //// DATOS
    By tab_personaFisica = new By.ById("pt1:r1:0:sdi1::disAcr");
    By tab_domicilioReferencia = new By.ById("pt1:r1:0:sdi2::disAcr");
    By tab_caracteristicas = new By.ById("pt1:r1:0:sdi3::disAcr");
    By tab_predios = new By.ById("pt1:r1:0:sdi4::disAcr");

    By lbl_claveCatastral = new By.ById("pt1:r1:0:ot9");
    By lbl_propietario = new By.ById("pt1:r1:0:ot31");

    public void clickConsultaDePredios() {
        $(lnk_consultaDePredios).click();
    }

    public void clickHome() {
        $(lnk_home).click();
        Assert.assertEquals(title(), "Recaudador");
    }

    /**
     * Verifica que la tabla haya respondido con datos o con falta de datos
     * @return la condición
     */
    public static Condition showingResults(String whatKind) {
        return new Condition("showing some result") {
            @Override
            public boolean apply(Driver driver, WebElement webElement) {

                WebElement tbl_resultados = webElement.findElement(By.xpath("table"));
                switch (whatKind) {
                    case "any":
                        return (webElement.getText().equals("No se encontró ningún resultado con los datos ingresados.")
                        || Integer.parseInt(tbl_resultados.getAttribute("_rowcount")) > 0);

                    case "some":
                        return Integer.parseInt(tbl_resultados.getAttribute("_rowcount")) > 0;

                    case "none":
                        return webElement.getText().equals("No se encontró ningún resultado con los datos ingresados.");

                    default:
                        return false;
                }
            }
        };
    }

    private Condition showingAnyResults = showingResults("any");
    private Condition showingResults = showingResults("some");
    private Condition showingNoResults = showingResults("none");

    public void clickRestablecer() {
        $(btn_restablecer).click();
    }

    /**
     * Verifica que se desplieguen datos sin errores
     * @param condition La condición a revisar (showingResults, showingNoResults o showingAnyResults)
     */
    public void verificarConsulta(Condition condition){
        $(div_resultados).shouldBe(condition);
        $$(img_mensajesError).filterBy(Condition.visible).shouldHaveSize(0);
        try {
            $$(img_mensajesInformacion).filterBy(Condition.visible).shouldHaveSize(0);
        } catch (Throwable e) {
            System.out.println(e);
            System.out.println("Mensaje de información desplegado");
        }
    }

    /**
     * Amplía la pestaña que se solicite, si ya se encuentra abierta no hace nada
     * @param element el elemento que al hacer click amplía la pestaña
     */
    private void ampliarPestana(By element) {
        if ($(element).getAttribute("class").equals("xt6 x1lu")) {
            $(element).click();
        }
    }

    /**
     * Reduce la pestaña que se solicite, si ya se encuentra reducida no hace nada
     * @param element el elemento que al hacer click reduce la pestaña
     */
    private void reducirPestana(By element) {
        if ($(element).getAttribute("class").equals("xt6 x1lv")) {
            $(element).click();
        }
    }

    private void consulta(String input, By field, boolean existente) {
        consulta(new String[]{input}, new By[]{field}, existente);
    }

    /**
     * Realiza una consulta, asegurar que todos los campos se encuentren disponibles antes de llamar al método
     * @param inputs los valores a ingresar. Debe ser paralelo con fields
     * @param fields los campos a llenar, en caso de ser select, se ignora el input correspondiente y se hace aleatorio.
     *               Debe ser paralelo con inputs.
     * @param debeExistir true si debería desplegar resultados
     */
    private void consulta(String[] inputs, By[] fields, boolean debeExistir) {
        if (inputs.length != fields.length) {
            throw new ArrayIndexOutOfBoundsException("Las listas de inputs y fields son de diferente longitud");
        }
        for(int i=0; i<inputs.length; i++){
            String input = inputs[i];
            By field = fields[i];
            if ($(field).getTagName().equals("select")) {
                seleccionarOpcionAleatoria(field);
            } else {
                $(field).setValue(input);
            }
        }
        $(btn_buscar).click();
        if (debeExistir) {
            verificarConsulta(showingResults);
        } else {
            verificarConsulta(showingAnyResults);
        }
    }

    private void consultaAleatoria(By lista, By section) {
        ampliarPestana(section);
        seleccionarOpcionAleatoria(lista);
        $(btn_buscar).click();
        verificarConsulta(showingAnyResults);
        clickRestablecer();
    }

    public void consultaPorClaveCatastral(String clave) {
        System.out.println("Consultando por clave catastral");
        ampliarPestana(sec_predio);
        consulta(clave, txt_claveCatastral, true);
        clickRestablecer();
    }

    public void consultaPorFolio(String folio) {
        System.out.println("Consultando por folio inventado");
        ampliarPestana(sec_predio);
        consulta(folio,txt_folioReal, false);
        clickRestablecer();
    }

    public void consultaPorCURT(String curt) {
        System.out.println("Consultando por curt inventado");
        ampliarPestana(sec_predio);
        consulta(curt,txt_CURT, false);
        clickRestablecer();
    }

    public void consultaAleatoriaPorPropiedad() {
        System.out.println("Consultando por tipo de propiedad");
        ampliarPestana(sec_predio);
        consultaAleatoria(lst_tipoPropiedad, sec_predio);
        clickRestablecer();
    }

    public void consultaAleatoriaPorTipoPredio() {
        System.out.println("Consultando por tipo de predio");
        consultaAleatoria(lst_tipoPredio, sec_predio);
        clickRestablecer();
    }

    public void consultaPorVialidad(String vialidad) {
        System.out.println("Consultando por vialidad");
        ampliarPestana(sec_domicilio);
        $(rad_domicilioDepurado).click();
        consulta(vialidad, txt_vialidad, false);
        clickRestablecer();
    }

    public void consultaPorCodigoPostal(String codigoPostal) {
        System.out.println("Consultando por código postal");
        ampliarPestana(sec_domicilio);
        $(rad_domicilioDepurado).click();
        consulta(codigoPostal, txt_codigoPostal, false);
        clickRestablecer();
    }

    public void consultaPorAsentamiento(String asentamiento) {
        System.out.println("Consultando por asentamiento");
        ampliarPestana(sec_domicilio);
        $(rad_domicilioDepurado).click();
        consulta(asentamiento, txt_asentamiento, false);
        clickRestablecer();
    }

    public void consultaDomicilioNoDepurado(String domicilio) {
        System.out.println("Consultando por domicilio");
        ampliarPestana(sec_domicilio);
        $(rad_domicilioNoDepurado).click();
        consulta(domicilio, txt_domicilioNoDepurado, false);
        clickRestablecer();
    }

    public void consultaPorNombre(String nombre) {
        System.out.println("Consultando por nombre");
        ampliarPestana(sec_propietario);
        $(rad_personaFisica).click();
        consulta(nombre, txt_nombre, false);
        clickRestablecer();
    }

    public void consultaPorPrimerApellido(String primerApellido) {
        System.out.println("Consultando por primer apellido");
        ampliarPestana(sec_propietario);
        $(rad_personaFisica).click();
        consulta(primerApellido, txt_primerApellido, false);
        clickRestablecer();
    }

    public void consultaPorSegundoApellido(String segundoApellido) {
        System.out.println("Consultando por segundo apellido");
        ampliarPestana(sec_propietario);
        $(rad_personaFisica).click();
        consulta(segundoApellido, txt_segundoApellido, false);
        clickRestablecer();
    }

    public void consultaPorCURP(String curp) {
        System.out.println("Consultando por curp");
        ampliarPestana(sec_propietario);
        $(rad_personaFisica).click();
        consulta(curp, txt_CURP, false);
        clickRestablecer();
    }

    public void consultaPorRfcFisica(String[] rfc) {
        System.out.println("Consultando por rfc (persona física)");
        ampliarPestana(sec_propietario);
        $(rad_personaFisica).click();
        consulta(rfc, new By[]{txt_rfcAlfaFisica, txt_rfcFechaFisica, txt_rfcHomoFisica},false);
        clickRestablecer();
    }

    public void consultaPorRazonSocial(String razonSocial) {
        System.out.println("Consultando por razón social");
        ampliarPestana(sec_propietario);
        $(rad_personaMoral).click();
        consulta(razonSocial, txt_razonSocial, true);
        clickRestablecer();
    }

    public void consultaPorNombreComercial(String nombreComercial) {
        System.out.println("Consultando por nombre comercial");
        ampliarPestana(sec_propietario);
        $(rad_personaMoral).click();
        consulta(nombreComercial, txt_nombreComercial, true);
        clickRestablecer();
    }

    public void consultaPorRfcMoral(String[] rfc) {
        System.out.println("Consultando por rfc (persona moral)");
        ampliarPestana(sec_propietario);
        $(rad_personaMoral).click();
        consulta(rfc, new By[]{txt_rfcAlfaMoral, txt_rfcFechaMoral, txt_rfcHomoMoral}, true);
        clickRestablecer();
    }

    public void consultaPorTipoPredioYRazonSocial(String razonSocial) {
        System.out.println("Consultando por tipo de predio y razón social");
        ampliarPestana(sec_predio);
        ampliarPestana(sec_propietario);
        $(rad_personaMoral).click();
        consulta(new String[]{null, razonSocial},
                new By[]{lst_tipoPredio, txt_razonSocial},
                false);
        clickRestablecer();
    }

    public void consultaPorTipoPropiedadYNombre(String nombre) {
        System.out.println("Consultando por tipo de propiedad y nombre");
        ampliarPestana(sec_predio);
        ampliarPestana(sec_propietario);
        $(rad_personaFisica).click();
        consulta(new String[]{null, nombre},
                new By[]{lst_tipoPropiedad, txt_nombre},
                false);
        clickRestablecer();
    }

    public void consultaPorTipoPredioYTipoVialidad() {
        System.out.println("Consultando por tipo de predio y tipo de vialidad");
        ampliarPestana(sec_predio);
        ampliarPestana(sec_domicilio);
        $(rad_domicilioDepurado).click();
        consulta(new String[]{null, null}, new By[]{lst_tipoPredio,lst_tipoVialidad}, false);
        clickRestablecer();
    }

    public void consultaPorPersonaMoralYCatastro(String alpha, String fecha, String homo, String clave) {
        System.out.println("Consultando por rfc (persona moral) y clave catastral");
        ampliarPestana(sec_predio);
        ampliarPestana(sec_propietario);
        $(rad_personaMoral).click();
        consulta(new String[]{alpha, fecha, homo, clave},
                new By[]{txt_rfcAlfaMoral, txt_rfcFechaMoral, txt_rfcHomoMoral, txt_claveCatastral},
                        true);
        clickRestablecer();
    }

    public void consultaParaDatos(String[] datos) {
        System.out.println("Consultando para verificar datos en la siguiente pantalla");
        ampliarPestana(sec_predio);
        ampliarPestana(sec_domicilio);
        ampliarPestana(sec_propietario);
        consulta(datos,
                new By[] {txt_nombre, txt_primerApellido, txt_segundoApellido, txt_rfcAlfaFisica, txt_rfcFechaFisica,
                        txt_rfcHomoFisica,txt_claveCatastral, txt_vialidad, txt_numeroExterior},
                true);
    }

    public void clickSiguiente() throws InterruptedException {
        long currentTimeout = Configuration.timeout;
        Configuration.timeout = 30000;
        $(btn_siguiente).click();
        $(btn_siguiente).should(disappear);
        $("#pt1\\:r1\\:0\\:pb2").should(appear);
        Configuration.timeout = currentTimeout;
    }


}
