package recaudador.INPR1_predial.PageObjects;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import recaudador.Pagina;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import static com.codeborne.selenide.CollectionCondition.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

public class ConsultaDePrediosDetalles extends Pagina {
    WebDriver driver;

    public ConsultaDePrediosDetalles(WebDriver driver) {
        WebDriverRunner.setWebDriver(driver);
        this.driver = driver;
        Configuration.timeout = 20000; // millis
        Configuration.savePageSource = false;
        Configuration.holdBrowserOpen = true;
    }

    private By img_home = new By.ById("pt1:i1");

    //// Tab Datos del Predio

    private By lbl_claveCatastral = new By.ById("pt1:r1:0:ot9");
    private By lbl_ubicacion = new By.ById("pt1:r1:0:ot30");
    private By lbl_propietario = new By.ById("pt1:r1:0:ot25");

    // Tab Persona Fisica
    private By tab_personaFisica = new By.ById("pt1:r1:0:sdi1::disAcr");
    private By lbl_rfcPropietario = new By.ById("pt1:r1:0:ot12");

    //// Tab Obligaciones
    private By tab_obligaciones = new By.ById("pt1:sdi3::disAcr");
    private By rows_movimientos = new By.ByXPath("//div[@id='pt1:pc3:histMov::db']/table/tbody/tr");
    private By cel_ultimoValorTotalCatastral = new By.ById("pt1:pc3:histMov:1:cl1");
    private By lbl_valorTotalCatastral = new By.ById("pt1:itVTOC::content");
    private By btn_popAceptar = new By.ById("pt1:d2::ok");

    public void validarClaveCatastral(String clave) {
        System.out.println("Validando clave catastral");
        $(lbl_claveCatastral).shouldHave(exactText(clave));
    }

    public void validarNombre(String nombre) {
        System.out.println("Validando nombre");
        $(lbl_propietario).shouldHave(text(nombre));
    }

    public void validarCalle(String direccion) {
        System.out.println("Validando calle");
        $(lbl_ubicacion).shouldHave(text(direccion));
    }

    public void validarNumeroDeMovimientos(int minimo) {
        System.out.println("Validando que haya " + minimo + " o más movimientos");
        $$(rows_movimientos).shouldHave(sizeGreaterThanOrEqual(minimo));
    }
    public void validarValorCatastral() throws ParseException, InterruptedException {
        System.out.println("Validando que el valor total catastral de la tabla coincida con el de la ventana");
        NumberFormat format = NumberFormat.getInstance(Locale.US);
        double valorTabla = format.parse($(cel_ultimoValorTotalCatastral).getText()).doubleValue();
        $(cel_ultimoValorTotalCatastral).hover();
        Thread.sleep(400);
        $(cel_ultimoValorTotalCatastral).click();
        $(lbl_valorTotalCatastral).shouldHave(exactText(format.format(valorTabla)));
        $(btn_popAceptar).click();
        $(btn_popAceptar).should(disappear);
    }

    public void abrirPestanaObligaciones() {
        $(tab_obligaciones).hover().click();
        $(cel_ultimoValorTotalCatastral).should(appear);
    }

    public void clickHome(){
        $(img_home).click();
    }
}
