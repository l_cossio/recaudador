package recaudador.INPR1_predial.PageObjects;

import recaudador.Base;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class DatosEntradaConsultaPredios extends Base {

    public String cliente;
    public String datos_reales;
    public String pefi_predios;
    /**
     * Carga de variables para la prueba de Consulta de Predios
     *
     * @throws IOException when file not found
     */
    public void cargaVariablesConsultaDePredios() throws IOException {

        // Preparar archivo
        Properties prop = new Properties();
        FileInputStream fis = new FileInputStream
                ("src\\test\\java\\recaudador\\INPR1_predial\\dataBank\\ConsultaDePredios.properties");

        prop.load(fis);
        cliente = prop.getProperty("cliente");
        datos_reales = prop.getProperty("datos_reales");
        pefi_predios = prop.getProperty("pefi_predios");

    }
}
