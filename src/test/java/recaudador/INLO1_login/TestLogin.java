package recaudador.INLO1_login;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import junit.framework.Assert;
import recaudador.Base;
import recaudador.INLO1_login.pageObjects.Login;
import recaudador.INLO1_login.pageObjects.Menu;

@Test(groups = {"TAB", "DGO", "CUU"})
public class TestLogin extends Base{
	
	public WebDriver driver;
	Login objLogin;
	Menu objMenu;

	private static final Logger LOGGER = LogManager.getLogger(TestLogin.class.getName());

	@BeforeMethod
	public void setup() throws IOException {

		driver = initializerDriver();

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		driver.get(url);

	}
	/*******************************************************************************
		*Nombre: Login
		*Descripción: Inicio de sesión en recaudador.  
		*Objetivos de la prueba:    
		    - Inicio de sesión
		*Autor: Jesus Muñoz   
		*Fecha de creación: 18-09-2020.  
		*Modificado por: N/A
		*Fecha de modificación: N/A
	*******************************************************************************/ 
	
	@Test()
	public void test_login() {
		
    	objLogin = new Login(driver);
    	LOGGER.info("Prueba test_LoginPass");
    	objLogin.login(userName, password);
    	
    	//Validar ingresar el menú principal
		Assert.assertEquals("Bienvenido a Recaudador", objLogin.getTituloMenuPrincipal());
    	LOGGER.info("Validación exitosa");
		
	}
	
	/*******************************************************************************  
		*Nombre: Error Login
		*Descripción: Inicio de sesión Con usuario no existente.  
		*Objetivos de la prueba:    
		    - Validar no permita inicio de sesión
		*Autor: Jesus Muñoz   
		*Fecha de creación: 18-09-2020.  
		*Modificado por: N/A
		*Fecha de modificación: N/A
	*******************************************************************************/ 
	
	@Test()
	public void test_loginError() throws InterruptedException {
		
    	objLogin = new Login(driver);
    	objLogin.login("PruebaError", "PruebaError123");
    	
    	Thread.sleep(3000);
    	
    	//Validar el querer ingresar con usuario INACTIVO.

		Assert.assertEquals("Nombre de usuario o contraseña incorrecto.", objLogin.getErrorUsuarioIncorrecto());
    	LOGGER.info("Validación exitosa");

    	Assert.assertTrue(objLogin.getErrorUsuarioIncorrecto().equals("Nombre de usuario o contraseña incorrecto."));

		
	}
	
    @AfterMethod
    public void finisher() {

		driver.quit();

    }
	
}
