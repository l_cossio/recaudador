package recaudador.INLO1_login.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static com.codeborne.selenide.Selenide.$;

public class Login {
	
	//Se declara constructor de la clase
	public Login(WebDriver driver){

        this.driver = driver;
        PageFactory.initElements(driver, this);

    }
	//VARIABLES
	
	//se declara el WebDriver
	WebDriver driver;
	
	//campos de texto
	@FindBy(id="txtUsuario::content")
	WebElement txt_userName;
	
	@FindBy(id="txtPass::content")
	WebElement txt_password;
	
	@FindBy(id="pt1:it1::content")    
	WebElement txt_userNameOC;
	
	@FindBy(id="pt1:it2::content")
	WebElement txt_respuestaSecreta;
	
	//botones
	@FindBy (id="cb1")    
	WebElement btn_iniciarSesion;
	
	@FindBy(id="pt1:cb2")
	WebElement btn_aceptar;
		
	@FindBy(id="pt1:cb1")
	WebElement btn_cancelar;
	
	@FindBy (id="pt1:ctb3")    
	WebElement btn_RestablecerOC;
	
	@FindBy(css="div button[id='pt1:cb1']")
	WebElement btn_volverOC;
	
	@FindBy(id="d1::msgDlg::cancel")
	WebElement btn_AceptarErrorOC;
	
	@FindBy(css="span button[id='pt1:cb1']")
	WebElement btn_cancelarOC;	
	
	@FindBy(css="span button[id='cb1'")
	WebElement btn_cerrarSesion;

	//links
	@FindBy(linkText="¿Ha olvidado su contraseña?")
	WebElement lnk_olvidadoContrasena;
	
	//etiquetas
	@FindBy(css="div[title='Bienvenido a Recaudador'] > h1")
	WebElement lbl_tituloMenuPrincipal;

	@FindBy(css="table tbody tr td div[class='x15j']")
    public
    WebElement lbl_errorUsuarioIncorrecto;

	@FindBy(css= "span[style='color:Navy; font-size:large; font-family:Arial, Helvetica, sans-serif; font-weight:bold;']")
	WebElement lbl_restablecioContrasenaOC;

	@FindBy(css="div[title='Restablecer clave de usuario'] > h1")
	WebElement lbl_tituloRestablecerClaveOC;

	@FindBy(css="span[Style='font-size:12.0px; font-family:Arial, Helvetica, sans-serif; color:Navy;']")
	WebElement lbl_preguntaSecreta;
	
	@FindBy(css="table tbody tr td div[class='x15j']")
	public
	WebElement lbl_usuarioNoEncntradoOC;

	@FindBy(css="div[id='pb2']>table[class='xqm p_AFCore p_AFDefault'] tbody tr td[class='xra p_AFCore p_AFDefault'] > table[cellspacing='0'] tbody tr td[style='white-space:nowrap'] > h2[class='xr0']")
	WebElement lbl_InicioSesion;


	//METODOS
	public WebElement btnInicioSesion() {
		return btn_iniciarSesion;
	}

	public WebElement btnCerrarSesion() {
		return btn_cerrarSesion;
	}

	public void login(String userName, String password) {
			
		$(txt_userName).sendKeys(userName);
		$(txt_password).sendKeys(password);
		$(btn_iniciarSesion).click();

	}
	
	public void clicOlvidoContrasena() {
		
		lnk_olvidadoContrasena.click();
		
	}
	
	public void getUsuarioRecuperaC(String userName) {
		
		txt_userNameOC.sendKeys(userName);
		
	}
	
	public void clicAceptarRecuperaC() {
		
		btn_aceptar.click();
		
	}
	
	public void clicCancelarRecuperarC() {
		
		btn_cancelar.click();
		
	}

	public void setRespuestaSecretaRecuperaC(String respuestaSecreta) {
		
		txt_respuestaSecreta.sendKeys(respuestaSecreta);
		
	}	
    
	public void clicRestablecerRecuperaC() {
		
		btn_RestablecerOC.click();
		
	}
	
	
    public void clicVolverRecuperaC() {
		
    	btn_volverOC.click();
		
	}
	
	public String getTituloMenuPrincipal() {
		
		return lbl_tituloMenuPrincipal.getText();
			
	}

	public String getErrorUsuarioIncorrecto() {
		
		return lbl_errorUsuarioIncorrecto.getText();
			
	}	
	
    public String getRestablecioContrasena() {
		
		return lbl_restablecioContrasenaOC.getText();
			
	}	
	
    public String getTituloRestablecerClave() {
		
		return lbl_tituloRestablecerClaveOC.getText();
			
	}	
    
    public String getPreguntaSecreta() {
		
		return lbl_preguntaSecreta.getText();
			
	}
    
    public String getUsuarioNoEncontradoOC() {
    	return lbl_usuarioNoEncntradoOC.getText();
    }
    
    public void clicAceptarErrorOC() {
    	btn_AceptarErrorOC.click();
    }
    
    public void clicCancelarOC() {
    	btn_cancelarOC.click();
    }
    
    public String getInicioSesion() {
    	return lbl_InicioSesion.getText();
    }
    
    public void clicCerrarSesion() {
    	btn_cerrarSesion.click();
    }

}
