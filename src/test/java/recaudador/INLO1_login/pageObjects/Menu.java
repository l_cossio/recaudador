package recaudador.INLO1_login.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static com.codeborne.selenide.Selenide.*;

public class Menu {
	
	//Se declara constructor de la clase
    public Menu(WebDriver driver) {		
		this.driver = driver;		
		PageFactory.initElements(driver,this);
	}
    //VARIABLES
	
  	//se declara el WebDriver   
	WebDriver driver;
	
	//links
	@FindBy(css="a[id='sdi1::disAcr']")
	WebElement lnk_menu;
	
	@FindBy(linkText = "Menú de Administración de la Aplicación")
	WebElement lnk_administracionAplicacion;

	@FindBy(linkText = "Padrón de Catastro")
	WebElement lnk_padronCatastro;

	public By lnk_menuPredial = new By.ByLinkText("Menú de Predial");

	public By lnk_menuInfraccionesTransito = new By.ByLinkText("Menú Infracciones de tránsito");

	// Vehicular
	public By lnk_menuVehicular = new By.ByLinkText("Menú de Vehicular");
	public By lnk_consultaDeValoresPorFolio = new By.ByLinkText("Consulta de Valores por Folio");

	//MÉTODOS
	
	public String getMenu() {
		return lnk_menu.getText();
	}

	public WebElement lnkAdministracionAplicacion() {
		return lnk_administracionAplicacion;
	}

	public WebElement getLnkPadronCatastro() {
		return lnk_padronCatastro;
	}

	public void clicAdministracionAplicacion() {
		lnk_administracionAplicacion.click();
	}

	public void clickMenuPredial() {
		$(lnk_menuPredial).click();
	}

	public void clickInfraccionesTransito() {
		$(lnk_menuInfraccionesTransito).click();
	}

	public String imprimirModulo(){
		int modulos = driver.findElements(By.cssSelector("a[id*='submenu1'][theme='light']")).size();
		int x = 0;
		while (x<modulos){
			if(driver.findElement(By.cssSelector("a[id*='submenu1'][theme='light'][title='"+x+"']")).getText().equals("Menú de Administración de la Aplicación")) {
				return driver.findElement(By.cssSelector("a[id*='submenu1'][theme='light'][title='" + (x) + "']")).getText();
			}
			x++;
		}
		return null;
	}

	public void clicModulo(String modulo){
		int modulos = driver.findElements(By.cssSelector("a[id*='submenu1'][theme='light']")).size();
		int x = 0;
		while (x<modulos){
			if(driver.findElement(By.cssSelector("a[id*='submenu1'][theme='light'][title='"+x+"']")).getText().equals(modulo)) {
				driver.findElement(By.cssSelector("a[id*='submenu1'][theme='light'][title='" + (x) + "']")).click();
				break;
			}
			x++;
		}
	}

	public void clickMenuVehicular() {
		$(lnk_menuVehicular).click();
	}

	public void clickConsultaDeValoresPorFolio() {
		$(lnk_consultaDeValoresPorFolio).click();
	}
}
	