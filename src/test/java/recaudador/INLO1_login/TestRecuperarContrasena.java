package recaudador.INLO1_login;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import junit.framework.Assert;
import recaudador.Base;
import recaudador.INLO1_login.pageObjects.Login;

@Test(groups = {"TAB", "DGO", "CUU"})
public class TestRecuperarContrasena extends Base{

	public WebDriver driver;
	Login objLogin;
	
	@BeforeMethod
	public void setup() throws IOException {
		
		driver = initializerDriver();
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		driver.get(url);
		
	}
	
	/*******************************************************************************  
		*Nombre:  Recuperar Contraseña
		*Descripción: Recuperar contraseña de un usuario activo en recaudador y validar 
		 el inicio de sesión con nueva contraseña.  
		*Objetivos de la prueba:    
		    - Restablecer contraseña
		    - Validar inicio de sesión 
		*Autor: Jesus Muñoz y Paulina Pérez.   
		*Fecha de creación: 18-09-2020.  
		*Modificado por: N/A
		*Fecha de modificación: N/A
	*******************************************************************************/ 
	
	@Test()
	public void test_olvidoContrasenaHappyPath() {
	
		objLogin = new Login(driver);
		//Clic Olvido contraseña
		objLogin.clicOlvidoContrasena();
		//Valida ingreso a pantalla Restablecer
		Assert.assertEquals("Restablecer clave de usuario", objLogin.getTituloRestablecerClave());
		//Captura Usuario 
		objLogin.getUsuarioRecuperaC(userRecoveryPass);
		//Clic en botón recoperar contraseña
		objLogin.clicAceptarRecuperaC();
		//Valida se solicite pregunta secreta
		Assert.assertEquals("* Conteste la pregunta secreta para restablecer la contraseña:",
				objLogin.getPreguntaSecreta());
		//Se ingresa respuesta a pregunta secreta 
		objLogin.setRespuestaSecretaRecuperaC(answerRecoveryPass);
		//Se da clic en restablecer contraseña
		objLogin.clicRestablecerRecuperaC();
		//Se valida mensaje de exito al recuperar contraseña
		Assert.assertEquals("La contraseña se restablecio exitosamente.", objLogin.getRestablecioContrasena());
		//Clic en botón volver
		objLogin.clicVolverRecuperaC();
		//Se ingresa contraseña nueva uario$01
		objLogin.login(userRecoveryPass, userRecoveryPass+"$01");
		// Se valida regreso a pantalla principal
		Assert.assertEquals("Bienvenido a Recaudador", objLogin.getTituloMenuPrincipal());
	
	}
	
	/*******************************************************************************  
		*Nombre:  Recuperar Contraseña usuario inactivo
		*Descripción: Recuperar contraseña de un usuario Inactivo en recaudador y validar 
		 el inicio de sesión con nueva contraseña.  
		*Objetivos de la prueba:    
		    - Restablecer contraseña
		    - Validar no realize el inicio de sesión 
		*Autor: Jesus Muñoz y Paulina Pérez.   
		*Fecha de creación: 18-09-2020.  
		*Modificado por: N/A
		*Fecha de modificación: N/A
	*******************************************************************************/ 
	
	@Test()
	public void test_olvidoContrasenaUsuarioInactivo()  {
	
		//Create Login Page object
		objLogin = new Login(driver);
		//Clic Olvido contraseña
		objLogin.clicOlvidoContrasena();
		//Valida ingreso a pantalla Restablecer
		Assert.assertEquals("Restablecer clave de usuario", objLogin.getTituloRestablecerClave());
		//Captura Usuario
		objLogin.getUsuarioRecuperaC(userRecoveryPassIN);
		//Clic en botón recoperar contraseña
		objLogin.clicAceptarRecuperaC();
		//Valida se solicite pregunta secreta
		Assert.assertEquals("* Conteste la pregunta secreta para restablecer la contraseña:",
				objLogin.getPreguntaSecreta());
		//Se ingresa respuesta a pregunta secreta 
		objLogin.setRespuestaSecretaRecuperaC(answerRecoveryPassIN);
		//Se da clic en restablecer contraseña
		objLogin.clicRestablecerRecuperaC();
		//Se valida mensaje de exito al recuperar contraseña
		Assert.assertEquals("La contraseña se restablecio exitosamente.", objLogin.getRestablecioContrasena());
		//Clic en botón volver
		objLogin.clicVolverRecuperaC();
		//Se ingresa contraseña nueva uario$01
		objLogin.login(userRecoveryPassIN, userRecoveryPassIN+"$01");
		//Tiempo de espera implicito para mensaje de usuario incorrecto
		WebDriverWait w = new WebDriverWait(driver,5);
	    w.until(ExpectedConditions.visibilityOf(objLogin.lbl_errorUsuarioIncorrecto));
		//Se valida mensaje de usuario incorrecto
		Assert.assertEquals("Nombre de usuario o contraseña incorrecto.", objLogin.getErrorUsuarioIncorrecto());
		
	}
	
	/*******************************************************************************  
		*Nombre:  Validaciones para olvido contraseña
		*Descripción: Se realizan validaciones como caracteres especiales y validaciones de botones.  
		*Objetivos de la prueba:    
		    - Restablecer contraseña
		    - Validar inicio de sesión 
		*Autor: Jesus Muñoz y Paulina Pérez.   
		*Fecha de creación: 18-09-2020.  
		*Modificado por: N/A
		*Fecha de modificación: N/A
	*******************************************************************************/ 

	@Test()
	public void test_olvidoContrasenaValidaciones() {
		
		objLogin = new Login(driver);
		//Clic Olvio contraseña
		objLogin.clicOlvidoContrasena();
		//Valida ingreso a pantalla Restablecer
		Assert.assertEquals("Restablecer clave de usuario", objLogin.getTituloRestablecerClave());
		//Se capturan caracteres especiales
		objLogin.getUsuarioRecuperaC("#$$%$$");
		//Se da clic en recuperar contraseña
		objLogin.clicAceptarRecuperaC();
		//tiempo de espera a mensaje de usuario no encontrado 
		WebDriverWait w = new WebDriverWait(driver,5);
	    w.until(ExpectedConditions.visibilityOf(objLogin.lbl_usuarioNoEncntradoOC));
		//Se valida mensaje
		Assert.assertEquals("No se encontraron los datos del usuario, verifique el nombre de usuario.",
				objLogin.getUsuarioNoEncontradoOC());
		// Clic en botn aceptar
		objLogin.clicAceptarErrorOC();
		// Clic en botn cancelar
		objLogin.clicCancelarOC();
		//Se valida regreso a pantalla principal 
		Assert.assertEquals("Inicio de sesión", objLogin.getInicioSesion());
	
	}
	
	@AfterMethod
    public void finisher() {
    	
    	driver.quit();
    	
    }
}
