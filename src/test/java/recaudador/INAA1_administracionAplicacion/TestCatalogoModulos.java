package recaudador.INAA1_administracionAplicacion;

import junit.framework.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import recaudador.INAA1_administracionAplicacion.pageObjects.CatalogoModulos;
import recaudador.INAA1_administracionAplicacion.pageObjects.DatosEntradaAdmonAplicacion;
import recaudador.INLO1_login.pageObjects.Login;
import recaudador.INLO1_login.pageObjects.Menu;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

@Test(groups = {"TAB", "DGO", "CUU"})
public class TestCatalogoModulos extends DatosEntradaAdmonAplicacion {

    public WebDriver driver;
    WebDriverWait w;
    Login objLogin;
    Menu objMenu;
    CatalogoModulos objCataModu;

    @BeforeMethod
    public void setup () throws IOException {
        driver = initializerDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        CargaVariablesCataModu();
        driver.get(url);
        objLogin = new Login(driver);
        objMenu  = new Menu(driver);
        objCataModu = new CatalogoModulos(driver);
    }

    /*******************************************************************************
     *Nombre: Happy path agregar módulo.
     *Descripción: Flujo completo del catálogo de módulos.
     *Objetivos de la prueba:
     - Crear un módulo padre.
     - Modificar el módulo padre creado.
     - Crear un módulo hijo.
     - Modificar el módulo hijo.
     - Borrar el módulo hijo.
     - Borrar el módulo padre.
     *Autor: Jesús Javier Muñoz Romero.
     *Fecha de creacion: 30-09-2020.
     *Modificado por: N/A.
     *Fecha de modificación: N/A.
     *******************************************************************************/
    @Test
    public void test_catalogos_Modulos(){
        objLogin.login(userName,password);
        //Validar ingresar el menú principal
        org.testng.Assert.assertEquals(objMenu.getMenu(), "Menú");
        objMenu.clicAdministracionAplicacion();
        objCataModu.clicCataModu();

        //Validar ingresar al catálogo de módulos
        Assert.assertEquals("Catálogo de módulos",objCataModu.getTitulo());

        objCataModu.clicAgregarModulo();
        objCataModu.crearModuloPadre(claveModuP1,nombreModuP1,nombreCortoModuP1,tipoModuP1,controladorModuP1,transicionModuP1,
                ordenModuP1,implementacionModuP1,cajaDiversoModuP1);
        objCataModu.clicCancelarPadre();

        objCataModu.clicAgregarModulo();
        objCataModu.crearModuloPadre(claveModuP1,nombreModuP1,nombreCortoModuP1,tipoModuP1,controladorModuP1,transicionModuP1,
                ordenModuP1,implementacionModuP1,cajaDiversoModuP1);
        objCataModu.clicCrearPadre();

        //Validar la creación del módulo padre
        Assert.assertEquals(claveModuP1.toUpperCase(),objCataModu.getClaveP().toUpperCase());
        Assert.assertEquals(nombreCortoModuP1.toUpperCase(),objCataModu.getNombreCortoP().toUpperCase());
        Assert.assertEquals(tipoModuP1.toUpperCase(),objCataModu.getTipoP().toUpperCase());
        //Assert.assertEquals(controladorModuP1.toUpperCase(),objCataModu.getControlP().toUpperCase());
        Assert.assertEquals(transicionModuP1.toUpperCase(),objCataModu.getTransP().toUpperCase());
        Assert.assertEquals(ordenModuP1.toUpperCase(),objCataModu.getOrdenP().toUpperCase());
        Assert.assertEquals(implementacionModuP1.toUpperCase().toUpperCase(),objCataModu.getImplementaP().toUpperCase());
        Assert.assertEquals(cajaDiversoModuP1.toUpperCase(),objCataModu.getCajaDiversoP().toUpperCase());

        objCataModu.clicNombrePadre();
        objCataModu.modificarModuloPadre(claveModuP2,nombreModuP2,nombreCortoModuP2,tipoModuP2,controladorModuP2,transicionModuP2,
                ordenModuP2,implementacionModuP2,cajaDiversoModuP2);
        objCataModu.clicCrearPadre();

        //Validar la modificación del módulo padre
        Assert.assertEquals(claveModuP2.toUpperCase(),objCataModu.getClaveP().toUpperCase());
        Assert.assertEquals(nombreCortoModuP2.toUpperCase(),objCataModu.getNombreCortoP().toUpperCase());
        Assert.assertEquals(tipoModuP2.toUpperCase(),objCataModu.getTipoP().toUpperCase());
        //Assert.assertEquals(controladorModuP2.toUpperCase(),objCataModu.getControlP().toUpperCase());
        Assert.assertEquals(transicionModuP2.toUpperCase(),objCataModu.getTransP().toUpperCase());
        Assert.assertEquals(ordenModuP2.toUpperCase(),objCataModu.getOrdenP().toUpperCase());
        Assert.assertEquals(implementacionModuP2.toUpperCase().toUpperCase(),objCataModu.getImplementaP().toUpperCase());
        Assert.assertEquals(cajaDiversoModuP2.toUpperCase(),objCataModu.getCajaDiversoP().toUpperCase());

        objCataModu.clicInsertaHijo();
        objCataModu.crearModuloHijo(claveModuH1,nombreModuH1,nombreCortoModuH1,tipoModuH1,controladorModuH1,transicionModuH1,
                ordenModuH1,implementacionModuH1,cajaDiversoModuH1);
        objCataModu.clicCancelarHijo();

        objCataModu.encontrarModuPadre(nombreModuP2);

        objCataModu.clicInsertaHijo();
        objCataModu.crearModuloHijo(claveModuH1,nombreModuH1,nombreCortoModuH1,tipoModuH1,controladorModuH1,transicionModuH1,
                ordenModuH1,implementacionModuH1,cajaDiversoModuH1);
        objCataModu.clicCrearHijo();

        objCataModu.encontrarModuPadre(nombreModuP2);

        //Validar la creación del hijo
        Assert.assertEquals(claveModuH1.toUpperCase(),objCataModu.getClaveH().toUpperCase());
        Assert.assertEquals(nombreCortoModuH1.toUpperCase(),objCataModu.getNombreCortoH().toUpperCase());
        Assert.assertEquals(tipoModuH1.toUpperCase(),objCataModu.getTipoH().toUpperCase());
        //Assert.assertEquals(controladorModuH1.toUpperCase(),objCataModu.getControlP().toUpperCase());
        Assert.assertEquals(transicionModuH1.toUpperCase(),objCataModu.getTransicionH().toUpperCase());
        Assert.assertEquals(ordenModuH1.toUpperCase(),objCataModu.getOrdenH().toUpperCase());
        Assert.assertEquals(implementacionModuH1.toUpperCase().toUpperCase(),objCataModu.getImplementaH().toUpperCase());
        Assert.assertEquals(cajaDiversoModuH1.toUpperCase(),objCataModu.getCajaDiversoH().toUpperCase());

        objCataModu.clicNombreHijo();

        objCataModu.modicarModuloHijo(claveModuH2,nombreModuH2,nombreCortoModuH2,tipoModuH2,controladorModuH2,transicionModuH2,
                ordenModuH2,implementacionModuH2,cajaDiversoModuH2);
        objCataModu.clicModificarHijo();

        objCataModu.encontrarModuPadre(nombreModuP2);

        //Validar la modificación del hijo
        Assert.assertEquals(claveModuH2.toUpperCase(),objCataModu.getClaveH().toUpperCase());
        Assert.assertEquals(nombreCortoModuH2.toUpperCase(),objCataModu.getNombreCortoH().toUpperCase());
        Assert.assertEquals(tipoModuH2.toUpperCase(),objCataModu.getTipoH().toUpperCase());
        //Assert.assertEquals(controladorModuH1.toUpperCase(),objCataModu.getControlP().toUpperCase());
        Assert.assertEquals(transicionModuH2.toUpperCase(),objCataModu.getTransicionH().toUpperCase());
        Assert.assertEquals(ordenModuH2.toUpperCase(),objCataModu.getOrdenH().toUpperCase());
        Assert.assertEquals(implementacionModuH2.toUpperCase().toUpperCase(),objCataModu.getImplementaH().toUpperCase());
        Assert.assertEquals(cajaDiversoModuH2.toUpperCase(),objCataModu.getCajaDiversoH().toUpperCase());

        objCataModu.borrarModulo();

        objCataModu.borrarModulo();
    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }

}
