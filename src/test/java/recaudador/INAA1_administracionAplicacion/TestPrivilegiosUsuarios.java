package recaudador.INAA1_administracionAplicacion;

import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.testng.Assert;
import org.testng.annotations.*;
import recaudador.INAA1_administracionAplicacion.pageObjects.DatosEntradaAdmonAplicacion;
import recaudador.INAA1_administracionAplicacion.pageObjects.PrivilegiosUsuarios;
import recaudador.INLO1_login.pageObjects.Login;
import recaudador.INLO1_login.pageObjects.Menu;

import java.io.IOException;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

@Test(groups = {"TAB", "DGO", "CUU"})
public class TestPrivilegiosUsuarios extends DatosEntradaAdmonAplicacion {

    public WebDriver driver;
    Login objLogin;
    Menu objMenu;
    FluentWait w;
    PrivilegiosUsuarios objPrivilegiosUsuarios;


    @BeforeMethod
    public void setup() throws IOException{
        //Se inicializa driver
        driver = initializerDriver();

        //Se maximiza el navegador
        driver.manage().window().maximize();

        //Se carga las variables del properties
        cargaVariablesPrivilegiosUsuarios();

        //Se crean los objetos de las clases a utilizar en las pruebas
        objLogin = new Login(driver);
        objMenu  = new Menu(driver);
        objPrivilegiosUsuarios = new PrivilegiosUsuarios(driver);

        //Se ingresa a recaudador
        driver.get(url);

        //Se crea objeto de un fluent wait
        w = new FluentWait(driver)
                .pollingEvery(Duration.ofMillis(200))
                .withTimeout(Duration.ofSeconds(15))
                .ignoring(StaleElementReferenceException.class, Exception.class);
        w.until(ExpectedConditions.elementToBeClickable(objLogin.btnInicioSesion()));

        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    /*******************************************************************************
     *Nombre: Agregar Privilegios usuarios
     *Descripción: Se crear Privilegios a un usuario
     *Objetivos de la prueba:
     - Crear  privilegios a un usuario
     - Validar que se guarden cambios
     *Autor: Paulina Alejandra Pérez Ochoa.
     *Fecha de creacion: 01-10-2020.
     *Modificado por: N/A
     *Fecha de modificación: N/A
     *******************************************************************************/

    @Test(priority = 2)
    public void test_CrearPrivilegio() throws InterruptedException {
        //Se inicia sesion en la aplicacion recaudador
        objLogin.login(userName, password);

        //Se espera a que se encuentre clikeable el link de administracion de la aplicacion
        w.until(ExpectedConditions.elementToBeClickable(objMenu.lnkAdministracionAplicacion()));

        //Se valida el inicio de sesion
        Assert.assertEquals(objMenu.getMenu(),"Menú");

        //Se ingresa al menu administracion de la aplicacion
        objMenu.clicAdministracionAplicacion();

        //Se ingresa a privilegios usuarios
        objPrivilegiosUsuarios.clicPrivilegiosUsuarios();

        //Se espera cargue la pagina validando se habilite el boton de agregar
        w.until(ExpectedConditions.visibilityOf(objPrivilegiosUsuarios.tbl_tabla()));

        //Se valida ingreso a la pantalla privilegios usuarios
        Assert.assertTrue(objPrivilegiosUsuarios.getPrivilegiosUsuario().contentEquals("Privilegios de usuario"));

        //Se busca usuario
        objPrivilegiosUsuarios.setUsuario(usuarioPriv);

        //Clic en boton buscar
        objPrivilegiosUsuarios.clicBuscar();

        //Agregar nuevo privilegio
        w.until(ExpectedConditions.visibilityOf(objPrivilegiosUsuarios.tbl_tabla()));

        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);

        //Se da clic al boton agregar
        objPrivilegiosUsuarios.clicAgregar();

        //Se valida ingreso a la pantalla
        Assert.assertTrue(objPrivilegiosUsuarios.getTituloCrear().contentEquals("Privilegios de usuario - Crear"));

        //Se valida boton cancelar
        objPrivilegiosUsuarios.clicCancelar();

        //Tiempo de espera regreso a pagina principal
        w.until(ExpectedConditions.visibilityOf(objPrivilegiosUsuarios.tbl_tabla()));

        //Se valida ingreso a la pantalla privilegios usuarios
        Assert.assertTrue(objPrivilegiosUsuarios.getPrivilegiosUsuario().contentEquals("Privilegios de usuario"));

        //Se busca usuario
        objPrivilegiosUsuarios.setUsuario(usuarioPriv);

        //Se espera cargue la pagina validando se habilite el boton de buscar
        w.until(ExpectedConditions.elementToBeClickable(objPrivilegiosUsuarios.btnBuscar()));
        objPrivilegiosUsuarios.clicBuscar();

        //Se espera busqueda
        w.until(ExpectedConditions.visibilityOf(objPrivilegiosUsuarios.tbl_tabla()));

        //Agregar nuevo privilegio
        objPrivilegiosUsuarios.clicAgregar();

        //Se espera a que carge el boton Crear
        w.until(ExpectedConditions.elementToBeClickable(objPrivilegiosUsuarios.btnCrear()));

        //Se valida ingreso a la pantalla
        Assert.assertTrue(objPrivilegiosUsuarios.getTituloCrear().contentEquals("Privilegios de usuario - Crear"));

        //Se ingresan datos del nuevo privilegio
        objPrivilegiosUsuarios.setCrearPrivilegio(tipoPriv, descripcionPriv, valor, valor2, valor3);

        //click al boton crear
        objPrivilegiosUsuarios.clicCrear();

        //Se espera carga pantalla principal
        w.until(ExpectedConditions.visibilityOf(objPrivilegiosUsuarios.tbl_tabla()));

        //Se verifica que el usaurio se dio de alta correctamente
        Assert.assertTrue(objPrivilegiosUsuarios.getTipo().contentEquals(tipoPriv.toUpperCase()));
        Assert.assertTrue(objPrivilegiosUsuarios.getDescripcion().contentEquals(descripcionPriv.toUpperCase()));
        Assert.assertTrue(objPrivilegiosUsuarios.getUsuarioTabla().contentEquals(usuarioPriv.toUpperCase()));
        Assert.assertTrue(objPrivilegiosUsuarios.getValor().contentEquals(valor.toUpperCase()));
        Assert.assertTrue(objPrivilegiosUsuarios.getValor2().contentEquals(valor2.toUpperCase()));
        Assert.assertTrue(objPrivilegiosUsuarios.getValor3().contentEquals(valor3.toUpperCase()));

    }

    /*******************************************************************************
     *Nombre: Consultar  Privilegios usuarios
     *Descripción: Se concultan diferentes privilegios de un usuario
     *Objetivos de la prueba:
     - Consultar privilegios por filtro "Tipo","Descripción" y "Valor"
     *Autor: Paulina Alejandra Pérez Ochoa.
     *Fecha de creacion: 01-10-2020.
     *Modificado por: N/A
     *Fecha de modificación: N/A
     *******************************************************************************/
    @Test(priority = 3)
    public void test_consultaPrivilegio() {
        //Se inicia sesion en la aplicacion recaudador
        objLogin.login(userName, password);

        //Se espera a que se encuentre clickeable el link de administracion de la aplicacion
        w.until(ExpectedConditions.elementToBeClickable(objMenu.lnkAdministracionAplicacion()));

        //Se valida el inicio de sesion
        Assert.assertEquals(objMenu.getMenu(), "Menú");

        //Se ingresa al menu administracion de la aplicacion
        objMenu.clicAdministracionAplicacion();

        //Se ingresa a privilegios usuarios
        objPrivilegiosUsuarios.clicPrivilegiosUsuarios();

        //Se espera carge la pagina validando se hablite el boton de agreagar
        w.until(ExpectedConditions.elementToBeClickable(objPrivilegiosUsuarios.btnAgregar()));

        //Se valida ingreso a la pantalla privilegios usuarios
        Assert.assertTrue(objPrivilegiosUsuarios.getPrivilegiosUsuario().contentEquals("Privilegios de usuario"));

        //Se busca usuario para consulta
        objPrivilegiosUsuarios.setUsuario(usuarioCon);
        //Se espera carge la pagina validando se hablite el boton de buscar
        w.until(ExpectedConditions.elementToBeClickable(objPrivilegiosUsuarios.btnBuscar()));
        //Clic en boton buscar
        objPrivilegiosUsuarios.clicBuscar();
        //Se espera busqueda
        w.until(ExpectedConditions.elementToBeClickable(objPrivilegiosUsuarios.btnBuscar()));
        w.until(ExpectedConditions.elementToBeClickable(objPrivilegiosUsuarios.btnAgregar()));
        //Se realiza consulta descripcion
        objPrivilegiosUsuarios.setDescripcion(filtroDesc);
        //Se da clic en buscar
        objPrivilegiosUsuarios.clicBuscar();
        // se espera cargen datos en la  tabla
        w.until(ExpectedConditions.elementToBeClickable(objPrivilegiosUsuarios.btnAgregar()));
        //Se valida resultados  de busqueda
        Assert.assertTrue(objPrivilegiosUsuarios.getDescripcion().contentEquals(filtroDesc.toUpperCase()));
        //Se limpia la descripcion
        objPrivilegiosUsuarios.borrarDecripcion();
        //Clic en busacar para refrescar pantalla
        objPrivilegiosUsuarios.clicBuscar();
        //tiempo de espera a que se actualice tabla
        w.until(ExpectedConditions.elementToBeClickable(objPrivilegiosUsuarios.btnBuscar()));
        w.until(ExpectedConditions.elementToBeClickable(objPrivilegiosUsuarios.btnAgregar()));
        //Ingresar busqueda por valor
        objPrivilegiosUsuarios.setValor(filtroValor);
        //Clic en buscar
        objPrivilegiosUsuarios.clicBuscar();
        //Se espera a que carge datos la tabla habilitando botones
        w.until(ExpectedConditions.elementToBeClickable(objPrivilegiosUsuarios.btnBuscar()));
        w.until(ExpectedConditions.elementToBeClickable(objPrivilegiosUsuarios.btnAgregar()));
        //Se valida resultados de busqueda
        Assert.assertTrue(objPrivilegiosUsuarios.getValor().contentEquals(filtroValor.toUpperCase()));
        //Se limpia el valor
        objPrivilegiosUsuarios.borrarValor();
        //Se da clic en buscar  para actualizar
        objPrivilegiosUsuarios.clicBuscar();
       //Se espera se actualize tabla
        w.until(ExpectedConditions.elementToBeClickable(objPrivilegiosUsuarios.btnBuscar()));
        w.until(ExpectedConditions.elementToBeClickable(objPrivilegiosUsuarios.btnAgregar()));
        //Se ingresa busqueda por tipo
        objPrivilegiosUsuarios.setTipoPrivilegio(filtroTipo);
        //Se da clic en buscar
        objPrivilegiosUsuarios.clicBuscar();
        //Se espera actialice la tabla
        w.until(ExpectedConditions.elementToBeClickable(objPrivilegiosUsuarios.btnAgregar()));
        //Se validan los resultados
        Assert.assertTrue(objPrivilegiosUsuarios.getTipo().contentEquals(filtroTipo.toUpperCase()));

    }

    /*******************************************************************************
     *Nombre: Modificar  Privilegios usuarios
     *Descripción: Se modifican privilegios a usuarios
     *Objetivos de la prueba:
     - Modificar  privilegios a un usuario
     - Validar se guarden cambios
     *Autor: Paulina Alejandra Pérez Ochoa.
     *Fecha de creacion: 06-10-2020.
     *Modificado por: N/A
     *Fecha de modificación: N/A
     *******************************************************************************/
    @Test(priority = 4)
    public void test_modificarPrivilegio() {
        // Se inicia sesion en la aplicacion recaudador
        objLogin.login(userName, password);

        // Se espera a que se encuentre clikeable el link de administracion de la aplicacion
        w.until(ExpectedConditions.elementToBeClickable(objMenu.lnkAdministracionAplicacion()));

        // Se valida el inicio de sesion
        Assert.assertEquals(objMenu.getMenu(), "Menú");

        // Se ingresa al menu administracion de la aplicacion
        objMenu.clicAdministracionAplicacion();

        // Se espera a que se encuentre clickeable el link privilegios de usuarios
        w.until(ExpectedConditions.elementToBeClickable(objPrivilegiosUsuarios.lnk_privilegiosUsuarios()));

        // Se ingresa a privilegios usuarios
        objPrivilegiosUsuarios.clicPrivilegiosUsuarios();

        // Se espera carge la pagina validando se hablite el boton de agreagar
        w.until(ExpectedConditions.elementToBeClickable(objPrivilegiosUsuarios.btnAgregar()));

        // Se valida ingreso a la pantalla privilegios usuarios
        Assert.assertTrue(objPrivilegiosUsuarios.getPrivilegiosUsuario().contentEquals("Privilegios de usuario"));

        //Se busca usuario
        objPrivilegiosUsuarios.setUsuario(usuarioPriv);

        //Se espera carge la pagina validando se hablite el boton de buscar
        w.until(ExpectedConditions.elementToBeClickable(objPrivilegiosUsuarios.btnBuscar()));

        //Se realiza consulta descripcion, valor y tipo de privilegio
        objPrivilegiosUsuarios.setValor(valor);
        objPrivilegiosUsuarios.setTipoPrivilegio(tipoPriv);
        objPrivilegiosUsuarios.setDescripcion(descripcionPriv);

        //Clic en boton buscar
        objPrivilegiosUsuarios.clicBuscar();

        //Espera a que cargen elementos
        w.until(ExpectedConditions.elementToBeClickable(objPrivilegiosUsuarios.btnBuscar()));
        w.until(ExpectedConditions.elementToBeClickable(objPrivilegiosUsuarios.btnAgregar()));

        //Clic en el boton modificar
        objPrivilegiosUsuarios.clicModificar();

        //Validar carguen etiquetas
        Assert.assertTrue(objPrivilegiosUsuarios.getTituloModificar().contentEquals("Privilegios de usuario - Modificar"));

        //Borrar los datos del privilegio
        objPrivilegiosUsuarios.borrarMod();

        //insertar datos nuevos a modificar
        objPrivilegiosUsuarios.setModificarPrivilegio(tipoPrivMod,descripcionMod,valorMod,valor2Mod,valor3Mod);

        //Clic en el boton guardar
        objPrivilegiosUsuarios.clicGuardarCambios();

        //Se espera a que los elementos de la pagina carguen
        w.until(ExpectedConditions.elementToBeClickable(objPrivilegiosUsuarios.btnBuscar()));

        //se valida titulo de pantalla principal
        Assert.assertTrue(objPrivilegiosUsuarios.getPrivilegiosUsuario().contentEquals("Privilegios de usuario"));
        //Se valida se modifiquen campos
        Assert.assertTrue(objPrivilegiosUsuarios.getTipo().contentEquals(tipoPrivMod.toUpperCase()));
        Assert.assertTrue(objPrivilegiosUsuarios.getDescripcion().contentEquals(descripcionMod.toUpperCase()));
        Assert.assertTrue(objPrivilegiosUsuarios.getUsuarioTabla().contentEquals(usuarioPriv.toUpperCase()));
        Assert.assertTrue(objPrivilegiosUsuarios.getValor().contentEquals(valorMod.toUpperCase()));
        Assert.assertTrue(objPrivilegiosUsuarios.getValor2().contentEquals(valor2Mod.toUpperCase()));
        Assert.assertTrue(objPrivilegiosUsuarios.getValor3().contentEquals(valor3Mod.toUpperCase()));

    }

    /*******************************************************************************
     *Nombre: Borrar Privilegios usuarios
     *Descripción: Se borra privilegios a usuarios
     *Objetivos de la prueba:
     - Modificar  privilegios a un usuario
     - Validar se guarden cambios
     *Autor: Paulina Alejandra Pérez Ochoa.
     *Fecha de creacion: 06-10-2020.
     *Modificado por: N/A
     *Fecha de modificación: N/A
     *******************************************************************************/
    @Test(priority = 5)
    public void test_borrarPrivilegio() {
        //Se inicia sesion en la aplicacion recaudador
        objLogin.login(userName, password);
        //Se espera a que se encuentre clikeable el link de administracion de la aplicacion
        w.until(ExpectedConditions.elementToBeClickable(objMenu.lnkAdministracionAplicacion()));
        //Se valida el inicio de sesion
        Assert.assertEquals(objMenu.getMenu(), "Menú");
        //Se ingresa al menu administracion de la aplicacion
        objMenu.clicAdministracionAplicacion();
        //Se espera a que se encuentre clickeable el link privilegios de usuarios
        w.until(ExpectedConditions.elementToBeClickable(objPrivilegiosUsuarios.lnk_privilegiosUsuarios()));
        //Se ingresa a privilegios usuarios
        objPrivilegiosUsuarios.clicPrivilegiosUsuarios();
        //Se espera carge la pagina validando se hablite el boton de agreagar
        w.until(ExpectedConditions.elementToBeClickable(objPrivilegiosUsuarios.btnAgregar()));
        //Se valida ingreso a la pantalla privilegios usuarios
        Assert.assertTrue(objPrivilegiosUsuarios.getPrivilegiosUsuario().contentEquals("Privilegios de usuario"));
        //Se busca usuario
        objPrivilegiosUsuarios.setUsuario(usuarioPriv);
        //Se espera carge la pagina validando se hablite el boton de buscar
        w.until(ExpectedConditions.elementToBeClickable(objPrivilegiosUsuarios.btnBuscar()));
        //Se realiza consulta descripcion, valor y tipo de privilegio
        objPrivilegiosUsuarios.setValor(valorMod);
        objPrivilegiosUsuarios.setTipoPrivilegio(tipoPrivMod);
        objPrivilegiosUsuarios.setDescripcion(descripcionMod);
        //Clic en boton buscar
        objPrivilegiosUsuarios.clicBuscar();
        //Espera a que carguen elementos
        w.until(ExpectedConditions.elementToBeClickable(objPrivilegiosUsuarios.btnBuscar()));
        w.until(ExpectedConditions.elementToBeClickable(objPrivilegiosUsuarios.btnAgregar()));
        //Clic en el boton borrar
        objPrivilegiosUsuarios.clicBorrar();
        //Validar regreso a la pantalla
        Assert.assertTrue(objPrivilegiosUsuarios.getTituloModificar().contentEquals("Privilegios de usuario"));
        //Se consulta de nuevo el privilegio para asegurar se borre registro
        objPrivilegiosUsuarios.setValor(valorMod);
        objPrivilegiosUsuarios.setTipoPrivilegio(tipoPrivMod);
        objPrivilegiosUsuarios.setDescripcion(descripcionMod);
        //se asegura que se borre el registro
        Assert.assertTrue(objPrivilegiosUsuarios.getNoRegistro().contentEquals("No hay registros que mostrar"));



    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }
}
