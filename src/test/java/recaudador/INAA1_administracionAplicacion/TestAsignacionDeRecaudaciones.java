package recaudador.INAA1_administracionAplicacion;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import recaudador.INAA1_administracionAplicacion.pageObjects.AsignacionDeRecaudaciones;
import recaudador.INAA1_administracionAplicacion.pageObjects.DatosEntradaAdmonAplicacion;
import recaudador.INLO1_login.pageObjects.Login;
import recaudador.INLO1_login.pageObjects.Menu;

import java.io.IOException;

@Test(groups = {"TAB", "DGO", "CUU"})
public class TestAsignacionDeRecaudaciones extends DatosEntradaAdmonAplicacion {

    public WebDriver driver;
    Login objLogin;
    Menu objMenu;
    WebDriverWait w;
    AsignacionDeRecaudaciones objAsignacionDeRecaudaciones;

    @BeforeMethod
    public void setup() throws IOException {
        //Se inicializa driver
        driver = initializerDriver();
        //Se maximiza el navegador
        driver.manage().window().maximize();

        //Se carga las variables del properties
        //Flta cargar variables al properties

        //Se crean los objetos de las clases a utilizar en las pruebas
        objLogin = new Login(driver);
        objMenu = new Menu(driver);
        objAsignacionDeRecaudaciones = new AsignacionDeRecaudaciones(driver);
        //Se ingresa a recaudador
        driver.get(url);
        //Se crea objeto de un explicit wait
        w = new WebDriverWait(driver, 10);
        w.until(ExpectedConditions.elementToBeClickable(objLogin.btnInicioSesion()));
        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }

}
