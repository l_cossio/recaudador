package recaudador.INAA1_administracionAplicacion.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class CatalogoModulos {

    public CatalogoModulos(WebDriver driver) {

        this.driver = driver;

        PageFactory.initElements(driver, this);

    }

    //VARIABLES

    //se declara el WebDriver
    WebDriver driver;

    //SECCION PRINCIPAL

    //botones
    @FindBy(css = "button[type='button']")
    WebElement btn_agregarModulo;

    //etiquetas
    @FindBy(css = "span[class='xdk']")
    WebElement lbl_titulo;

    @FindBy(css = "#j_id_id5\\:j_id_id53 > table.x7c > tbody > tr:nth-child(2) > td:nth-child(2)")
    WebElement lbl_clavePadre;

    @FindBy(css = "#j_id_id5\\:j_id_id53 > table.x7c > tbody > tr:nth-child(2) > td:nth-child(3)")
    WebElement lbl_nombreCortoPadre;

    @FindBy(css = "#j_id_id5\\:j_id_id53 > table.x7c > tbody > tr:nth-child(2) > td:nth-child(4)")
    WebElement lbl_tipoPadre;

    @FindBy(css = "#j_id_id5\\:j_id_id53 > table.x7c > tbody > tr:nth-child(2) > td:nth-child(5)")
    WebElement lbl_controladorPadre;

    @FindBy(css = "#j_id_id5\\:j_id_id53 > table.x7c > tbody > tr:nth-child(2) > td:nth-child(6)")
    WebElement lbl_transicionPadre;

    @FindBy(css = "#j_id_id5\\:j_id_id53 > table.x7c > tbody > tr:nth-child(2) > td:nth-child(7)")
    WebElement lbl_ordenPadre;

    @FindBy(css = "#j_id_id5\\:j_id_id53 > table.x7c > tbody > tr:nth-child(2) > td:nth-child(8)")
    WebElement lbl_implementacionPadre;

    @FindBy(css = "#j_id_id5\\:j_id_id53 > table.x7c > tbody > tr:nth-child(2) > td:nth-child(9)")
    WebElement lbl_cajaDiversoPadre;

    @FindBy(css = "#j_id_id5\\:j_id_id53 > table.x7c > tbody > tr:nth-child(3) > td:nth-child(2)")
    WebElement lbl_claveHijo;

    @FindBy(css = "#j_id_id5\\:j_id_id53 > table.x7c > tbody > tr:nth-child(3) > td:nth-child(3)")
    WebElement lbl_nombreCortoHijo;

    @FindBy(css = "#j_id_id5\\:j_id_id53 > table.x7c > tbody > tr:nth-child(3) > td:nth-child(4)")
    WebElement lbl_tipoHijo;

    @FindBy(css = "#j_id_id5\\:j_id_id53 > table.x7c > tbody > tr:nth-child(3) > td:nth-child(5)")
    WebElement lbl_controladorHijo;

    @FindBy(css = "#j_id_id5\\:j_id_id53 > table.x7c > tbody > tr:nth-child(3) > td:nth-child(6)")
    WebElement lbl_transicionHijo;

    @FindBy(css = "#j_id_id5\\:j_id_id53 > table.x7c > tbody > tr:nth-child(3) > td:nth-child(7)")
    WebElement lbl_ordenHijo;

    @FindBy(css = "#j_id_id5\\:j_id_id53 > table.x7c > tbody > tr:nth-child(3) > td:nth-child(8)")
    WebElement lbl_implementacionHijo;

    @FindBy(css = "#j_id_id5\\:j_id_id53 > table.x7c > tbody > tr:nth-child(3) > td:nth-child(9)")
    WebElement lbl_cajaDiversoHijo;

    //links
    @FindBy(css = "#j_id_id5\\:j_id_id53 > table.x7c > tbody > tr:nth-child(2) > td:nth-child(1) > div > a.xi")
    WebElement lnk_nombrePadre;

    @FindBy(css = "#j_id_id5\\:j_id_id53 > table.x7c > tbody > tr:nth-child(2) > td:nth-child(10) > a > img")
    WebElement lnk_insertaHPadre;

    @FindBy(css = "img[src='/re_admonaplicacion/images/delete.gif']")
    WebElement lnk_borrarModulo;

    @FindBy(css = "#j_id_id5\\:j_id_id53 > table.x7c > tbody > tr:nth-child(3) > td:nth-child(1) > div > a.xi")
    WebElement lnk_nombreHijo;

    @FindBy(linkText ="Catálogo de módulos")
    WebElement lnk_catalogoModulos;

    @FindBy(css = "#j_id_id5\\:j_id_id53\\:0\\:hgi > img")
    WebElement pr;

    @FindBy(linkText = "Siguientes 1")
    WebElement btn_siguiente;

    //AGREGAR PADRE

    //campos de texto
    @FindBy(css="#j_id_id5\\:j_id_id44")
    WebElement txt_clavePadre;

    @FindBy(css="#j_id_id5\\:j_id_id47")
    WebElement txt_nombrePadre;

    @FindBy(css="#j_id_id5\\:j_id_id49")
    WebElement txt_nombreCPadre;

    @FindBy(css="#j_id_id5\\:j_id_id55")
    WebElement txt_transicionPadre;

    @FindBy(css="#j_id_id5\\:j_id_id57")
    WebElement txt_ordenPadre;

    @FindBy(css="#j_id_id5\\:j_id_id62")
    WebElement txt_cajaDiversoPadre;

    //listas valores
    @FindBy(css="#j_id_id5\\:j_id_id51")
    WebElement lst_tipoPadre;
    Select vlr_tipoPadre;

    @FindBy(css="#j_id_id5\\:j_id_id53")
    WebElement lst_controladorPadre;
    Select vlr_controladorPadre;

    @FindBy(css="#j_id_id5\\:j_id_id60")
    WebElement lst_implementacionPadre;
    Select vlr_implementacionPadre;

    //botones
    @FindBy(css = "button[type='button'][onclick*='submitForm'][onclick*='j_id_id5:j_id_id74']")
    WebElement btn_crearPadre;

    @FindBy(css = "button[type='button'][onclick*='return _chain']")
    WebElement btn_cancelarPadre;

    //MODIFICAR PADRE

    //campos de texto
    @FindBy(css="#j_id_id5\\:j_id_id44")
    WebElement txt_clavePadreM;

    @FindBy(css="#j_id_id5\\:j_id_id47")
    WebElement txt_nombrePadreM;

    @FindBy(css="#j_id_id5\\:j_id_id49")
    WebElement txt_nombreCPadreM;

    @FindBy(css="#j_id_id5\\:j_id_id55")
    WebElement txt_transicionPadreM;

    @FindBy(css="#j_id_id5\\:j_id_id57")
    WebElement txt_ordenPadreM;

    @FindBy(css="#j_id_id5\\:j_id_id62")
    WebElement txt_cajaDiversoPadreM;

    //listas valores
    @FindBy(css="#j_id_id5\\:j_id_id53")
    WebElement lst_tipoPadreM;
    Select vlr_tipoPadreM;

    @FindBy(css="#j_id_id5\\:j_id_id51")
    WebElement lst_controladorPadreM;
    Select vlr_controladorPadreM;

    @FindBy(css="#j_id_id5\\:j_id_id60")
    WebElement lst_implementacionPadreM;
    Select vlr_implementacionPadreM;

    //botones
    @FindBy(css = "button[type='button'][onclick*='submitForm'][onclick*='j_id_id5:j_id_id74']")
    WebElement btn_crearPadreM;

    @FindBy(css = "button[type='button'][onclick*='return _chain']")
    WebElement btn_cancelarPadreM;

    //AGREGAR HIJO

    //campos de texto
    @FindBy(css="#j_id_id5\\:j_id_id46")
    WebElement txt_claveHijo;

    @FindBy(css="#j_id_id5\\:j_id_id49")
    WebElement txt_nombreHijo;

    @FindBy(css="#j_id_id5\\:j_id_id51")
    WebElement txt_nombreCHijo;

    @FindBy(css="#j_id_id5\\:j_id_id57")
    WebElement txt_transicionHijo;

    @FindBy(css="#j_id_id5\\:j_id_id59")
    WebElement txt_ordenHijo;

    @FindBy(css="#j_id_id5\\:j_id_id64")
    WebElement txt_cajaDiversoHijo;

    //listas valores
    @FindBy(css="#j_id_id5\\:j_id_id53")
    WebElement lst_tipoHijo;
    Select vlr_tipoHijo;

    @FindBy(css="#j_id_id5\\:j_id_id55")
    WebElement lst_controladorHijo;
    Select vlr_controladorHijo;

    @FindBy(css="#j_id_id5\\:j_id_id62")
    WebElement lst_implementacionHijo;
    Select vlr_implementacionHijo;

    //botones
    @FindBy(css="button[type='button'][onclick*='submitForm'][onclick*='j_id_id5:j_id_id76']")
    WebElement btn_crearHijo;

    @FindBy(css="button[type='button'][onclick*='submitForm'][onclick*='confirmaBorrado']")
    WebElement btn_cancelarHijo;

    //MODIFICAR HIJO

    //campos de texto
    @FindBy(css = "#j_id_id5\\:j_id_id44")
    WebElement txt_claveHijoM;

    @FindBy(css = "#j_id_id5\\:j_id_id47")
    WebElement txt_nombreHijoM;

    @FindBy(css = "#j_id_id5\\:j_id_id49")
    WebElement txt_nombreCHijoM;

    @FindBy(css = "#j_id_id5\\:j_id_id55")
    WebElement txt_transicionHijoM;

    @FindBy(css = "#j_id_id5\\:j_id_id57")
    WebElement txt_ordenHijoM;

    @FindBy(css = "#j_id_id5\\:j_id_id62")
    WebElement txt_cajaDiversoHijoM;

    //listas valores
    @FindBy(css="#j_id_id5\\:j_id_id53")
    WebElement lst_tipoHijoM;
    Select vlr_tipoHijoM;

    @FindBy(css="#j_id_id5\\:j_id_id51")
    WebElement lst_controladorHijoM;
    Select vlr_controladorHijoM;

    @FindBy(css="#j_id_id5\\:j_id_id60")
    WebElement lst_implementacionHijoM;
    Select vlr_implementacionHijoM;

    //botones
    @FindBy(css="button[type='button'][onclick*='submitForm'][onclick*='j_id_id5:j_id_id74']")
    WebElement btn_crearHijoM;

    @FindBy(css="button[type='button'][onclick*='submitForm'][onclick*='confirmaBorrado']")
    WebElement btn_cancelarHijoM;

    //METODOS

    //Seccion principal

    public void clicAgregarModulo(){
        btn_agregarModulo.click();
    }

    public String getClaveP(){
        return lbl_clavePadre.getText();
    }

    public String getNombreCortoP(){
        return lbl_nombreCortoPadre.getText();
    }

    public String getTipoP(){
        return lbl_tipoPadre.getText();
    }

    public String getControlP(){
        return lbl_controladorPadre.getText();
    }

    public String getTransP(){
        return lbl_transicionPadre.getText();
    }

    public String getOrdenP(){
        return lbl_ordenPadre.getText();
    }

    public String getImplementaP(){
        return lbl_implementacionPadre.getText();
    }

    public String getCajaDiversoP(){
        return lbl_cajaDiversoPadre.getText();
    }

    public void clicInsertaHijo(){
        lnk_insertaHPadre.click();
    }

    public String getClaveH(){ return lbl_claveHijo.getText(); }

    public String getNombreCortoH(){ return lbl_nombreCortoHijo.getText(); }

    public String getTipoH(){ return lbl_tipoHijo.getText(); }

    public String getTransicionH(){ return lbl_transicionHijo.getText(); }

    public String getOrdenH(){ return lbl_ordenHijo.getText(); }

    public String getImplementaH(){ return lbl_implementacionHijo.getText(); }

    public String getCajaDiversoH(){ return lbl_cajaDiversoHijo.getText(); }

    public void clicNombreHijo(){ lnk_nombreHijo.click(); }

    public void crearModuloPadre(String clavePadre, String nombrePadre, String nombreCPadre, String tipoPadre,
                                 String controladorPadre, String transicionPadre, String ordenPadre, String implementacionPadre,
                                 String cajaDiversoPadre){
        txt_clavePadre.sendKeys(clavePadre);
        txt_nombrePadre.sendKeys(nombrePadre);
        txt_nombreCPadre.sendKeys(nombreCPadre);
        vlr_tipoPadre = new Select(lst_tipoPadre);
        vlr_tipoPadre.selectByVisibleText(tipoPadre);
        vlr_controladorPadre = new Select(lst_controladorPadre);
        vlr_controladorPadre.selectByVisibleText(controladorPadre);
        txt_transicionPadre.sendKeys(transicionPadre);
        txt_ordenPadre.sendKeys(ordenPadre);
        vlr_implementacionPadre = new Select(lst_implementacionPadre);
        vlr_implementacionPadre.selectByVisibleText(implementacionPadre);
        txt_cajaDiversoPadre.sendKeys(cajaDiversoPadre);
    }

    public void modificarModuloPadre(String clavePadre, String nombrePadre, String nombreCPadre, String tipoPadre,
                                String controladorPadre, String transicionPadre, String ordenPadre, String implementacionPadre,
                                String cajaDiversoPadre){
        txt_clavePadreM.clear();
        txt_nombrePadreM.clear();
        txt_nombreCPadreM.clear();
        txt_transicionPadreM.clear();
        txt_ordenPadreM.clear();
        txt_cajaDiversoPadreM.clear();

        txt_clavePadreM.sendKeys(clavePadre);
        txt_nombrePadreM.sendKeys(nombrePadre);
        txt_nombreCPadreM.sendKeys(nombreCPadre);
        vlr_tipoPadreM = new Select(lst_tipoPadreM);
        vlr_tipoPadreM.selectByVisibleText(tipoPadre);
        vlr_controladorPadreM = new Select(lst_controladorPadreM);
        vlr_controladorPadreM.selectByVisibleText(controladorPadre);
        txt_transicionPadreM.sendKeys(transicionPadre);
        txt_ordenPadreM.sendKeys(ordenPadre);
        vlr_implementacionPadreM = new Select(lst_implementacionPadreM);
        vlr_implementacionPadreM.selectByVisibleText(implementacionPadre);
        txt_cajaDiversoPadreM.sendKeys(cajaDiversoPadre);
    }

    public void crearModuloHijo(String claveHijo, String nombreHijo, String nombreCHijo, String tipoHijo,
                                String controladorHijo, String transicionHijo, String ordenHijo, String implementacionHijo,
                                String cajaDiversoHijo){
        txt_claveHijo.sendKeys(claveHijo);
        txt_nombreHijo.sendKeys(nombreHijo);
        txt_nombreCHijo.sendKeys(nombreCHijo);
        vlr_tipoHijo = new Select(lst_tipoHijo);
        vlr_tipoHijo.selectByVisibleText(tipoHijo);
        vlr_controladorHijo = new Select(lst_controladorHijo);
        vlr_controladorHijo.selectByVisibleText(controladorHijo);
        txt_transicionHijo.sendKeys(transicionHijo);
        txt_ordenHijo.sendKeys(ordenHijo);
        vlr_implementacionHijo = new Select(lst_implementacionHijo);
        vlr_implementacionHijo.selectByVisibleText(implementacionHijo);
        txt_cajaDiversoHijo.sendKeys(cajaDiversoHijo);
    }

    public void modicarModuloHijo(String claveHijo, String nombreHijo, String nombreCHijo, String tipoHijo,
                                  String controladorHijo, String transicionHijo, String ordenHijo, String implementacionHijo,
                                  String cajaDiversoHijo){
        txt_claveHijoM.clear();
        txt_nombreHijoM.clear();
        txt_nombreCHijoM.clear();
        txt_transicionHijoM.clear();
        txt_ordenHijoM.clear();
        txt_cajaDiversoHijoM.clear();

        txt_claveHijoM.sendKeys(claveHijo);
        txt_nombreHijoM.sendKeys(nombreHijo);
        txt_nombreCHijoM.sendKeys(nombreCHijo);
        vlr_tipoHijoM = new Select(lst_tipoHijoM);
        vlr_tipoHijoM.selectByVisibleText(tipoHijo);
        vlr_controladorHijoM = new Select(lst_controladorHijoM);
        vlr_controladorHijoM.selectByVisibleText(controladorHijo);
        txt_transicionHijoM.sendKeys(transicionHijo);
        txt_ordenHijoM.sendKeys(ordenHijo);
        vlr_implementacionHijoM = new Select(lst_implementacionHijoM);
        vlr_implementacionHijoM.selectByVisibleText(implementacionHijo);
        txt_cajaDiversoHijoM.sendKeys(cajaDiversoHijo);
    }

    public int getNumModuPadre(String nombreModuPadre){
        return driver.findElements(By.linkText(nombreModuPadre)).size();
    }

    public void encontrarModuPadre(String nombreModuPadre){
        while (1==1){
            if (driver.findElements(By.linkText(nombreModuPadre)).size()>0) {
                break;
            }else{
                btn_siguiente.click();
            }
        }

    }
    public void clicCrearPadre(){
        btn_crearPadre.click();
    }

    public WebElement btnCrearPadre(){
        return btn_crearPadre;
    }

    public void clicCancelarPadre(){
        btn_cancelarPadre.click();
        driver.switchTo().alert().accept();
    }

    public void clicCancelarHijo(){
        btn_cancelarHijo.click();
        driver.switchTo().alert().accept();
    }

    public void borrarModulo(){
        lnk_borrarModulo.click();
        driver.switchTo().alert().accept();
    }

    public void clicCrearHijo(){
        btn_crearHijo.click();
    }

    public void clicModificarHijo(){
        btn_crearHijoM.click();
    }

    public void clicNombrePadre(){
        lnk_nombrePadre.click();
    }

    public void clicCataModu() { lnk_catalogoModulos.click(); }

    public String getTitulo() { return lbl_titulo.getText(); }


}
