package recaudador.INAA1_administracionAplicacion.pageObjects;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import recaudador.Base;

public class DatosEntradaAdmonAplicacion extends Base{
	
	//Variables agregar usuario
	public String usuario1;
	public String usuario2;
	public String usuario3;
	public String nombre;
	public String puesto;
	public String numEmpleado;
	public String contrasena;
	public String confirmarContrasena;
	public String pregunta;
	public String respuesta;
	public String estatusActivo;
	public String estatusInactivo;
	public String estatusNoAplica;
	public String tipoUsuarioLocal;
	public String tipoUsuarioExterno;
	public String tipoUsuarioRemoto;

	//Variables Modificar usuario
	public String usuarioMod1;
	public String usuarioMod2;
	public String usuarioMod3;
	public String nombreMod1;
	public String nombreMod2;
	public String nombreMod3;
	public String nuevoNombre;
	public String puestoMod;
	public String numEmpleadoMod;
	public String preguntaMod;
	public String respuestaMod;

	//variables restablecer contraseña
	public String usuarioRestablecerPassActivo;
	public String usuarioRestablecerPassInactivo;
	public String usuarioRestablecerPassNoAplica;

	//Variables Catálogo de Módulos
	public String claveModuP1;
	public String nombreModuP1;
	public String nombreCortoModuP1;
	public String controladorModuP1;
	public String tipoModuP1;
	public String transicionModuP1;
	public String ordenModuP1;
	public String implementacionModuP1;
	public String cajaDiversoModuP1;
	public String claveModuP2;
	public String nombreModuP2;
	public String nombreCortoModuP2;
	public String controladorModuP2;
	public String tipoModuP2;
	public String transicionModuP2;
	public String ordenModuP2;
	public String implementacionModuP2;
	public String cajaDiversoModuP2;
	public String claveModuH1;
	public String nombreModuH1;
	public String nombreCortoModuH1;
	public String controladorModuH1;
	public String tipoModuH1;
	public String transicionModuH1;
	public String ordenModuH1;
	public String implementacionModuH1;
	public String cajaDiversoModuH1;
	public String claveModuH2;
	public String nombreModuH2;
	public String nombreCortoModuH2;
	public String controladorModuH2;
	public String tipoModuH2;
	public String transicionModuH2;
	public String ordenModuH2;
	public String implementacionModuH2;
	public String cajaDiversoModuH2;

	//Variables privilegios Usuarios
	public String usuarioPriv;
	public String tipoPriv;
	public String descripcionPriv;
	public String valor;
	public String valor2;
	public String valor3;
	public String usuarioCon;
	public String filtroDesc;
	public String filtroValor;
	public String filtroTipo;
	public String tipoPrivMod;
	public String descripcionMod;
	public String valorMod;
	public String valor2Mod;
	public String valor3Mod;

	//Variables catálogo roles
	public String nombreRol1;
	public String descripcionRol1;
	public String nombreRol2;
	public String descripcionRol2;



	// Metodo Carga de variables
    public void cargaVariablesCatalogoUsuarios() throws IOException {
    	
    	Properties prop = new Properties();
    	FileInputStream fis = new FileInputStream
				("src\\test\\java\\recaudador\\INAA1_administracionAplicacion\\dataBank\\CatalogoUsuarios.properties");
    	
    	prop.load(fis);
    	//agregar usuario
    	usuario1 = prop.getProperty("usuario1");
    	usuario2 = prop.getProperty("usuario2");
    	usuario3 = prop.getProperty("usuario3");
    	nombre = prop.getProperty("nombre");
    	puesto = prop.getProperty("puesto");
    	numEmpleado = prop.getProperty("numEmpleado");
    	contrasena = prop.getProperty("contrasena");
    	confirmarContrasena = prop.getProperty("confirmarContrasena");
    	pregunta = prop.getProperty("pregunta");
    	respuesta = prop.getProperty("respuesta");
    	estatusNoAplica = prop.getProperty("estatusNoAplica");
    	estatusActivo = prop.getProperty("estatusActivo");
    	estatusInactivo = prop.getProperty("estatusInactivo");
    	tipoUsuarioLocal = prop.getProperty("tipoUsuarioLocal");
    	tipoUsuarioExterno = prop.getProperty("tipoUsuarioExterno");
    	tipoUsuarioRemoto = prop.getProperty("tipoUsuarioRemoto");
    	//Modificar usuario 
    	usuarioMod1 = prop.getProperty("usuarioMod1");
    	usuarioMod2= prop.getProperty("usuarioMod2");
    	usuarioMod3 = prop.getProperty("usuarioMod3");
    	nombreMod1 = prop.getProperty("nombreMod1");
    	nombreMod2 = prop.getProperty("nombreMod2");
    	nombreMod3 = prop.getProperty("nombreMod3");
    	nuevoNombre = prop.getProperty("nuevoNombre");
    	puestoMod = prop.getProperty("puestoMod");
    	numEmpleadoMod = prop.getProperty("numEmpleadoMod");
    	preguntaMod = prop.getProperty("preguntaMod");
    	respuestaMod = prop.getProperty("respuestaMod");
    	//restablecer contraseña
    	usuarioRestablecerPassActivo = prop.getProperty("usuarioRestablecerPassActivo");
    	usuarioRestablecerPassInactivo = prop.getProperty("usuarioRestablecerPassInactivo");
    	usuarioRestablecerPassNoAplica = prop.getProperty("usuarioRestablecerPassNoAplica");
    	
    }

    //Método para cargar las variables de Catálogo de Módulos.
	public void CargaVariablesCataModu() throws IOException {
		Properties prop = new Properties();
		FileInputStream fis = new FileInputStream
				("src\\test\\java\\recaudador\\INAA1_administracionAplicacion\\dataBank\\CatalogoModulos.properties");

		prop.load(fis);
		//agregar módulo
		claveModuP1 = prop.getProperty("claveModuP1");
		nombreModuP1 = prop.getProperty("nombreModuP1");
		nombreCortoModuP1 = prop.getProperty("nombreCortoModuP1");
		controladorModuP1 = prop.getProperty("controladorModuP1");
		tipoModuP1 = prop.getProperty("tipoModuP1");
		transicionModuP1 = prop.getProperty("transicionModuP1");
		ordenModuP1 = prop.getProperty("ordenModuP1");
		implementacionModuP1 = prop.getProperty("implementacionModuP1");
		cajaDiversoModuP1 = prop.getProperty("cajaDiversoModuP1");
		claveModuP2 = prop.getProperty("claveModuP2");
		nombreModuP2 = prop.getProperty("nombreModuP2");
		nombreCortoModuP2 = prop.getProperty("nombreCortoModuP2");
		controladorModuP2 = prop.getProperty("controladorModuP2");
		tipoModuP2 = prop.getProperty("tipoModuP2");
		transicionModuP2 = prop.getProperty("transicionModuP2");
		ordenModuP2 = prop.getProperty("ordenModuP2");
		implementacionModuP2 = prop.getProperty("implementacionModuP2");
		cajaDiversoModuP2 = prop.getProperty("cajaDiversoModuP2");
		claveModuH1 = prop.getProperty("claveModuH1");
		nombreModuH1 = prop.getProperty("nombreModuH1");
		nombreCortoModuH1 = prop.getProperty("nombreCortoModuH1");
		controladorModuH1 = prop.getProperty("controladorModuH1");
		tipoModuH1 = prop.getProperty("tipoModuH1");
		transicionModuH1 = prop.getProperty("transicionModuH1");
		ordenModuH1 = prop.getProperty("ordenModuH1");
		implementacionModuH1 = prop.getProperty("implementacionModuH1");
		cajaDiversoModuH1 = prop.getProperty("cajaDiversoModuH1");
		claveModuH2 = prop.getProperty("claveModuH2");
		nombreModuH2 = prop.getProperty("nombreModuH2");
		nombreCortoModuH2 = prop.getProperty("nombreCortoModuH2");
		controladorModuH2 = prop.getProperty("controladorModuH2");
		tipoModuH2 = prop.getProperty("tipoModuH2");
		transicionModuH2 = prop.getProperty("transicionModuH2");
		ordenModuH2 = prop.getProperty("ordenModuH2");
		implementacionModuH2 = prop.getProperty("implementacionModuH2");
		cajaDiversoModuH2 = prop.getProperty("cajaDiversoModuH2");
	}

	//Metodo Carga de variables para privilegios
	public void cargaVariablesPrivilegiosUsuarios() throws IOException {
		Properties prop = new Properties();
		FileInputStream fis = new FileInputStream
				("src\\test\\java\\recaudador\\INAA1_administracionAplicacion\\dataBank\\PrivilegiosUsuarios.properties");

		prop.load(fis);
		//Crear Privilegios
		usuarioPriv = prop.getProperty("usuarioPriv");
		tipoPriv = prop.getProperty("tipoPriv");
		descripcionPriv = prop.getProperty("descripcionPriv");
		valor = prop.getProperty("valor");
		valor2 = prop.getProperty("valor2");
		valor3 = prop.getProperty("valor3");
		//Consulta privilegios
		usuarioCon = prop.getProperty("usuarioCon");
		filtroDesc = prop.getProperty("filtroDesc");
		filtroValor = prop.getProperty("filtroValor");
		filtroTipo = prop.getProperty("filtroTipo");
		//Modificar Privilegios
		tipoPrivMod = prop.getProperty("tipoPrivMod");
		descripcionMod = prop.getProperty("descripcionMod");
		valorMod = prop.getProperty("valorMod");
		valor2Mod = prop.getProperty("valor2Mod");
		valor3Mod = prop.getProperty("valor3Mod");




	}

	//Método para cargar variables del Catálogo de Roles.
	public void cargaVariablesCataRol() throws IOException {
    	Properties prop = new Properties();
    	FileInputStream fis = new FileInputStream
				("src\\test\\java\\recaudador\\INAA1_administracionAplicacion\\dataBank\\CatalogoRoles.properties");

    	prop.load(fis);
    	nombreRol1 = prop.getProperty("nombreRol1");
    	descripcionRol1 = prop.getProperty("descripcionRol1");
    	nombreRol2 = prop.getProperty("nombreRol2");
		descripcionRol2 = prop.getProperty("descripcionRol2");
	}
}

