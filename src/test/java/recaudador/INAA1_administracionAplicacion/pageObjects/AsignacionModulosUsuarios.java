package recaudador.INAA1_administracionAplicacion.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AsignacionModulosUsuarios {

    //Se declara constructor de la clase
    public AsignacionModulosUsuarios(WebDriver driver) {

        this.driver = driver;
        PageFactory.initElements(driver, this);

    }
    //VARIABLES

    //se declara el WebDriver
    WebDriver driver;

    //MODULO
    @FindBy(css = "a[id='cl2j_id_4']")
    WebElement lnk_modulosUsuarios;

    //SELECCION USUARIO

    //campos de texto
    @FindBy(css = "input[id='pt1:itUsername::content']")
    WebElement txt_usuario;

    @FindBy(css = "input[id='pt1:itNombre::content']")
    WebElement txt_nombre;

    //botones
    @FindBy(css = "button[id='pt1:cbBuscar']")
    WebElement btn_buscarUsuario;

    //etiquetas
    @FindBy(css = "div[title='Asignación de módulos a usuarios'] > h2")
    WebElement lbl_AsignacionModulosUsuarios;

    @FindBy(css = "tr[id='pt1:plUsername'] > td:last-child")
    WebElement lbl_usuario;

    @FindBy(css = "tr[id='pt1:plam1'] > td:last-child")
    WebElement lbl_nombreUsuario;

    @FindBy(css = "#pt1\\:plam2 > td.xrj.xpq")
    WebElement lbl_numeroEmpleado;

    @FindBy(css = "#pt1\\:plam3 > td.xrj.xpq")
    WebElement lbl_puesto;

    @FindBy(css = "#pt1\\:plam4 > td.xrj.xpq")
    WebElement lbl_estatus;

    //POR ROL

    //campos de texto
    @FindBy(css = "input[id='pt1:it3::content']")
    WebElement txt_rol;

    @FindBy(css = "input[id='pt1:it4::content']")
    WebElement txt_rolDescripcion;

    //botones
    @FindBy(css = "button[id='pt1:cb4']")
    WebElement btn_buscarRol;

    @FindBy(css = "button[id='pt1:cb6']")
    WebElement btn_limpiarRol;

    @FindBy(css = "button[id='pt1:cb7']")
    WebElement btn_asignarRol;

    @FindBy(css = "button[id='pt1:d11::ok']")
    WebElement btn_aceptarVerModulos;

    @FindBy(css = "button[id='d1::msgDlg::cancel']")
    WebElement btn_aceptarAsignarRol;

    //etiquetas
    @FindBy(css = "table[class='xza x102'] > tbody > tr > td:nth-child(1)")
    WebElement lbl_nombreRol;

    @FindBy(css = "table[class='xza x102'] > tbody > tr > td:nth-child(2)")
    WebElement lbl_descripcionRol;

    @FindBy(css = "div[id='pt1:t2::db']")
    WebElement lbl_limpiarRol;

    @FindBy(css = "div[id='pt1:d11::_ttxt']")
    WebElement lbl_contieneModulos;

    @FindBy(css = "#d1\\:\\:msgDlg\\:\\:_cnt > div > table > tbody > tr > td > table > tbody > tr > td:nth-child(2) > div")
    WebElement lbl_seAsignoRol;

    //links
    @FindBy(css = "a[id='pt1:sdi1::disAcr']")
    WebElement lnk_modulo;

    @FindBy(css = "a[id='pt1:sdi2::disAcr']")
    WebElement lnk_rol;

    @FindBy(css = "a[id='pt1:t2:0:cl2']")
    WebElement lnk_verModulos;

    //POR MODULO

    //botones
    @FindBy(css = "button[id='pt1:cbAdd']")
    WebElement btn_agregarModulo;

    @FindBy(css = "button[id='pt1:cbRemove']")
    WebElement btn_quitarModulo;

    @FindBy(css = "button[id='pt1:cbCommit']")
    WebElement btn_guardarModulos;

    @FindBy(css = "button[id='pt1:cbRollback']")
    WebElement btn_cancelarModulos;

    @FindBy(css = "button[id='pt1:d2::ok']")
    WebElement btn_aceptarAccion;

    //etiquetas

    @FindBy(css = "div[id='pt1:trUsMo::db'] span:nth-child(2)")
    WebElement lbl_moduloPrincipal;

    @FindBy(css = "div[id='pt1:trAllMo::db'] span:nth-child(2)")
    WebElement lbl_seleccionarModuloAsignar;

    @FindBy(css = "div[id='pt1:trAllMo::db'] span:nth-child(2)")
    WebElement lbl_seleccionarModuloDesasignar;

    @FindBy(css = "div[id='pt1:trUsMo::db']")
    WebElement lbl_sinModulosAsignados;

    @FindBy(css = "td[id='pt1:d2::contentContainer']")
    WebElement lbl_accionExitosa;

    //links
    @FindBy(css = "a[id='pt1:trUsMo:0::di']")
    WebElement lnk_ampliarModulos;

    //METODOS

    //PRINCIPAL

    public WebElement lblAsignacionModulosUsuarios() {return lbl_AsignacionModulosUsuarios; }

    public WebElement lnkModulosUsuarios() {return lnk_modulosUsuarios; }

    public void clicModulosUsuarios() { lnk_modulosUsuarios.click(); }

    //SELECCION USUARIO

    //web elements
    public WebElement btnBuscarUsuario() {return btn_buscarUsuario; }

    //campos de texto
    public void setBuscarUsuario(String usuario) {
        txt_usuario.sendKeys(usuario);
    }

    public void setBuscarNombre(String nombre) {
        txt_nombre.sendKeys(nombre);
    }

    //botones
    public void clicBuscarUsuario() {
        btn_buscarUsuario.click();
    }

    //etiquetas
    public String getUsuario() {
        return lbl_usuario.getText();
    }

    public String getNombreUsuario() { return lbl_nombreUsuario.getText(); }

    public String getNumeroEmpleado() { return lbl_numeroEmpleado.getText(); }

    public String getPuesto() { return lbl_puesto.getText(); }

    public String getEstatus() { return lbl_estatus.getText(); }

    public String getTituloPantalla() {
        return lbl_AsignacionModulosUsuarios.getText();
    }

    //POR ROL

    //web elements
    public WebElement lblNombreRol(){ return lbl_nombreRol; }

    public WebElement btnAsignarRol(){ return btn_asignarRol; }

    public WebElement btnAceptarAsignarRol(){ return btn_aceptarAsignarRol; }

    public WebElement lblLimpiarRol() {return lbl_limpiarRol; }

    //campos de texto
    public void setBuscarRol(String rol) {
        txt_rol.sendKeys(rol);
    }

    public void setBuscarDescripcionRol(String descripcion) {
        txt_rolDescripcion.sendKeys(descripcion);
    }

    //botones
    public void clicBuscarRol() {
        btn_buscarRol.click();
    }

    public void clicLimpiarRol() {
        btn_limpiarRol.click();
    }

    public void clicAsignarRol() {
        btn_asignarRol.click();
    }

    public void clicAceptarAsignarRol() { btn_aceptarAsignarRol.click(); }

    public void clicAceptarVerModulos() {
        btn_aceptarVerModulos.click();
    }

    //etiquetas
    public String getNombreRol() {
        return lbl_nombreRol.getText();
    }

    public String getDescripcionRol() {
        return lbl_descripcionRol.getText();
    }

    public String getVerModulos() {
        return lbl_contieneModulos.getText();
    }

    public String getLimpiarRol() { return lbl_limpiarRol.getText(); }

    public String getSeAsignoRol() { return lbl_seAsignoRol.getText(); }

    //links
    public void clicPorRol() {
        lnk_rol.click();
    }

    public void clicPorModulo() {
        lnk_modulo.click();
    }

    public void clicVerModulos() {
        lnk_verModulos.click();
    }

    //POR MODULO

    //web elements
    public WebElement lblRecaudadorEstatal() { return lbl_moduloPrincipal; }

    public WebElement lblSinModulosAsignados() { return lbl_sinModulosAsignados; }

    public WebElement lblAccionExitosa() { return lbl_accionExitosa; }

    //botones
    public void clicAgregarModulo() {
        btn_agregarModulo.click();
    }

    public void clicQuitarModulo() {
        btn_quitarModulo.click();
    }

    public void clicGuardarModulos() {
        btn_guardarModulos.click();
    }

    public void clicCancelarModulos() {
        btn_cancelarModulos.click();
    }

    public void clicAceptarAccion() {
        btn_aceptarAccion.click();
    }

    //etiquetas

    public int cantidadModulos() { return driver.findElements(By.cssSelector("div[id='pt1:trUsMo::db'] > table:nth-child(2) > tbody > tr")).size(); }

    public void clicModuloAsignar() { lbl_seleccionarModuloAsignar.click(); }

    public void clicModuloDesasignar() { lbl_moduloPrincipal.click(); }

    public void clicAmpliarModulos() {
        lnk_ampliarModulos.click();
    }

    public String getSinModulosAsignados() {
        return lbl_sinModulosAsignados.getText();
    }

    public String getAccionExitosa() {
        return lbl_accionExitosa.getText();
    }

}
