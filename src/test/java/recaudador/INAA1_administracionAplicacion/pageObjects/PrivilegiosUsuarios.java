package recaudador.INAA1_administracionAplicacion.pageObjects;

import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;

import java.time.Duration;

public class PrivilegiosUsuarios {

    //Se declara constructor de la clase
    public PrivilegiosUsuarios(WebDriver driver){

        this.driver = driver;

        AjaxElementLocatorFactory factory = new AjaxElementLocatorFactory(driver, 10);
        PageFactory.initElements(factory,this);
        w = new FluentWait(driver)
                .pollingEvery(Duration.ofMillis(200))
                .withTimeout(Duration.ofSeconds(15))
                .ignoring(StaleElementReferenceException.class, Exception.class);

    }

    //VARIABLES

    //se declara el WebDriver
    WebDriver driver;

    FluentWait w;

    //SECCION PRINCIPAL

    //Links

    @FindBy(linkText = "Catálogo de Privilegios de Usuario")
    WebElement lnk_privilegiosUsuarios;

    //Radio Button
    @FindBy(css="input[id='j_id_id5:j_id_id73:1']")
     WebElement btn_seleccionar;

    @FindBy(css="td[class='x7m xbp']")
    WebElement tbl_tabla;
    //Etiquetas
    @FindBy(css = "form[id='j_id_id5'] > table tbody tr td table tbody tr td span")
    WebElement lbl_privilegiosUsuarios;

    @FindBy(css= "#j_id_id5\\:j_id_id73 > table.x7b > tbody > tr:nth-child(2) > td:nth-child(2)")
    WebElement lbl_tipo;

    @FindBy(css= "#j_id_id5\\:j_id_id73 > table.x7b > tbody > tr:nth-child(2) > td:nth-child(3)")
    WebElement lbl_descripcion;

    @FindBy(css= "#j_id_id5\\:j_id_id73 > table.x7b > tbody > tr:nth-child(2) > td:nth-child(4)")
    WebElement lbl_usuario;

    @FindBy(css= "#j_id_id5\\:j_id_id73 > table.x7b > tbody > tr:nth-child(2) > td:nth-child(5)")
    WebElement lbl_valor;

    @FindBy(css= "#j_id_id5\\:j_id_id73 > table.x7b > tbody > tr:nth-child(2) > td:nth-child(6)")
    WebElement lbl_valor2;

    @FindBy(css= "#j_id_id5\\:j_id_id73 > table.x7b > tbody > tr:nth-child(2) > td:nth-child(7)")
    WebElement lbl_valor3;


    //Campos de texto
    @FindBy(css="input[id='j_id_id5:j_id_id60']")
    WebElement txt_descripcion;

    @FindBy(css="input[id='j_id_id5:j_id_id63']")
    WebElement txt_valor;

    //Listas valores
    @FindBy(css="select[id='j_id_id5:j_id_id57']")
    WebElement lst_tipoPrivilegio;
    Select vlr_tipoPrivilegio;

    @FindBy(css="select[id='j_id_id5:navList1']")
    WebElement lst_Usuario;
    Select vlr_Usuario;

    //Botones
    @FindBy(xpath = "//button[text()='Buscar']")
    WebElement btn_buscar;

    @FindBy(xpath = "//button[text()='Agregar']")
    WebElement btn_agregar;

    @FindBy(xpath = "//button[text()='Modificar']")
    WebElement btn_modificar;

    @FindBy(xpath="//button[text()='Borrar']")
    WebElement btn_borrar;

    @FindBy(css="#j_id_id5 > div:nth-child(4) > button")
    WebElement btn_generarReporte;

    //CREAR PRIVILEGIOS

    //Etiquetas
    @FindBy(css = "tbody td table tbody tr td span[class='xdk']" )
    WebElement lbl_Crear;

    //Lista Valores
    @FindBy(css="select[id='j_id_id5:j_id_id44']")
    WebElement lst_tipoPrivilegioCrear;
    Select vlr_tipoPrivilegioCrear;

    //Campos Texto
    @FindBy(css = "input[id='j_id_id5:j_id_id46']")
    WebElement txt_descripcionCrear;

    @FindBy(css= "input[id='j_id_id5:j_id_id48']")
    WebElement txt_valorCrear;

    @FindBy(css = "input[id='j_id_id5:j_id_id50']")
    WebElement txt_valor2;

    @FindBy(css="input[id='j_id_id5:j_id_id52']")
    WebElement txt_valor3;

   //Botones
    @FindBy(xpath = "//button[text()='Crear']")
    WebElement btn_crear;

    @FindBy(xpath = "//button[text()='Guardar cambios']")
    WebElement btn_guardarCambios;

    @FindBy(xpath = "//button[text()='Cancelar']")
    WebElement btn_cancelar;

   //MODIFICAR PRIVILEGIOS

    //Etiquetas
    @FindBy(css = "span[class='xdk']")
    WebElement lbl_modificar;

   //Lista Valores
   @FindBy(css="select[id='j_id_id5:j_id_id44']")
   WebElement lst_tipoPrivilegioMod;
   Select tipoPrivilegioMod;

   // Captura de texto
   @FindBy(css = "input[id='j_id_id5:j_id_id46']")
   WebElement txt_descripcionMod;

    @FindBy(css= "input[id='j_id_id5:j_id_id48']")
    WebElement txt_valorMod;

    @FindBy(css = "input[id='j_id_id5:j_id_id50']")
    WebElement txt_valor2Mod;

    @FindBy(css="input[id='j_id_id5:j_id_id52']")
    WebElement txt_Valor3Mod;

   //Botones

//    @FindBy(css ="#j_id_id5 > div:nth-child(4) > div:nth-child(9) > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td.x51 > button:nth-child(1)")
//    WebElement btn_guardarCambios;

    @FindBy(css="#j_id_id5 > div:nth-child(4) > div:nth-child(9) > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td.x51 > button:nth-child(2)")
    WebElement btn_cancelarMod;

    //BORRAR PRIVILEGIO
    @FindBy(css="td[colspan='6']")
    WebElement lbl_noRegistros;

    //METODOS

    // SECCION PRINCIPAL

    public WebElement lnk_privilegiosUsuarios(){return lnk_privilegiosUsuarios;}

    public WebElement btnAgregar() {return btn_agregar;}

    public WebElement btnBuscar() {return btn_buscar;}

    public WebElement btnModificar() {return btn_modificar;}

    public WebElement btnSeleccionar(){return btn_seleccionar;}



    public void clicPrivilegiosUsuarios(){
        w.until(ExpectedConditions.elementToBeClickable(lnk_privilegiosUsuarios));
        lnk_privilegiosUsuarios.click();
    }

    public String getPrivilegiosUsuario(){
        return lbl_privilegiosUsuarios.getText();
    }

    public String getTipo(){
        return lbl_tipo.getText();
    }

    public String getDescripcion(){
        return lbl_descripcion.getText();
    }

    public String getUsuarioTabla(){
        return lbl_usuario.getText();
    }

    public String getValor(){
        return lbl_valor.getText();
    }

    public String getValor2(){
        return lbl_valor2.getText();
    }

    public String getValor3(){
        return lbl_valor3.getText();
    }

    public void setDescripcion(String descripcion ){
        txt_descripcion.sendKeys(descripcion);
    }

    public void setValor(String valor){
        txt_valor.sendKeys(valor);
    }

    public void setTipoPrivilegio(String privilegio){
        vlr_tipoPrivilegio = new Select (lst_tipoPrivilegio);
        vlr_tipoPrivilegio.selectByVisibleText(privilegio);
    }

    public void setUsuario(String UsuarioPriv){
        vlr_Usuario = new Select (lst_Usuario);
        vlr_Usuario.selectByVisibleText(UsuarioPriv);
    }

    public void borrarDecripcion(){
        txt_descripcion.clear();
    }

    public void borrarValor(){
        txt_valor.clear();
    }


    public void clicBuscar() {
        w.until(ExpectedConditions.elementToBeClickable(btn_buscar));
        btn_buscar.click();
    }

    public void clicAgregar() {
        w.until(ExpectedConditions.elementToBeClickable(btn_agregar));
//        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        btn_agregar.click();
    }

    public void clicModificar() {
        w.until(ExpectedConditions.elementToBeClickable(btn_modificar));
        btn_modificar.click();
    }

    public void clicBorrar() {
        w.until(ExpectedConditions.elementToBeClickable(btn_borrar));
        btn_borrar.click();
        w.until(ExpectedConditions.alertIsPresent());
        driver.switchTo().alert().accept();
    }

    public void setReporte(){
        btn_generarReporte.click();
    }

    //CREAR PRIVILEGIOS

    public WebElement btnCrear() {
        return btn_crear;}

    public String getTituloCrear(){
        return lbl_Crear.getText();
    }

    public void setCrearPrivilegio(String tipoPriv, String descripcionPriv, String valor, String valor2, String valor3){
        vlr_tipoPrivilegioCrear = new Select (lst_tipoPrivilegioCrear);
        vlr_tipoPrivilegioCrear.selectByVisibleText(tipoPriv);
        txt_descripcionCrear.sendKeys(descripcionPriv);
        txt_valorCrear.sendKeys(valor);
        txt_valor2.sendKeys(valor2);
        txt_valor3.sendKeys(valor3);
    }

    public void clicCrear(){
        btn_crear.click();
    }

    public void clicCancelar(){
        btn_cancelar.click();
        w.until(ExpectedConditions.alertIsPresent());
        driver.switchTo().alert().accept();
    }

    // MODIFICAR

    public String getTituloModificar(){
        return lbl_modificar.getText();
    }


    public void borrarMod(){
        txt_descripcionCrear.clear();
        txt_descripcionCrear.clear();
        txt_valorCrear.clear();
        txt_valor2.clear();
        txt_valor3.clear();

    }

    public void setModificarPrivilegio(String tipoPrivilegioMod, String descripcionMod, String valorMod, String valor2Mod, String valor3Mod){
        vlr_tipoPrivilegioCrear = new Select (lst_tipoPrivilegioCrear);
        vlr_tipoPrivilegioCrear.selectByVisibleText(tipoPrivilegioMod);
        txt_descripcionCrear.sendKeys(descripcionMod);
        txt_valorCrear.sendKeys(valorMod);
        txt_valor2.sendKeys(valor2Mod);
        txt_valor3.sendKeys(valor3Mod);

    }

    //BORRAR
    public String getNoRegistro(){
        return lbl_noRegistros.getText();
    }


    public WebElement tbl_tabla(){
        return tbl_tabla;
    }


    public void clicGuardarCambios() {
        w.until(ExpectedConditions.elementToBeClickable(btn_guardarCambios));
        btn_guardarCambios.click();
    }
}
