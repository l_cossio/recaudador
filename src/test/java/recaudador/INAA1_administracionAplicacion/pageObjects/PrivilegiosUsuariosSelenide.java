package recaudador.INAA1_administracionAplicacion.pageObjects;

import com.codeborne.selenide.Configuration;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.WebDriverRunner.setWebDriver;


public class PrivilegiosUsuariosSelenide {

    //Se declara constructor de la clase
    public PrivilegiosUsuariosSelenide(WebDriver driver) {
        this.driver = driver;
        setWebDriver(driver);
        Configuration.timeout = 10000; // millis
    }

    //VARIABLES

    //se declara el WebDriver
    WebDriver driver;

    WebDriverWait w;

    //SECCION PRINCIPAL

    //Links
    By lnk_privilegiosUsuarios = new By.ByLinkText("Catálogo de Privilegios de Usuario");

    //Radio Button
    By btn_seleccionar = new By.ByCssSelector("input[id='j_id_id5:j_id_id73:1']");

    By tbl_tabla = new By.ByCssSelector("td[class='x7m xbp']");

    //Etiquetas
    By lbl_privilegiosUsuarios = new By.ByCssSelector("form[id='j_id_id5'] > table tbody tr td table tbody tr td span");

    By lbl_tipo = new By.ByCssSelector("#j_id_id5\\:j_id_id73 > table.x7b > tbody > tr:nth-child(2) > td:nth-child(2)");

    By lbl_descripcion = new By.ByCssSelector("#j_id_id5\\:j_id_id73 > table.x7b > tbody > tr:nth-child(2) > td:nth-child(3)");

    By lbl_usuario = new By.ByCssSelector("#j_id_id5\\:j_id_id73 > table.x7b > tbody > tr:nth-child(2) > td:nth-child(4)");

    By lbl_valor = new By.ByCssSelector("#j_id_id5\\:j_id_id73 > table.x7b > tbody > tr:nth-child(2) > td:nth-child(5)");

    By lbl_valor2 = new By.ByCssSelector("#j_id_id5\\:j_id_id73 > table.x7b > tbody > tr:nth-child(2) > td:nth-child(6)");

    By lbl_valor3 = new By.ByCssSelector("#j_id_id5\\:j_id_id73 > table.x7b > tbody > tr:nth-child(2) > td:nth-child(7)");


    //Campos de texto
    By txt_descripcion = new By.ById("j_id_id5:j_id_id60");

    By txt_valor = new By.ByCssSelector("#j_id_id5:j_id_id63");

    //Listas valores
    By lst_tipoPrivilegio = new By.ByXPath("select[id='j_id_id5:j_id_id57']");
    Select vlr_tipoPrivilegio;

    By lst_usuario = new By.ByXPath("select[id='j_id_id5:navList1']");
    Select vlr_Usuario;

    //Botones
    By btn_buscar = new By.ByXPath("//button[text()='Buscar']");

    By btn_agregar = new By.ByXPath("//button[text()='Agregar']");

    By btn_modificar = new By.ByXPath("//button[text()='Modificar']");

    By btn_borrar = new By.ByXPath("//button[text()='Borrar']");

    By btn_generarReporte = new By.ByCssSelector("#j_id_id5 > div:nth-child(4) > button");

    //CREAR PRIVILEGIOS

    //Etiquetas
    By lbl_Crear = new By.ByXPath("//tbody/td/table/tbody/tr/td/span[class='xdk']");

    //Lista Valores
    By lst_tipoPrivilegioCrear = new By.ByXPath("select[id='j_id_id5:j_id_id44']");
    Select vlr_tipoPrivilegioCrear;

    //Campos Texto
    By txt_descripcionCrear = new By.ByXPath("input[id='j_id_id5:j_id_id46']");

    By txt_valorCrear = new By.ByXPath("input[id='j_id_id5:j_id_id48']");

    By txt_valor2 = new By.ByXPath("input[id='j_id_id5:j_id_id50']");

    By txt_valor3 = new By.ByXPath("input[id='j_id_id5:j_id_id52']");

    //Botones
    By btn_crear = new By.ByXPath("//button[text()='Crear']");

    By btn_guardarCambios = new By.ByXPath("//button[text()='Guardar cambios']");

    By btn_cancelar = new By.ByXPath("//button[text()='Cancelar']");

    //MODIFICAR PRIVILEGIOS

    //Etiquetas
    By lbl_modificar = new By.ByXPath("span[class='xdk']");

    //Lista Valores
    By lst_tipoPrivilegioMod = new By.ByXPath("select[id='j_id_id5:j_id_id44']");
    Select tipoPrivilegioMod;

    // Captura de texto
    By txt_descripcionMod = new By.ByXPath("input[id='j_id_id5:j_id_id46']");

    By txt_valorMod = new By.ByXPath("input[id='j_id_id5:j_id_id48']");

    By txt_valor2Mod = new By.ByXPath("input[id='j_id_id5:j_id_id50']");

    By txt_Valor3Mod = new By.ByXPath("input[id='j_id_id5:j_id_id52']");

    //Botones

//    By btn_guardarCambios;
//   = new By.ByCssSelector(css ="#j_id_id5 > div:nth-child(4) > div:nth-child(9) > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td.x51 > button:nth-child(1)");

    By btn_cancelarMod = new By.ByCssSelector("#j_id_id5 > div:nth-child(4) > div:nth-child(9) > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td.x51 > button:nth-child(2)");

    //BORRAR PRIVILEGIO
    By lbl_noRegistros = new By.ByCssSelector("td[colspan='6']");

    public void clickAgregar() {
        $(btn_agregar).hover();
        $(btn_agregar).click();
        $(btn_agregar).should(disappear);
        // Valida que se entre a la pantalla correcta
        $(lbl_Crear).shouldHave(text("Privilegios de usuario - Crear"));

    }

    public void clickPrivilegiosUsuarios() {
        $(lnk_privilegiosUsuarios).click();
        $(tbl_tabla).shouldBe(visible);
        $(lbl_privilegiosUsuarios).shouldHave(text("Privilegios de usuario"));
    }

    public void seleccionarUsuario(String text) {
        $(lst_usuario).selectOption(text);
    }

    public void clickBuscar() {
        $(btn_buscar).hover();
        $(btn_buscar).click();
        $(tbl_tabla).shouldBe(visible);
    }

    public void clickCancelar() {
        $(btn_cancelar).click();
        confirm();
        $(tbl_tabla).shouldBe(visible);
        $(lbl_privilegiosUsuarios).shouldHave(text("Privilegios de usuario"));
    }

    public void setCrearPrivilegio(String tipoPriv,
                                   String descripcionPriv,
                                   String valor,
                                   String valor2,
                                   String valor3) {

        $(lst_tipoPrivilegioCrear).selectOption(tipoPriv);
        $(txt_descripcionCrear).sendKeys(descripcionPriv);
        $(txt_valorCrear).sendKeys(valor);
        $(txt_valor2).sendKeys(valor2);
        $(txt_valor3).sendKeys(valor3);
    }

    public void clickCrear() {
        $(btn_crear).click();
        $(btn_crear).should(disappear);
        $(tbl_tabla).shouldBe(visible);
    }

    public void verificarAlta(String tipoPriv,
                              String descripcionPriv,
                              String usuarioPriv,
                              String valor,
                              String valor2,
                              String valor3) {
        $(lbl_tipo).shouldHave(exactText(tipoPriv));
        $(lbl_descripcion).shouldHave(exactText(descripcionPriv));
        $(lbl_usuario).shouldHave(exactText(usuarioPriv));
        $(lbl_valor).shouldHave(exactText(valor));
        $(lbl_valor2).shouldHave(exactText(valor2));
        $(lbl_valor3).shouldHave(exactText(valor3));
    }

    public void verificarIngresoAPagina() {
        $(btn_agregar).shouldBe(visible);
        $(lbl_privilegiosUsuarios).shouldHave(text("Privilegios de usuario"));
    }

    public void escribirConsulta(String valor, String tipoPriv, String descripcionPriv) {
        $(txt_valor).setValue(valor);
        $(lst_tipoPrivilegio).selectOption(tipoPriv);
        $(txt_descripcion).setValue(descripcionPriv);
    }

    public void clickModificar() {
        $(btn_modificar).click();
        $(btn_modificar).should(disappear);
        $(lbl_modificar).shouldHave(text("Privilegios de usuario - Modificar"));
    }

    public void borrarCamposParaModificacion() {
        $(txt_descripcionCrear).clear();
        $(txt_descripcionCrear).clear();
        $(txt_valorCrear).clear();
        $(txt_valor2).clear();
        $(txt_valor3).clear();

    }

    public void setModificarPrivilegio(String tipoPrivilegioMod,
                                       String descripcionMod,
                                       String valorMod,
                                       String valor2Mod,
                                       String valor3Mod) {
        $(lst_tipoPrivilegioCrear).selectOption(tipoPrivilegioMod);
        $(txt_descripcionCrear).sendKeys(descripcionMod);
        $(txt_valorCrear).sendKeys(valorMod);
        $(txt_valor2).sendKeys(valor2Mod);
        $(txt_valor3).sendKeys(valor3Mod);
    }

    public void clickGuardarCambios() {
        $(btn_guardarCambios).click();
        $(btn_guardarCambios).should(disappear);
        $(lbl_privilegiosUsuarios).shouldHave(text("Privilegios de usuario"));
    }

    public void clickBorrar() {
        $(btn_borrar).click();
        confirm();
    }

    public void verificarCeroRegistros() {

        $(lbl_noRegistros).shouldBe(visible);
        $(lbl_noRegistros).shouldHave(text("No hay registros que mostrar"));
    }

    public void ingresar() {

    }

    public void validarBusquedaPorTipo() {

    }
}
