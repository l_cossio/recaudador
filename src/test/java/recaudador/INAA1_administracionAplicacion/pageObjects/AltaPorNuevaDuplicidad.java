package recaudador.INAA1_administracionAplicacion.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import recaudador.Pagina;

public class AltaPorNuevaDuplicidad extends Pagina {

    // ATRIBUTOS
    private final WebDriver driver;
    private final WebDriverWait w;

    public AltaPorNuevaDuplicidad(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);

        w = new WebDriverWait(driver, 3);
    }

    ///// ELEMENTOS WEB
    // Datos Generales
    @FindBy(css = "#pt1\\:soc4\\:\\:content")
    WebElement lst_tipoDePropiedad;


    // Ubicación

    @FindBy(css = "#pt1\\:r2\\:0\\:soc1\\:\\:content")
    WebElement lst_tipoDePredio;

    @FindBy(css = "#pt1\\:r2\\:0\\:soc18\\:\\:content")
    WebElement lst_localidad;

    @FindBy(css = "#pt1\\:r2\\:0\\:soc6\\:\\:content")
    WebElement lst_tipoDeAsentamiento;

    @FindBy(css = "#pt1\\:r2\\:0\\:it15\\:\\:content")
    WebElement campo_nombreDeAsentamiento;

    @FindBy(css = "#pt1\\:r2\\:0\\:soc5\\:\\:content")
    WebElement lst_nombreDeAsentamiento;

    @FindBy(css = "#pt1\\:r2\\:0\\:soc2\\:\\:content")
    WebElement lst_tipoDeVialidad;

    @FindBy(css = "#pt1\\:r2\\:0\\:it21\\:\\:content")
    WebElement campo_Vialidad;

    @FindBy(css = "#pt1\\:r2\\:0\\:soc7\\:\\:content")
    WebElement lst_Vialidad;

    @FindBy(css = "#pt1\\:r2\\:0\\:it92\\:\\:content")
    WebElement campo_numeroExterior;

    @FindBy(css = "#pt1\\:r2\\:0\\:it83\\:\\:content")
    WebElement campo_letra;

    @FindBy(css = "#pt1\\:r2\\:0\\:it23\\:\\:content")
    WebElement campo_numeroInterior;

    @FindBy(css = "#pt1\\:r2\\:0\\:it59\\:\\:content")
    WebElement campo_extNumeroInterior;

    @FindBy(css = "#pt1\\:r2\\:0\\:it36\\:\\:content")
    WebElement campo_codigoPostal;

    @FindBy(css = "#pt1\\:r2\\:0\\:it74\\:\\:content")
    WebElement campo_telefono1;

    @FindBy(css = "#pt1\\:r2\\:0\\:it62\\:\\:content")
    WebElement campo_telefono2;

    @FindBy(css = "#pt1\\:r2\\:0\\:it47\\:\\:content")
    WebElement campo_telefono3;

    @FindBy(css = "#pt1\\:r2\\:0\\:it50\\:\\:content")
    WebElement campo_medidorDeLuz;

    @FindBy(css = "#pt1\\:r2\\:0\\:it45\\:\\:content")
    WebElement campo_medidorDeAgua;

    @FindBy(css = "#pt1\\:r2\\:0\\:soc15\\:\\:content")
    WebElement lst_tipoEntreCalle;

    @FindBy(css = "#pt1\\:r2\\:0\\:it18\\:\\:content")
    WebElement campo_entreCalle;

    @FindBy(css = "#pt1\\:r2\\:0\\:soc11\\:\\:content")
    WebElement lst_tipoYCalle;

    @FindBy(css = "#pt1\\:r2\\:0\\:it8\\:\\:content")
    WebElement campo_yCalle;

    @FindBy(css = "#pt1\\:r2\\:0\\:soc8\\:\\:content")
    WebElement lst_tipoVialidadPosterior;

    @FindBy(css = "#pt1\\:r2\\:0\\:it42\\:\\:content")
    WebElement campo_vialidadPosterior;

    @FindBy(css = "#pt1\\:r2\\:0\\:it12\\:\\:content")
    WebElement campo_descripcionUbicacion;

    ////// METODOS

    // Getters
    public WebElement getLst_tipoDePropiedad() {
        return lst_tipoDePropiedad;
    }

    // Acciones

    public void seleccionarTipoDePropiedadAleatoria() {
        seleccionarOpcionAleatoria(lst_tipoDePropiedad);
    }

    public void seleccionarOpcionPorTexto(WebElement element, String seleccion) {
        Select vlr_Elemento = new Select(element);
        vlr_Elemento.selectByVisibleText(seleccion);
    }

    public void llenarCamposUbicacionUrbana(String numeroExterior, String letra,
                                            String numeroInterior, String extNumeroInterior,
                                            String telefono1, String telefono2,
                                            String telefono3, String medidorDeLuz,
                                            String medidorDeAgua, String entreCalle,
                                            String yCalle, String vialidadPosterior,
                                            String descripcionUbicacion) {
        seleccionarOpcionPorTexto(lst_tipoDePredio,"URBANO");
        seleccionarOpcionAleatoria(lst_nombreDeAsentamiento);
        seleccionarOpcionAleatoria(lst_Vialidad);
        campo_numeroExterior.sendKeys(numeroExterior);
        campo_letra.sendKeys(letra);
        campo_numeroInterior.sendKeys(numeroInterior);
        campo_extNumeroInterior.sendKeys(extNumeroInterior);
        campo_telefono1.sendKeys(telefono1);
        campo_telefono2.sendKeys(telefono2);
        campo_telefono3.sendKeys(telefono3);
        campo_medidorDeLuz.sendKeys(medidorDeLuz);
        campo_medidorDeAgua.sendKeys(medidorDeAgua);
        seleccionarOpcionAleatoria(lst_tipoEntreCalle);
        campo_entreCalle.sendKeys(entreCalle);
        seleccionarOpcionAleatoria(lst_tipoYCalle);
        campo_yCalle.sendKeys(yCalle);
        seleccionarOpcionAleatoria(lst_tipoVialidadPosterior);
        campo_vialidadPosterior.sendKeys(vialidadPosterior);
        campo_descripcionUbicacion.sendKeys(descripcionUbicacion);

    }
}
