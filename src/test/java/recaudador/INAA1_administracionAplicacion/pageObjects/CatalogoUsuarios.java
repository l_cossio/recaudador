package recaudador.INAA1_administracionAplicacion.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class CatalogoUsuarios {
	
	//Se declara constructor de la clase
	public CatalogoUsuarios(WebDriver driver) {

        this.driver = driver;

        PageFactory.initElements(driver, this);

	}
	//VARIABLES
	
	//se declara el WebDriver
	WebDriver driver;
	
	//SECCION PRINCIPAL
	
	//links
	@FindBy(linkText ="Catálogo de usuarios")
	public WebElement lnk_catalogoUsuarios;
	
	@FindBy(linkText ="Salir")
	WebElement lnk_salir;
	
	//etiquetas
	@FindBy(css="span[class='xdk']")
	WebElement lbl_catalogoUsuarios;
	
	@FindBy(css="table.x7b > tbody > tr:nth-child(2) > td:nth-child(2)")
	WebElement lbl_usuario;

	@FindBy(css="table.x7b > tbody > tr:nth-child(2) > td:nth-child(3)")
	WebElement lbl_nombre;
	
	@FindBy(css="table.x7b > tbody > tr:nth-child(2) > td:nth-child(4)")
	WebElement lbl_puesto;
	
	@FindBy(css="table.x7b > tbody > tr:nth-child(2) > td:nth-child(5)")
	WebElement lbl_numEmpleado;
	
	@FindBy(css="table.x7b > tbody > tr:nth-child(2) > td:nth-child(6)")
	WebElement lbl_estatus;

	@FindBy(css="table.x7b > tbody > tr:nth-child(2) > td:nth-child(7)")
	WebElement lbl_tipoUsuario;
	
	//campos de texto
	@FindBy(css="input[id='j_id_id5:j_id_id50']")
	WebElement txt_usuario;
	
	@FindBy(css="input[id='j_id_id5:j_id_id51']")
	WebElement txt_nombre;
	
	//botones
	@FindBy(css="button[id='j_id_id5:navcons1']")
	WebElement btn_consultar;
	
	@FindBy(css="button[id=j_id_id5:j_id_id62:j_id_id87]")
	WebElement btn_borrar;
	
	@FindBy(css="button[id=j_id_id5:j_id_id62:j_id_id88]")
	WebElement btn_modificar;
	
	@FindBy(css="button[id='j_id_id5:j_id_id62:j_id_id90']")
	WebElement btn_restablecerContrasena;
	
	@FindBy(css="td:nth-child(4) > button")
	WebElement btn_agregar;
	
	//CREAR USUARIO
	
	//etiquetas
	@FindBy(css="span[class='xdk']")
	WebElement lbl_crearUsuario;
	
	//campos de texto
	@FindBy(css="input[id='j_id_id5:j_id_id44']")
	WebElement txt_crearUsuario;

	@FindBy(css="textarea[id='j_id_id5:j_id_id46']")
	WebElement txt_crearNombre;

	@FindBy(css="textarea[id='j_id_id5:j_id_id48']")
	WebElement txt_crearPuesto;

	@FindBy(css="input[id='j_id_id5:j_id_id50']")
	WebElement txt_crearNumEmpleado;

	@FindBy(css="input[id='j_id_id5:j_id_id52']")
	WebElement txt_crearContrasena;

	@FindBy(css="input[id='j_id_id5:j_id_id54']")
	WebElement txt_crearConfirmarContrasena;

	@FindBy(css="input[id='j_id_id5:j_id_id55']")
	WebElement txt_crearPregunta;

	@FindBy(css="input[id='j_id_id5:j_id_id57']")
	WebElement txt_crearRespuesta;

	//listas valores
	@FindBy(css="select[id='j_id_id5:j_id_id59']")
	WebElement lst_crearEstatus;
	Select vlr_crearEstatus;

	@FindBy(css="select[id='j_id_id5:j_id_id61']")
	WebElement lst_crearTipoUsuario;
	Select vlr_crearTipoUsuario;
	
	//botones
	@FindBy(css="button:nth-child(1)")
	WebElement btn_crearUsuario;

	@FindBy(css="button:nth-child(2)")
	WebElement btn_cancelarCrear;
	
	//MODIFICAR USUARIO 
	
	//etiqueta
	@FindBy(css="span[class='xdk']")
	WebElement lbl_CatalogoUsuarioMod;
	
	//campos de texto
	@FindBy(css="textarea[id='j_id_id5:j_id_id46']")
	WebElement txt_nombreMod;
	
	@FindBy(css="textarea[id='j_id_id5:j_id_id48']")
	WebElement txt_puestoMod;
	
	@FindBy(css="input[id='j_id_id5:j_id_id50']")
	WebElement txt_numeroEmpMod;
	
	@FindBy(css="input[id='j_id_id5:j_id_id52']")
	WebElement txt_preguntaSecretaMod;
	
	@FindBy(css="input[id='j_id_id5:j_id_id54']")
	WebElement txt_respuestaSecretaMod;
	
	//Listas valores
	@FindBy(css="select[id='j_id_id5:j_id_id56']")
	WebElement lst_estatusMod;
	Select vlr_estatusMod;
	
	@FindBy(css="select[id='j_id_id5:j_id_id58']")
	WebElement lst_tipoUsuarioMod;
	Select vlr_tipoUsuarioMod;
	
	//botones
	@FindBy(css="button:nth-child(1)")
	WebElement btn_guardarCambiosMod;

	@FindBy(css="button:nth-child(2)")
	WebElement btn_CancelarMod;
	
	
	//METODOS
	
	//SECCION PRINCIPAL
	public WebElement lnkCatalogoUsuarios(){ return lnk_catalogoUsuarios; }

	public WebElement btnAgregar() { return btn_agregar; }

	public WebElement lnkSalir() { return lnk_salir; }

	public void clicCatalogoUsuarios() {
		lnk_catalogoUsuarios.click();
	}
	
	public void clicSalir() {
		lnk_salir.click();
	}
	
	public String getTituloCatalogoUsuario() {
		return lbl_catalogoUsuarios.getText();
	}
	
	public String getUsuario() {
		return lbl_usuario.getText();
	}
	
	public String getNombre() {
		return lbl_nombre.getText();
	}
	
	public String getPuesto() {
		return lbl_puesto.getText();
	}
	
	public String getNumEmpleado() {
		return lbl_numEmpleado.getText();
	}
	
	public String getEstatus() {
		return lbl_estatus.getText();
	}
	
	public String getTipoUsuario() {
		return lbl_tipoUsuario.getText();
	}
	
	public void setUsuario(String userName) {
		txt_usuario.sendKeys(userName);
	}
	
	public void setNombre(String name) {
		txt_nombre.sendKeys(name);
	}
	
    public void clicCosultar() {
    	btn_consultar.click();
    }
    
    public void clicBorrar() {
    	btn_borrar.click();
    }
    
    public void clicModificar() {
    	btn_modificar.click();
    }	
    
    public void clicRestablecerContrasenaAceptar() {
    	btn_restablecerContrasena.click();
    	driver.switchTo().alert().accept();
    
    }
    
    public void clicRestablecerContrasenaCancelar() {
    	btn_restablecerContrasena.click();
    	driver.switchTo().alert().dismiss();
    }
    
    public void clicAgregar() {
    	btn_agregar.click();
    }
    
    public void borrarUsuario() {
    	txt_usuario.clear();
    }
    public void borrarNombre() {
    	txt_nombre.clear();
    }
    
    //CREAR USUARIO

	public WebElement btnCrearUsuario(){ return btn_crearUsuario; }

    public void crearUsuario(String usuario, String nombre, String puesto, String numEmpleado, String contrasena,
							 String confirmarContrasena, String pregunta, String respuesta, String estatus,
							 String tipoUsuario) {
    	//Se capturan los campos de crear usuario
    	txt_crearUsuario.sendKeys(usuario);
	    txt_crearNombre.sendKeys(nombre);
	    txt_crearPuesto.sendKeys(puesto);
	    txt_crearNumEmpleado.sendKeys(numEmpleado);
	    txt_crearContrasena.sendKeys(contrasena);
	    txt_crearConfirmarContrasena.sendKeys(confirmarContrasena);
	    txt_crearPregunta.sendKeys(pregunta);
	    txt_crearRespuesta.sendKeys(respuesta);
	    vlr_crearEstatus = new Select(lst_crearEstatus);
	    vlr_crearEstatus.selectByVisibleText(estatus);
	    vlr_crearTipoUsuario = new Select(lst_crearTipoUsuario);
	    vlr_crearTipoUsuario.selectByVisibleText(tipoUsuario);
    
    }

    public void clicCrearUsuario() {
    	btn_crearUsuario.click();
    }

    public void clicCancelarCrear() {
    btn_cancelarCrear.click();
    driver.switchTo().alert().accept();
    }
    
    public String getCrearUsuario() {
     return lbl_crearUsuario.getText();
    }
    
 //Modificar
    
    public void borrarMod() {
    	txt_nombreMod.clear();
    	txt_puestoMod.clear();
    	txt_numeroEmpMod.clear();
    	txt_preguntaSecretaMod.clear();
    	txt_respuestaSecretaMod.clear();
    	
    }
    
    public void ModificarUsuario(String nuevoNombre, String puestoMod,String numEmpleadoMod,String preguntaMod,
								 String respuestaMod, String estatusMod, String tipoUsuarioMod ) {
    	//Se captura los campos de modificar usuario
    	txt_nombreMod.sendKeys(nuevoNombre);
    	txt_puestoMod.sendKeys(puestoMod);
    	txt_numeroEmpMod.sendKeys(numEmpleadoMod);
    	txt_preguntaSecretaMod.sendKeys(preguntaMod);
    	txt_respuestaSecretaMod.sendKeys(respuestaMod);
    	vlr_estatusMod = new Select(lst_estatusMod);
    	vlr_estatusMod.selectByVisibleText(estatusMod);
    	vlr_tipoUsuarioMod = new Select(lst_tipoUsuarioMod);
    	vlr_tipoUsuarioMod.selectByVisibleText(tipoUsuarioMod);
    	
    }
    
    public void clickGuadarCambiosMod() {
    	btn_guardarCambiosMod.click();
    	
    }
    
    public void clickCancelaMod() {
    	btn_CancelarMod.click();
    	driver.switchTo().alert().accept();
    }
    
    public String getCatalogoUsuarioMod() {
        return lbl_CatalogoUsuarioMod.getText();
    }


}
