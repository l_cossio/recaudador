package recaudador.INAA1_administracionAplicacion.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class AsignacionDeRecaudaciones {

    //Se declara constructor de la clase
    public AsignacionDeRecaudaciones(WebDriver driver){

        this.driver = driver;

        PageFactory.initElements(driver,this);
    }

    //VARIABLES

    //Se declara el WebDriver

    WebDriver driver;

    //SECCION PRINCIPAL

    //Links

    @FindBy(linkText = "Catálogo de asignación de recaudaciones a usuarios - Principal")
    WebElement lbl_asignacionRecaudaciones;

    @FindBy(linkText = "Seleccione un usuario para asignarle varias recaudaciones")
    WebElement lbl_seleccioUsuario;

    //Etiquetas
    @FindBy(css= "#j_id_id5\\:j_id_id53 > table.af_table_content > tbody > tr:nth-child(1) > th:nth-child(1)")
    WebElement lbl_clave;

    @FindBy(css= "#j_id_id5\\:j_id_id53 > table.af_table_content > tbody > tr:nth-child(1) > th:nth-child(2)")
    WebElement lbl_nombre;

    @FindBy(css= "#j_id_id5\\:j_id_id53 > table.af_table_content > tbody > tr:nth-child(1) > th:nth-child(3)")
    WebElement lbl_recaudador;

    @FindBy(css= "#j_id_id5\\:j_id_id53 > table.af_table_content > tbody > tr:nth-child(1) > th:nth-child(4)")
    WebElement lbl_recaudacion;

    //Listas valores
    @FindBy(css= "select[id='j_id_id5:navList1']")
    WebElement lst_usuarios;
    Select vlr_usuarios;

    //Botones
    @FindBy(css= "#j_id_id5\\:j_id_id53 > table:nth-child(1) > tbody > tr > td > table > tbody > tr > td:nth-child(1) > button")
    WebElement btn_configurar;

    //CONFIGURAR RECADACIONES

    //Etiquetas

    @FindBy(linkText = "Catálogo de asignación de recaudaciones a usuarios - Operación")
    WebElement lnk_operacion;

    @FindBy(linkText = "Recaudaciones asignadas")
    WebElement lnk_recaudaciones;

    //Listas valores
    @FindBy(css= "select[id = 'j_id_id5:navList1']")
    WebElement lst_recaudaciones;
    Select vlr_recaudaciones;

    @FindBy(css= "select[id = 'j_id_id5:j_id_id62'")
    WebElement lst_recaudacionBase;
    Select vlr_recaudacionBase;

    //Botones

    @FindBy(css= "#j_id_id5\\:j_id_id30 > div:nth-child(3) > table.af_panelButtonBar > tbody > tr > td:nth-child(1) > button")
    WebElement btn_guardarCambios;

    @FindBy(css= "#j_id_id5\\:j_id_id30 > div:nth-child(3) > table.af_panelButtonBar > tbody > tr > td:nth-child(3) > button")
    WebElement btn_cancelar;

    @FindBy(css= "button[id='j_id_id5:j_id_id58']")
    WebElement btn_aceptar;

    @FindBy(xpath = "//*[@id=\"j_id_id5:tabla\"]/table[1]/tbody/tr/td/table/tbody/tr/td[1]/table/tbody/tr/td[1]/button")
    WebElement btn_borrar;

    @FindBy(xpath = "//*[@id=\"j_id_id5:tabla:btnAgregar\"]")
    WebElement btn_agregar;

    //Radio Button
    @FindBy(css="input[id='j_id_id5:tabla:0']")
    WebElement btn_seleccionar;

    //Etiquetas
    @FindBy(css= "#j_id_id5\\:tabla > table.af_table_content > tbody > tr:nth-child(2) > td:nth-child(2)")
    WebElement lbl_claveOperaciones;

    @FindBy(css="#j_id_id5\\:tabla > table.af_table_content > tbody > tr:nth-child(2) > td:nth-child(3)")
    WebElement lbl_nombreOperaciones;

    @FindBy(css="#j_id_id5\\:tabla > table.af_table_content > tbody > tr:nth-child(2) > td:nth-child(4)")
    WebElement lbl_recaudadorOperaciones;

    @FindBy(css="#j_id_id5\\:tabla > table.af_table_content > tbody > tr:nth-child(2) > td:nth-child(5)")
    WebElement lbl_recaudacionBaseOp;

    @FindBy(xpath = "//*[@id=\"j_id_id5:j_id_id30\"]/div[1]/table[4]/tbody/tr[2]/td[2]")
    WebElement lbl_usuario;

    //METODOS

    //Seccion principal

    public WebElement lnk_asignacionRecaudaciones(){
        return lbl_asignacionRecaudaciones;
    }

    public WebElement lnk_seleccioUsuario(){
        return lbl_seleccioUsuario;
    }

    public String getClave(){
        return lbl_clave.getText();
    }

    public String getNombre(){
        return lbl_nombre.getText();
    }

    public String getRecaudador(){
        return lbl_recaudador.getText();
    }

    public String getRecaudacion(){
        return lbl_recaudacion.getText();
    }

    public void setUsuario(String usuario){
        vlr_usuarios = new Select (lst_usuarios);
        vlr_usuarios.selectByVisibleText(usuario);
    }

    public void clicConfigurar() {
        btn_configurar.click();
    }

    //Asignacion

    public WebElement lnk_operacion(){
        return lnk_operacion;
    }

    public WebElement lnk_recaudaciones(){
        return lnk_recaudaciones;
    }

    public void setRecaudaciones(String recaudaciones){
        vlr_recaudaciones = new Select (lst_recaudaciones);
        vlr_recaudaciones.selectByVisibleText(recaudaciones);
    }

    public void setRecaudacionBase(String base){
        vlr_recaudacionBase = new Select (lst_recaudacionBase);
        vlr_recaudacionBase.selectByVisibleText(base);
    }

    public void clicGuardarCambios() {
        btn_guardarCambios.click();
    }

    public void clicCancelar() {
        btn_cancelar.click();
    }

    public void clicAceptar() {
        btn_aceptar.click();
    }

    public void clicBorrar() {
        btn_borrar.click();
    }

    public void clicAgregar() {
        btn_agregar.click();
    }

    public void clicSeleccionar() {
        btn_seleccionar.click();
    }

    public String getClaveOperaciones(){
        return lbl_claveOperaciones.getText();
    }

    public String getNombreOperaciones(){
        return lbl_nombreOperaciones.getText();
    }

    public String getRecaudadorOperaciones(){
        return lbl_recaudadorOperaciones.getText();
    }

    public String getRecaudacionBaseOp(){
        return lbl_recaudacionBaseOp.getText();
    }

    public String getUsuarioOp(){
        return lbl_usuario.getText();
    }

}
