package recaudador.INAA1_administracionAplicacion;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import recaudador.INAA1_administracionAplicacion.pageObjects.DatosEntradaAdmonAplicacion;
import recaudador.INAA1_administracionAplicacion.pageObjects.PrivilegiosUsuariosSelenide;
import recaudador.INLO1_login.pageObjects.Login;
import recaudador.INLO1_login.pageObjects.Menu;

import java.io.IOException;

@Test(groups = {"TAB", "DGO", "CUU"})
public class TestPrivilegiosUsuariosSelenide extends DatosEntradaAdmonAplicacion {

    public WebDriver driver;
    Login objLogin;
    Menu objMenu;
    WebDriverWait w;
    PrivilegiosUsuariosSelenide objPrivilegiosUsuarios;


    @BeforeMethod
    public void setup() throws IOException {
        //Se inicializa driver
        driver = initializerDriver();

        //Se carga las variables del properties
        cargaVariablesPrivilegiosUsuarios();

        //Se crean los objetos de las clases a utilizar en las pruebas
        objLogin = new Login(driver);
        objMenu  = new Menu(driver);
        objPrivilegiosUsuarios = new PrivilegiosUsuariosSelenide(driver);

        //Se maximiza el navegador
        driver.manage().window().maximize();

        //Se ingresa a recaudador
        driver.get(url);

        //Se crea objeto de un explicit wait
        w = new WebDriverWait(driver, 15);
        w.until(ExpectedConditions.elementToBeClickable(objLogin.btnInicioSesion()));

        //Se inicia sesion en la aplicación recaudador
        objLogin.login(userName, password);

        //Se espera a que se encuentre clickeable el link de administracion de la aplicacion
        w.until(ExpectedConditions.elementToBeClickable(objMenu.lnkAdministracionAplicacion()));

        //Se valida el inicio de sesion
        Assert.assertEquals(objMenu.getMenu(), "Menú");

        //Se ingresa al menu administracion de la aplicacion
        objMenu.clicAdministracionAplicacion();

        //Se ingresa a privilegios usuarios
        objPrivilegiosUsuarios.clickPrivilegiosUsuarios();

    }

    /*******************************************************************************
     *Nombre: Agregar Privilegios usuarios
     *Descripción: Se crear Privilegios a un usuario
     *Objetivos de la prueba:
     - Crear  privilegios a un usuario
     - Validar que se guarden cambios
     *Autor: Paulina Alejandra Pérez Ochoa.
     *Fecha de creacion: 01-10-2020.
     *Modificado por: N/A
     *Fecha de modificación: N/A
     *******************************************************************************/

    @Test(priority = 0)
    public void test_CrearPrivilegio() throws InterruptedException {

        //Se selecciona usuario
        objPrivilegiosUsuarios.seleccionarUsuario(usuarioPriv);

        //Clic en boton buscar
        objPrivilegiosUsuarios.clickBuscar();

        ////Agregar nuevo privilegio
        //Se da clic al botón agregar
        objPrivilegiosUsuarios.clickAgregar();

        //Se da click al botón cancelar
        objPrivilegiosUsuarios.clickCancelar();

        //// Regresando a pantalla de privilegios
        //Se busca usuario
        objPrivilegiosUsuarios.seleccionarUsuario(usuarioPriv);

        //Se da click en buscar
        objPrivilegiosUsuarios.clickBuscar();

        //Agregar nuevo privilegio
        objPrivilegiosUsuarios.clickAgregar();

        //Se ingresan datos del nuevo privilegio
        objPrivilegiosUsuarios.setCrearPrivilegio(tipoPriv, descripcionPriv, valor, valor2, valor3);

        //click al boton crear
        objPrivilegiosUsuarios.clickCrear();

        //Se verifica que el usaurio se dio de alta correctamente
        objPrivilegiosUsuarios.verificarAlta(tipoPriv.toUpperCase(),
                descripcionPriv.toUpperCase(),
                usuarioPriv.toUpperCase(),
                valor.toUpperCase(),
                valor2.toUpperCase(),
                valor3.toUpperCase()
        );
    }

    @Test(priority = 2)
    public void test_modificarPrivilegio() {

        objPrivilegiosUsuarios.verificarIngresoAPagina();

        //Se busca usuario
        objPrivilegiosUsuarios.seleccionarUsuario(usuarioPriv);

        //Se realiza consulta descripcion, valor y tipo de privilegio
        objPrivilegiosUsuarios.escribirConsulta(valor, tipoPriv, descripcionPriv);

        //Clic en boton buscar
        objPrivilegiosUsuarios.clickBuscar();

        //Clic en el boton modificar
        objPrivilegiosUsuarios.clickModificar();

        //Borrar los datos del privilegio
        objPrivilegiosUsuarios.borrarCamposParaModificacion();

        //insertar datos nuevos a modificar
        objPrivilegiosUsuarios.setModificarPrivilegio(tipoPrivMod, descripcionMod, valorMod, valor2Mod, valor3Mod);

        //Clic en el boton guardar
        objPrivilegiosUsuarios.clickGuardarCambios();
        //Se valida se modifiquen campos
        objPrivilegiosUsuarios.verificarAlta(
                tipoPrivMod.toUpperCase(),
                descripcionMod.toUpperCase(),
                usuarioPriv.toUpperCase(),
                valorMod.toUpperCase(),
                valor2Mod.toUpperCase(),
                valor3Mod.toUpperCase()
                );
    }

    @Test(priority = 3)
    public void test_borrarPrivilegio() {

        // Se selecciona usuario
        objPrivilegiosUsuarios.seleccionarUsuario(usuarioPriv);

        // Se escribe consulta
        objPrivilegiosUsuarios.escribirConsulta(valorMod, tipoPrivMod, descripcionMod);

        // Click en boton buscar
        objPrivilegiosUsuarios.clickBuscar();

        //Clic en el boton borrar
        objPrivilegiosUsuarios.clickBorrar();

        // Se escribe consulta
        // Se consulta de nuevo el privilegio para asegurar se borre registro
        objPrivilegiosUsuarios.escribirConsulta(
                valorMod.toUpperCase(),
                tipoPrivMod,
                descripcionMod.toLowerCase());

        objPrivilegiosUsuarios.verificarCeroRegistros();
    }
        @AfterMethod
    public void tearDown() {
        driver.quit();
    }
}
