package recaudador.INAA1_administracionAplicacion;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


import recaudador.INAA1_administracionAplicacion.pageObjects.*;
import recaudador.INLO1_login.pageObjects.Login;
import recaudador.INLO1_login.pageObjects.Menu;

@Test(groups = {"TAB", "DGO", "CUU"})
public class TestCatalogoUsuarios extends DatosEntradaAdmonAplicacion {
	
	public WebDriver driver;
	Login objLogin;
	Menu  objMenu;
	WebDriverWait w;
	CatalogoUsuarios objCatalogoUsuarios;
		
	@BeforeMethod
    public void setup () throws IOException {
		//Se inicializa driver
    	driver = initializerDriver();
    	//se maximiza el navegador
    	driver.manage().window().maximize();
    	//Se cargan las variables del properties
    	cargaVariablesCatalogoUsuarios();
		//Se crean los objetos de las clases a utilizar en las pruebas
		objLogin = new Login(driver);
		objMenu  = new Menu(driver);
		objCatalogoUsuarios = new CatalogoUsuarios(driver);
    	//Se ingresa a recaudador
    	driver.get(url);
    	//Se crea objeto de un explicit wait
		w = new WebDriverWait(driver,10);
		//Se espera a que el boton de inico sesion se encuentre clickeable
		w.until(ExpectedConditions.elementToBeClickable(objLogin.btnInicioSesion()));

    }	
	
	/*******************************************************************************  
		*Nombre: Agregar usuario 
		*Descripción: Se crean tres usuarios de diferente estatus y tipo 
		 en el catalogo de usuarios del sistema Recaudador 
		*Objetivos de la prueba:    
		    - Crear usario de cada tipo y estatus 
		    - Realiza consulta de cada usuario
		    - Iniciar sesión con usuarios activos creados 
		*Autor: Fernando de Leon Vega.   
		*Fecha de creacion: 23-09-2020.
		*Modificado por: N/A
		*Fecha de modificación: N/A  
	*******************************************************************************/ 
    
    @Test(dataProvider = "dataProviderUsuario")
    public void test_AgregarUsuario(String usuario,String estatus, String tipoUsuario) {
    	//Se inicia sesion en la aplicacioncrearUsuario
    	objLogin.login(userName,password);
		//Se espera a que se encuentre clickeable el link de administracion de la aplicacion
		w.until(ExpectedConditions.elementToBeClickable(objMenu.lnkAdministracionAplicacion()));
    	//Se valida el inicio de sesion
		Assert.assertEquals(objMenu.getMenu(), "Menú");
    	//Se ingresa a menu de administración de la aplicación
    	objMenu.clicAdministracionAplicacion();
		//Se espera a que se encuentre clickeable el link del catalogo de usuarios
		w.until(ExpectedConditions.elementToBeClickable(objCatalogoUsuarios.lnkCatalogoUsuarios()));
    	//Se ingresa a catálogo de usuarios 
    	objCatalogoUsuarios.clicCatalogoUsuarios();
		//Se espera a que se encuentre clickeable el boton de agregar usuario
		w.until(ExpectedConditions.elementToBeClickable(objCatalogoUsuarios.btnAgregar()));
    	//Se valida ingreso a catálogo 
    	Assert.assertTrue(objCatalogoUsuarios.getTituloCatalogoUsuario().contentEquals("Catálogo de usuarios"));   	
    	//Clic al botón agregar Usuario
    	objCatalogoUsuarios.clicAgregar();
		//Se espera a que se encuentre clickeable el boton de crear usuario
		w.until(ExpectedConditions.elementToBeClickable(objCatalogoUsuarios.btnCrearUsuario()));
    	//Se valida ingreso a la pantalla Catálogo de usuarios
    	Assert.assertTrue(objCatalogoUsuarios.getCrearUsuario().contentEquals("Catálogo de usuarios - Crear"));
    	//Clic en boton cancelar 	
    	objCatalogoUsuarios.clicCancelarCrear();
		//Se espera a que se encuentre clickeable el boton de agregar usuario
		w.until(ExpectedConditions.elementToBeClickable(objCatalogoUsuarios.btnAgregar()));
    	//Validar regreso a pantalla catalogo de usuarios
    	Assert.assertTrue(objCatalogoUsuarios.getTituloCatalogoUsuario().contentEquals("Catálogo de usuarios")); 	
    	//Clic al botón agregar Usuario
    	objCatalogoUsuarios.clicAgregar();
		//Se espera a que se encuentre clickeable el boton de crear usuario
		w.until(ExpectedConditions.elementToBeClickable(objCatalogoUsuarios.btnCrearUsuario()));
    	//Se valida ingreso a la pantalla Catálogo de usuarios
    	Assert.assertTrue(objCatalogoUsuarios.getCrearUsuario().contentEquals("Catálogo de usuarios - Crear"));	 	   	
    	//Se capturan campos dentro de la pantalla catálogo de usuarios
    	objCatalogoUsuarios.crearUsuario(usuario, nombre, puesto, numEmpleado, contrasena, confirmarContrasena,
				pregunta, respuesta, estatus,tipoUsuario);
    	//Clic en botón Crear Usuario
    	objCatalogoUsuarios.clicCrearUsuario();
		//Se espera a que se encuentre clickeable el boton de agregar usuario
		w.until(ExpectedConditions.elementToBeClickable(objCatalogoUsuarios.btnAgregar()));
    	//Validar regreso a pantalla catalogo de usuarios
    	Assert.assertTrue(objCatalogoUsuarios.getTituloCatalogoUsuario().contentEquals("Catálogo de usuarios")); 
    	//Se captura el usuario y nombre del usuario
    	objCatalogoUsuarios.setUsuario(usuario);
    	objCatalogoUsuarios.setNombre(nombre);
    	//Se consulta el usuario
    	objCatalogoUsuarios.clicCosultar();
		//Se espera a que se encuentre clickeable el boton
		w.until(ExpectedConditions.elementToBeClickable(objCatalogoUsuarios.btnAgregar()));
    	//Se verifica que el usuario se dio de alta correctamente
    	Assert.assertTrue(objCatalogoUsuarios.getUsuario().contentEquals(usuario.toUpperCase())); 
    	Assert.assertTrue(objCatalogoUsuarios.getNombre().contentEquals(nombre.toUpperCase())); 
    	Assert.assertTrue(objCatalogoUsuarios.getPuesto().contentEquals(puesto.toUpperCase())); 
    	Assert.assertTrue(objCatalogoUsuarios.getNumEmpleado().contentEquals(numEmpleado.toUpperCase())); 
    	Assert.assertTrue(objCatalogoUsuarios.getEstatus().contentEquals(estatus.toUpperCase())); 
    	Assert.assertTrue(objCatalogoUsuarios.getTipoUsuario().contentEquals(tipoUsuario.toUpperCase())); 
    	if(estatus.equals("ACTIVO")) {
    		//Se sale de la aplicacion
        	objCatalogoUsuarios.clicSalir();
			//Se espera a que se encuentre clickeable el boton
			w.until(ExpectedConditions.elementToBeClickable(objLogin.btnCerrarSesion()));
        	//Se cierra sesion
        	objLogin.clicCerrarSesion();
			//Se espera a que el boton de inico sesion se encuentre clickeable
			w.until(ExpectedConditions.elementToBeClickable(objLogin.btnInicioSesion()));
        	//Se captura usuario y contrasena del usario creado
        	objLogin.login(usuario,contrasena);
			//Se espera a que se encuentre clickeable el link de administracion de la aplicacion
			w.until(ExpectedConditions.elementToBeClickable(objMenu.lnkAdministracionAplicacion()));
        	//Se valida el inicio de sesion
			Assert.assertEquals(objMenu.getMenu(), "Menú");
    	}
	}
    
	/*******************************************************************************  
		*Nombre: Modificar usuario 
		*Descripción: Se realiza modificacion de tres usuarios existetes dentro del
		sitema reacudador, con diferentes estatus y tipos usuarios. 
		*Objetivos de la prueba:    
		    - Modificar usarios  
		    - Realiza consulta de cada usuario modificado
		    - Iniciar sesión con usuarios activos modificados
		*Autor: Paulina Alejandra Pérez Ochoa.   
		*Fecha de creacion: 23-09-2020.
		*Modificado por: N/A
		*Fecha de modificación: N/A  
	*******************************************************************************/ 
    
    @Test(dataProvider = "dataProviderUsuarioMod")
    public void test_modificarUsuario(String usuario,String nombre, String estatus, String tipoUsuario) {
    	//Se inicia sesion en la aplicacion
    	objLogin.login(userName,password);  	
    	//Se valida el inico de sesion
		Assert.assertEquals(objLogin.getTituloMenuPrincipal(), "Bienvenido a Recaudador");
    	//Se ingresa a menu de administracion de la aplicacion
    	objMenu.clicAdministracionAplicacion();   	
    	//Se ingresa a catalogo de usuarios
    	objCatalogoUsuarios.clicCatalogoUsuarios();
    	//Se valida ingreso a catalogo
    	Assert.assertTrue(objCatalogoUsuarios.getTituloCatalogoUsuario().contentEquals("Catálogo de usuarios"));	
    	//Clic al bot�n modificar Usuario
    	objCatalogoUsuarios.clicModificar();
    	//Se valida ingreso a pantalla 
    	Assert.assertTrue(objCatalogoUsuarios.getCatalogoUsuarioMod().contentEquals
				("Catálogo de usuarios - Modificar"));
    	//Clic boton Cancelar
    	objCatalogoUsuarios.clickCancelaMod();
    	//Se valida regreso a pantalla Catalogo usuarios 
    	Assert.assertTrue(objCatalogoUsuarios.getTituloCatalogoUsuario().contentEquals("Catálogo de usuarios"));   
    	//Se busca usuario a modificar
    	//Se captura el usuario y nombre del usuario
    	objCatalogoUsuarios.setUsuario(usuario);
    	objCatalogoUsuarios.setNombre(nombre);
    	//Se consulta el usuario
    	objCatalogoUsuarios.clicCosultar();
    	// Validar consulte el usuario 
    	Assert.assertTrue(objCatalogoUsuarios.getUsuario().contentEquals(usuario.toUpperCase()));
    	Assert.assertTrue(objCatalogoUsuarios.getNombre().contentEquals(nombre.toUpperCase()));
    	//Clic al bot�n modificar Usuario
    	objCatalogoUsuarios.clicModificar();
    	//Se valida ingreso a pantalla 
    	Assert.assertTrue(objCatalogoUsuarios.getCatalogoUsuarioMod().contentEquals
				("Catálogo de usuarios - Modificar"));
    	//Se eliminan los datos de los campos
    	objCatalogoUsuarios.borrarMod();
    	//Se realizan cambios al usuario
    	objCatalogoUsuarios.ModificarUsuario(nuevoNombre, puestoMod, numEmpleadoMod, preguntaMod, respuestaMod,
				estatus, tipoUsuario);
    	//Clic en el boton guardar cambios
    	objCatalogoUsuarios.clickGuadarCambiosMod();
    	//Validar regreso a pantalla catalogo de usuarios
    	Assert.assertTrue(objCatalogoUsuarios.getTituloCatalogoUsuario().contentEquals("Catálogo de usuarios")); 
    	//Se borra usuario y nombre clic en consultar para refrescar pantalla
    	objCatalogoUsuarios.borrarUsuario();
    	objCatalogoUsuarios.borrarNombre();
    	objCatalogoUsuarios.clicCosultar();
    	//Se captura el usuario y nombre del usuario y nombre
    	objCatalogoUsuarios.setUsuario(usuario);
    	objCatalogoUsuarios.setNombre(nuevoNombre);
    	//Se consulta el usuario
    	objCatalogoUsuarios.clicCosultar();
    	//Se verifica que el usuario se dio de alta correctamente
    	Assert.assertTrue(objCatalogoUsuarios.getUsuario().contentEquals(usuario.toUpperCase())); 
    	Assert.assertTrue(objCatalogoUsuarios.getNombre().contentEquals(nuevoNombre.toUpperCase())); 
    	Assert.assertTrue(objCatalogoUsuarios.getPuesto().contentEquals(puestoMod.toUpperCase())); 
    	Assert.assertTrue(objCatalogoUsuarios.getNumEmpleado().contentEquals(numEmpleadoMod.toUpperCase())); 
    	Assert.assertTrue(objCatalogoUsuarios.getEstatus().contentEquals(estatus.toUpperCase())); 
    	Assert.assertTrue(objCatalogoUsuarios.getTipoUsuario().contentEquals(tipoUsuario.toUpperCase()));
    	if(estatus.equals("ACTIVO")) {
    		//Se sale de la aplicacion
        	objCatalogoUsuarios.clicSalir();
			//Se espera a que se encuentre clickeable el boton
			w.until(ExpectedConditions.elementToBeClickable(objLogin.btnCerrarSesion()));
        	//Se cierra sesion
        	objLogin.clicCerrarSesion();
			//Se espera a que el boton de inico sesion se encuentre clickeable
			w.until(ExpectedConditions.elementToBeClickable(objLogin.btnInicioSesion()));
        	//Se captura usuario y contrasena del usario creado
        	objLogin.login(usuario,contrasena); 
        	//Se valida el inicio de sesion
			Assert.assertEquals(objMenu.getMenu(), "Menú");
    	}
    }
    
	/*******************************************************************************  
		*Nombre: Restablecer contraseña 
		*Descripción: Se restablece contraseña de distintos usuarios de recaudador,
		 verificando no altere otro dato distinto a la contraseña
		*Objetivos de la prueba:    
		    - Realizar cambio de contraseña de usuarios en todos los estatus disponibles 
		      en recaudador. 
		    - Verificar no se modifique otro dato distinto a contraseña.
		    - Iniciar secion con usuarios activos con nueva contraseña 
		*Autor: Fernando de Leon Vega.   
		*Fecha de creacion: 23-09-2020.
		*Modificado por: N/A
		*Fecha de modificación: N/A  
	*******************************************************************************/ 
    
    @Test(dataProvider = "dataProviderRestablecerPass")
    public void restablecerContrasena(String usuario) throws InterruptedException {
    	//Se inicia sesion en la aplicacion
    	objLogin.login(userName,password);
		//Se espera a que se encuentre clickeable el link de administracion de la aplicacion
		w.until(ExpectedConditions.elementToBeClickable(objMenu.lnkAdministracionAplicacion()));
    	//Se valida el inicio de sesion
		Assert.assertEquals(objMenu.getMenu(), "Menú");
    	//Se ingresa a menu de administración de la aplicación
    	objMenu.clicAdministracionAplicacion();
		//Se espera a que se encuentre clickeable el link del catalogo de usuarios
		w.until(ExpectedConditions.elementToBeClickable(objCatalogoUsuarios.lnk_catalogoUsuarios));
		//Se ingresa a catálogo de usuarios
    	objCatalogoUsuarios.clicCatalogoUsuarios();
		//Se espera a que se encuentre clickeable el boton
		w.until(ExpectedConditions.elementToBeClickable(objCatalogoUsuarios.btnAgregar()));
    	//Se valida ingreso a catálogo 
    	Assert.assertTrue(objCatalogoUsuarios.getTituloCatalogoUsuario().contentEquals("Catálogo de usuarios"));
    	//Se captura el usuario
    	objCatalogoUsuarios.setUsuario(usuario);
    	//Se consulta el usuario
    	objCatalogoUsuarios.clicCosultar();
		//Se espera a que se encuentre clickeable el boton
		w.until(ExpectedConditions.elementToBeClickable(objCatalogoUsuarios.btnAgregar()));
    	//Se toman los datos del usuario consultado
    	String validarUsuario = objCatalogoUsuarios.getUsuario();
    	String validarNombre = objCatalogoUsuarios.getNombre();
    	String validarPuesto = objCatalogoUsuarios.getPuesto();
    	String validarNumEmpleado = objCatalogoUsuarios.getNumEmpleado();
    	String validarEstatus = objCatalogoUsuarios.getEstatus();
    	String validarTipoUsuario = objCatalogoUsuarios.getTipoUsuario();
    	//Se presiona el boton restablecer contraseña y se cancela accion
    	objCatalogoUsuarios.clicRestablecerContrasenaCancelar();
    	//Se presiona el boton restablecer contraseña y se confirma accion
    	objCatalogoUsuarios.clicRestablecerContrasenaAceptar();
    	//Se verifca que los datos del usuario sean los mismos despues de el restablecer contraseña
    	Assert.assertTrue(objCatalogoUsuarios.getUsuario().contentEquals(validarUsuario)); 
    	Assert.assertTrue(objCatalogoUsuarios.getNombre().contentEquals(validarNombre)); 
    	Assert.assertTrue(objCatalogoUsuarios.getPuesto().contentEquals(validarPuesto)); 
    	Assert.assertTrue(objCatalogoUsuarios.getNumEmpleado().contentEquals(validarNumEmpleado)); 
    	Assert.assertTrue(objCatalogoUsuarios.getEstatus().contentEquals(validarEstatus)); 
    	Assert.assertTrue(objCatalogoUsuarios.getTipoUsuario().contentEquals(validarTipoUsuario));
    	if(validarEstatus.equals("ACTIVO")) {
			//Se espera a que se encuentre clickeable el boton
			w.until(ExpectedConditions.elementToBeClickable(objCatalogoUsuarios.lnkSalir()));
    		//Se sale de la aplicacion
        	objCatalogoUsuarios.clicSalir();
			//Se espera a que se encuentre clickeable el boton
			w.until(ExpectedConditions.elementToBeClickable(objLogin.btnCerrarSesion()));
        	//Se cierra sesion
        	objLogin.clicCerrarSesion();
			//Se espera a que el boton de inicio sesion se encuentre clickeable
			w.until(ExpectedConditions.elementToBeClickable(objLogin.btnInicioSesion()));
        	//Se captura usuario y contrasena del usario creado
        	objLogin.login(usuario,usuario);
			//Se espera a que se encuentre clickeable el link de administracion de la aplicacion
			w.until(ExpectedConditions.elementToBeClickable(objMenu.lnkAdministracionAplicacion()));
        	//Se valida el inicio de sesion
			Assert.assertEquals(objMenu.getMenu(), "Menú");
    	}
    	
    }
    
  //Se define el metodo de data provider, donde se capturan los parametros de la prueba para su ejecucion
  	@DataProvider()
      public Object[][] dataProviderUsuario() throws IOException {
  		//Se cargan las variables del data propierties
  		cargaVariablesCatalogoUsuarios();
  		//Se define las ejecuciones y variables a utilizar
  		Object[][] datos = new Object[3][3];
  		
  		//ejecucion 1: usuario activo tipo local
  		datos[0][0] = usuario1;
  		datos[0][1] = estatusActivo;
  		datos[0][2] = tipoUsuarioLocal;
  		
  		//ejecucion 2: usuario inactivo tipo externo
  		datos[1][0] = usuario2;
  		datos[1][1] = estatusInactivo;
  		datos[1][2] = tipoUsuarioExterno;
  		
  		//ejecucion 3: usuario no aplica tipo remoto
  		datos[2][0] = usuario3;
  		datos[2][1] = estatusNoAplica;
  		datos[2][2] = tipoUsuarioRemoto;
  		
          return datos;
      }
  	
  	//Se define el metodo de data provider, donde se capturan los parametros de la prueba para su ejecucion
  	@DataProvider()
      public Object[][] dataProviderUsuarioMod() throws IOException {
  		//Se cargan las variables del data propierties
  		cargaVariablesCatalogoUsuarios();
  		//Se define las ejecuciones y variables a utilizar
  		Object[][] datos = new Object[3][4];
  		
  		//ejecucion 1: usuario activo de tipo local
  		datos[0][0] = usuarioMod1;
  		datos[0][1] = nombreMod1;
  		datos[0][2] = estatusActivo;
  		datos[0][3] = tipoUsuarioLocal;
  		
  		//ejecucion 2: usuario inactivo de tipo externo
  		datos[1][0] = usuarioMod2;
  		datos[1][1] = nombreMod2;
  		datos[1][2] = estatusInactivo;
  		datos[1][3] = tipoUsuarioExterno;
  		
  		//ejecucion 3: usario no aplica de tipo remoto
  		datos[2][0] = usuarioMod3;
  		datos[2][1] = nombreMod3;
  		datos[2][2] = estatusNoAplica;
  		datos[2][3] = tipoUsuarioRemoto;
  		
          return datos;
      }
  	
  	//Se define el metodo de data provider, donde se capturan los parametros de la prueba para su ejecucion
  	@DataProvider()
      public Object[][] dataProviderRestablecerPass() throws IOException {
  		//Se cargan las variables del data propierties
  		cargaVariablesCatalogoUsuarios();
  		//Se define las ejecuciones y variables a utilizar
  		Object[][] datos = new Object[3][1];
  		
  		//ejecucion 1: se restablece password de usario activo
  		datos[0][0] = usuarioRestablecerPassActivo;
  		
  		//ejecucion 2: se restablece password de usuario inactivo
  		datos[1][0] = usuarioRestablecerPassInactivo;
  		
  		//ejecucion 3: se restablece password de usuario no aplica
  		datos[2][0] = usuarioRestablecerPassNoAplica;
  		
          return datos;
      }
  	
    
    @AfterMethod
    public void tearDown() {
    	driver.quit();
    }
}
