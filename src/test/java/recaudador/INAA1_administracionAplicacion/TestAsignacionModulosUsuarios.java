package recaudador.INAA1_administracionAplicacion;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import recaudador.INAA1_administracionAplicacion.pageObjects.AsignacionModulosUsuarios;
import recaudador.INAA1_administracionAplicacion.pageObjects.DatosEntradaAdmonAplicacion;
import recaudador.INLO1_login.pageObjects.Login;
import recaudador.INLO1_login.pageObjects.Menu;

import java.io.IOException;

@Test(groups = {"TAB", "DGO", "CUU"})
public class TestAsignacionModulosUsuarios extends DatosEntradaAdmonAplicacion {

    public WebDriver driver;
    Login objLogin;
    Menu objMenu;
    WebDriverWait w;
    AsignacionModulosUsuarios objModulosUsuarios;

    @BeforeMethod
    public void setup () throws IOException {
        //Se inicializa driver
        driver = initializerDriver();
        //se cargan variables para la prueba
        cargaVariablesCatalogoUsuarios();
        cargaVariablesCataRol();
        //se maximiza el navegador
        driver.manage().window().maximize();
        //Se crean los objetos de las clases a utilizar en las pruebas
        objLogin = new Login(driver);
        objMenu  = new Menu(driver);
        objModulosUsuarios = new AsignacionModulosUsuarios(driver);
        //Se ingresa a recaudador
        driver.get(url);
        //Se crea objeto de un explicit wait
        w = new WebDriverWait(driver,10);
        //Se espera a que el boton de inico sesion se encuentre clickeable
        w.until(ExpectedConditions.elementToBeClickable(objLogin.btnInicioSesion()));

    }

    /*******************************************************************************
     *Nombre: Asignar modulo por rol
     *Descripción: Se asignan modulos a un usuario por rol
     *Objetivos de la prueba:
        -Validar que se pueda ingresar a la pantalla.
        -Validar la selección de usuario, por usuario y nombre.
        -Validar que se muestren los datos del usuario.
        -Consultar un rol por rol.
        -Limpiar la busqueda del rol.
        -Consultar rol por descripcion.
        -Asignar rol al usuario.
        -Iniciar sesion con el usuario asignado y verificar que se asigno el rol.
     *Autor: Fernando de Leon Vega.
     *Fecha de creacion: 06-10-2020.
     *Modificado por: N/A
     *Fecha de modificación: N/A
     *******************************************************************************/

    @Test(priority = 1)
    public void test_AsignarModuloPorRol() {
        //Se inicia sesion en Recaudador
        objLogin.login(userName, password);
        //Se espera a que se encuentre clickeable el link de administracion de la aplicacion
        w.until(ExpectedConditions.elementToBeClickable(objMenu.lnkAdministracionAplicacion()));
        //Se valida el inicio de sesion
        Assert.assertEquals(objMenu.getMenu(), "Menú");
        //Se ingresa a menu de administración de la aplicación
        objMenu.clicAdministracionAplicacion();
        //Se espera a que se encuentre clickeable la pantalla a ingresar
        w.until(ExpectedConditions.elementToBeClickable(objModulosUsuarios.lnkModulosUsuarios()));
        objModulosUsuarios.clicModulosUsuarios();
        //Se espera a que se encuentre clickeable la pantalla a ingresar
        w.until(ExpectedConditions.elementToBeClickable(objModulosUsuarios.lblAsignacionModulosUsuarios()));
        //Se valida que se ingreso a la pantalla
        Assert.assertEquals(objModulosUsuarios.getTituloPantalla(),"Asignación de módulos a usuarios");
        //se captura usuario
        objModulosUsuarios.setBuscarUsuario(usuario1);
        //se busca el usuario
        objModulosUsuarios.clicBuscarUsuario();
        //Se espera a que se encuentre clickeable la pantalla a ingresar
        w.until(ExpectedConditions.visibilityOf(objModulosUsuarios.btnAsignarRol()));
        w.until(ExpectedConditions.visibilityOf(objModulosUsuarios.lblLimpiarRol()));
        //Se validan los datos del usuario
        Assert.assertEquals(objModulosUsuarios.getUsuario().toUpperCase(), usuario1.toUpperCase());
        Assert.assertEquals(objModulosUsuarios.getNombreUsuario().toUpperCase(), nombre.toUpperCase());
        Assert.assertEquals(objModulosUsuarios.getNumeroEmpleado().toUpperCase(), numEmpleado.toUpperCase());
        Assert.assertEquals(objModulosUsuarios.getPuesto().toUpperCase(), puesto.toUpperCase());
        Assert.assertEquals(objModulosUsuarios.getEstatus().toUpperCase(), estatusActivo.toUpperCase());
        //se busca rol
        objModulosUsuarios.setBuscarRol(nombreRol1);
        objModulosUsuarios.setBuscarDescripcionRol(descripcionRol1);
        objModulosUsuarios.clicBuscarRol();
        //Se espera a que se encuentre clickeable el boton
        w.until(ExpectedConditions.elementToBeClickable(objModulosUsuarios.lblNombreRol()));
        //Se valida que se haya realizado la busqueda
        Assert.assertEquals(objModulosUsuarios.getNombreRol(), nombreRol1);
        Assert.assertEquals(objModulosUsuarios.getDescripcionRol(), descripcionRol1);
        //Se limpia la consulta
        objModulosUsuarios.clicLimpiarRol();
        //Se espera a que se encuentre clickeable el boton
        w.until(ExpectedConditions.visibilityOf(objModulosUsuarios.btnAsignarRol()));
        w.until(ExpectedConditions.textToBePresentInElement(objModulosUsuarios.lblLimpiarRol(),"No hay registros que mostrar"));
        //se busca rol
        objModulosUsuarios.setBuscarRol(nombreRol1);
        objModulosUsuarios.setBuscarDescripcionRol(descripcionRol1);
        objModulosUsuarios.clicBuscarRol();
        //Se espera a que se encuentre clickeable el boton
        w.until(ExpectedConditions.visibilityOf(objModulosUsuarios.lblNombreRol()));
        //Se asigna rol
        objModulosUsuarios.clicAsignarRol();
        //Se espera a que se encuentre clickeable el boton
        w.until(ExpectedConditions.elementToBeClickable(objModulosUsuarios.btnAceptarAsignarRol()));
        //Se valida que se asigno el modulo
        Assert.assertEquals(objModulosUsuarios.getSeAsignoRol(), "Se asignaron correctamente los módulos del rol, al usuario.");
        //Se cierra la ventana de confirmacion
        objModulosUsuarios.clicAceptarAsignarRol();
    }

    @Test(priority = 2)
    public void test_AsignarModuloAUsuarios() throws InterruptedException {
        //se declara variable para confirmar que se asignen o deasignen modulos
        int cantidadModulos;
        //Se inicia sesion en Recaudador
        objLogin.login(userName, password);
        //Se espera a que se encuentre clickeable el link de administracion de la aplicacion
        w.until(ExpectedConditions.elementToBeClickable(objMenu.lnkAdministracionAplicacion()));
        //Se valida el inicio de sesion
        Assert.assertEquals(objMenu.getMenu(), "Menú");
        //Se ingresa a menu de administración de la aplicación
        objMenu.clicAdministracionAplicacion();
        //Se espera a que se encuentre clickeable la pantalla a ingresar
        w.until(ExpectedConditions.elementToBeClickable(objModulosUsuarios.lnkModulosUsuarios()));
        objModulosUsuarios.clicModulosUsuarios();
        //Se espera a que se encuentre clickeable la pantalla a ingresar
        w.until(ExpectedConditions.elementToBeClickable(objModulosUsuarios.lblAsignacionModulosUsuarios()));
        //Se valida que se ingreso a la pantalla
        Assert.assertEquals(objModulosUsuarios.getTituloPantalla(),"Asignación de módulos a usuarios");
        //se captura usuario
        objModulosUsuarios.setBuscarUsuario(usuario1);
        //se busca el usuario
        objModulosUsuarios.clicBuscarUsuario();
        //Se espera a que se encuentre clickeable la pantalla a ingresar
        w.until(ExpectedConditions.visibilityOf(objModulosUsuarios.btnAsignarRol()));
        w.until(ExpectedConditions.visibilityOf(objModulosUsuarios.lblLimpiarRol()));
        //Se validan los datos del usuario
        Assert.assertEquals(objModulosUsuarios.getUsuario().toUpperCase(), usuario1.toUpperCase());
        Assert.assertEquals(objModulosUsuarios.getNombreUsuario().toUpperCase(), nombre.toUpperCase());
        Assert.assertEquals(objModulosUsuarios.getNumeroEmpleado().toUpperCase(), numEmpleado.toUpperCase());
        Assert.assertEquals(objModulosUsuarios.getPuesto().toUpperCase(), puesto.toUpperCase());
        Assert.assertEquals(objModulosUsuarios.getEstatus().toUpperCase(), estatusActivo.toUpperCase());
        //Se da clic en la pestaña por modulo
        objModulosUsuarios.clicPorModulo();
        //espera explicita para seguir ejecutando la prueba
        w.until(ExpectedConditions.textToBePresentInElement(objModulosUsuarios.lblRecaudadorEstatal() ,"Recaudador estatal"));
        //Se asigna el modulo al usuario
        objModulosUsuarios.clicModuloAsignar();
        objModulosUsuarios.clicAgregarModulo();
        //espera explicita para seguir ejecutando la prueba
        w.until(ExpectedConditions.textToBePresentInElement(objModulosUsuarios.lblRecaudadorEstatal() ,"Recaudador estatal"));
        //Se comprueba que se agregaron los modulos
        objModulosUsuarios.clicAmpliarModulos();
        cantidadModulos = objModulosUsuarios.cantidadModulos();
        System.out.println(cantidadModulos+"");
        Assert.assertNotEquals(cantidadModulos,1);
        //Se desagregan los modulos
        objModulosUsuarios.clicModuloDesasignar();
        objModulosUsuarios.clicQuitarModulo();
        //espera explicita para seguir ejecutando la prueba
        w.until(ExpectedConditions.textToBePresentInElement(objModulosUsuarios.lblSinModulosAsignados() ,"No hay registros para mostrar."));
        //Se valida que se designaron los modulos
        Assert.assertEquals(objModulosUsuarios.getSinModulosAsignados(), "No hay registros para mostrar.");
        //Se asigna el modulo al usuario
        objModulosUsuarios.clicModuloAsignar();
        objModulosUsuarios.clicAgregarModulo();
        //espera explicita para seguir ejecutando la prueba
        w.until(ExpectedConditions.textToBePresentInElement(objModulosUsuarios.lblRecaudadorEstatal() ,"Recaudador estatal"));
        //Se comprueba que se agregaron los modulos
        objModulosUsuarios.clicAmpliarModulos();
        cantidadModulos = objModulosUsuarios.cantidadModulos();
        //System.out.println(cantidadModulos+"");
        Assert.assertNotEquals(cantidadModulos,1);
        //Se cancela la accion
        objModulosUsuarios.clicCancelarModulos();
        //espera explicita para seguir ejecutando la prueba
        w.until(ExpectedConditions.textToBePresentInElement(objModulosUsuarios.lblAccionExitosa() ,"La operación se ha realizado con éxito."));
        //Se valida que se cancelo la accion
        Assert.assertEquals(objModulosUsuarios.getAccionExitosa(), "La operación se ha realizado con éxito.");
        //Se cierra la ventana de cancelacion
        objModulosUsuarios.clicAceptarAccion();
        //espera explicita para seguir ejecutando la prueba
        w.until(ExpectedConditions.textToBePresentInElement(objModulosUsuarios.lblRecaudadorEstatal() ,"Recaudador estatal"));
        //Se asigna el modulo al usuario
        objModulosUsuarios.clicModuloAsignar();
        objModulosUsuarios.clicAgregarModulo();
        //espera explicita para seguir ejecutando la prueba
        w.until(ExpectedConditions.elementToBeClickable(objModulosUsuarios.lblRecaudadorEstatal()));
        //Se guardan los cambios
        objModulosUsuarios.clicGuardarModulos();
        //espera explicita para seguir ejecutando la prueba
        w.until(ExpectedConditions.textToBePresentInElement(objModulosUsuarios.lblAccionExitosa() ,"La operación se ha realizado con éxito."));
        //Se valida que se guardo la accion
        Assert.assertEquals(objModulosUsuarios.getAccionExitosa(), "La operación se ha realizado con éxito.");
        //Se cierra la ventana de guardar cambios
        objModulosUsuarios.clicAceptarAccion();

    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }

}
