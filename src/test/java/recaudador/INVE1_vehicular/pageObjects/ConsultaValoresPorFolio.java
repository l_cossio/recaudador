package recaudador.INVE1_vehicular.pageObjects;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import recaudador.Pagina;

import static com.codeborne.selenide.Selenide.$;

public class ConsultaValoresPorFolio extends Pagina {

    WebDriver driver;

    public ConsultaValoresPorFolio(WebDriver driver) {
        this.driver = driver;
        WebDriverRunner.setWebDriver(driver);
    }

    private By rad_busquedaEnPantalla = new By.ById("j_id_id5:j_id_id47");
    private By lst_tipoDeForma = new By.ById("j_id_id5:navList1");
    private By txt_serie = new By.ById("j_id_id5:j_id_id56");
    private By txt_folio = new By.ById("j_id_id5:j_id_id58");
    private By btn_buscar = new By.ByCssSelector(".x7j");


    public By lbl_serie = new By.ByCssSelector(".xdl > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(2) > span:nth-child(1)");
    public By lbl_tipoForma = new By.ByCssSelector(".xdl > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(2) > span:nth-child(1)");
    public By lbl_estatus = new By.ByCssSelector(".xdl > tbody:nth-child(1) > tr:nth-child(3) > td:nth-child(2) > span:nth-child(1)");
    public By lbl_usuario = new By.ByCssSelector(".xdl > tbody:nth-child(1) > tr:nth-child(4) > td:nth-child(2) > span:nth-child(1)");
    public By lbl_oficina = new By.ByCssSelector(".xdl > tbody:nth-child(1) > tr:nth-child(5) > td:nth-child(2) > span:nth-child(1)");
    public By lbl_recaudacion = new By.ByCssSelector(".xdl > tbody:nth-child(1) > tr:nth-child(6) > td:nth-child(2) > span:nth-child(1)");


    public void llenarCamposConsulta(String clave, String serie, String folio) {
        $(rad_busquedaEnPantalla).click();
        seleccionarPorClave(lst_tipoDeForma, clave);
        $(txt_serie).setValue(serie);
        $(txt_folio).setValue(folio);
    }

    private void seleccionarPorClave(By lst_tipoDeForma, String clave) {
        // Selecciona el que termine con clave
        By opcionAElegir = new By.ByXPath("option[substring(text(), " +
                "string-length(text()) - string-length('"+clave+"') +1) = '"+clave+"']");
        SelenideElement opcion = $(lst_tipoDeForma).find(opcionAElegir);
        $(lst_tipoDeForma).selectOption(opcion.getText());
    }

    public void clickBuscar() {
        $(btn_buscar).click();
    }
}
