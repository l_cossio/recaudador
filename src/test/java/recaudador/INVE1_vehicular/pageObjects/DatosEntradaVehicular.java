package recaudador.INVE1_vehicular.pageObjects;

import recaudador.Base;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class DatosEntradaVehicular extends Base {

    public String datosFormaAleatoria;

    /**
     * Carga de variables para la prueba de Consulta de Predios
     *
     * @throws IOException when file not found
     */
    public void cargaVariablesVehicular() throws IOException {

        // Preparar archivo
        Properties prop = new Properties();
        FileInputStream fis = new FileInputStream
                ("src\\test\\java\\recaudador\\INVE1_vehicular\\dataBank\\vehicular.properties");

        prop.load(fis);
        datosFormaAleatoria= prop.getProperty("datosFormaAleatoria");

    }
}
