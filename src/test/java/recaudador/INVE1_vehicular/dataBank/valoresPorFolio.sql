select f.cc_tifo_tipo_forma as tipo_de_forma,
        f.folio,
        f.serie,
        d.descripcion_corta as estatus,
        f.aa_usua_usuario as usuario,
        o.nombre as oficina,
        r.nombre as recaudacion

from cc_formas f
join AA_USUARIO_OFICINA uo on uo.AA_USUA_USUARIO = f.AA_USUA_USUARIO
join CC_RECAUDACIONES r on r.IDENTIFICADOR = uo.CC_RECA_IDENTIFICADOR
join CC_OFICINAS o on o.IDENTIFICADOR = uo.CC_OFNA_IDENTIFICADOR
join (select valor_minimo, descripcion_corta
      from AA_LISTAS_VALORES
      where DOMINIO = 'CC_FORMAS.ESTATUS'
      ) d on d.valor_minimo = f.ESTATUS
fetch first 100 rows only
