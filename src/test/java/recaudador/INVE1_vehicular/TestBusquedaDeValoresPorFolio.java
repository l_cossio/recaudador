package recaudador.INVE1_vehicular;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import recaudador.INLO1_login.pageObjects.Login;
import recaudador.INLO1_login.pageObjects.Menu;
import recaudador.INVE1_vehicular.pageObjects.ConsultaValoresPorFolio;
import recaudador.INVE1_vehicular.pageObjects.DatosEntradaVehicular;

import java.io.IOException;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;

@Test(groups = {"TAB"})
public class TestBusquedaDeValoresPorFolio extends DatosEntradaVehicular {


    ConsultaValoresPorFolio objConsulta;
    Menu objMenu;
    Login objLogin;

    @DataProvider
    public Object[][] datosFormaAleatoria() throws IOException {
        Object[][] datos = new Object[1][1];
        datos[0][0] = getRandomRow(datosFormaAleatoria);
        return datos;
    }

    @BeforeTest
    public void setup() throws IOException {

        cargaVariablesVehicular();
        driver = initializerDriver();
        driver.manage().window().maximize();
        objLogin = new Login(driver);
        objMenu = new Menu(driver);
        objConsulta = new ConsultaValoresPorFolio(driver);
        driver.get(url);

        objLogin.login(userName, password);
    }

    @Test(priority = 1)
    public void entrarAConsultaDeValoresPorFolio() {
        // Arrange & Act
        objMenu.clickMenuVehicular();
        objMenu.clickConsultaDeValoresPorFolio();

        // Assert
        Assert.assertEquals(driver.getTitle(),"Consulta de valores por folio");
    }

    @Test(priority = 2, dataProvider = "datosFormaAleatoria")
    public void consultaPorFormaAleatoria(String datos) {
        // Arrange
        String[] datosArr = datos.split(",");
        String clave = datosArr[0];
        String folio = datosArr[1];
        String serie = datosArr[2];
        String estatus = datosArr[3].toUpperCase();
        String usuario = datosArr[4];
        String oficina = datosArr[5];
        String recaudacion = datosArr[6];

        objConsulta.llenarCamposConsulta(clave, serie, folio);

        // Act
        objConsulta.clickBuscar();

        // Assert
        $(objConsulta.lbl_serie).shouldHave(text(serie + "-" + folio));
        $(objConsulta.lbl_tipoForma).shouldHave(text(clave));
        $(objConsulta.lbl_estatus).shouldHave(text(estatus));
        $(objConsulta.lbl_usuario).shouldHave(text(usuario));
        $(objConsulta.lbl_oficina).shouldHave(text(oficina));
        $(objConsulta.lbl_recaudacion).shouldHave(text(recaudacion));
    }

    @AfterTest
    public void tearDown() {
        driver.quit();
    }
}
