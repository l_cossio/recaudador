alter SESSION set NLS_DATE_FORMAT = 'DD-MM-YYYY';
SELECT numero_infraccion as folio_boleta,       --0
        placa_vehiculo,                         --1
        propietario_vehiculo,                   --2
        nombre_infractor as conductor,          --3
        domicilio_infractor as domicilio,       --4
        folio_identificacion as identificacion, --5
        campo10 as folio_garantia,              --6
        fecha_infraccion                        --7
FROM it_infracciones
WHERE estatus = 'AC'
AND numero_infraccion is not null
AND placa_vehiculo is not null
AND propietario_vehiculo is not null
AND nombre_infractor is not null
AND domicilio_infractor is not null
AND folio_identificacion is not null
AND campo10 is not null
AND fecha_infraccion is not null

FETCH FIRST 500 ROWS ONLY