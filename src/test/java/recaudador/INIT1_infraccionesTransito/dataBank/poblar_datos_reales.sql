--0 nombre_comercial, 1 razon_social, 2 rfc_alfa, 3 rfc_fecha, 4 rfc_homo, 5 clave_catastral, 6 calle, 7 numero_exterior

SELECT pm.nombre_comercial, pm.razon_social, pm.rfc_alfa, pm.rfc_fecha, pm.rfc_homo, pred.clave_catastral, d.calle, d.numero_exterior
FROM pc_sujetos_objetos so
JOIN pr_predios pred on pred.pc_obje_identificador = so.pc_obje_identificador
JOIN pc_personas_morales pm on pm.no_persona = so.pc_pemo_no_persona
JOIN pc_domicilios d on d.pc_obje_identificador = so.pc_obje_identificador
WHERE so.pc_obje_serie = 2007
AND so.estatus = 'AC'
AND so.pc_pemo_no_persona is not null
FETCH FIRST 500 ROWS ONLY