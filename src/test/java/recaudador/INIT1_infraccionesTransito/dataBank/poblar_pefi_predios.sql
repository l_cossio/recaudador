--0 nombre, 1 primer_apellido, 2 segundo_apellido, 3rfc_alfa, 4 rfc_fecha, 5 rfc_homo, 6 clave_catastral, 7 calle, 8 numero_exterior

SELECT pf.nombre, pf.primer_apellido, pf.segundo_apellido, pf.rfc_alfa, pf.rfc_fecha, pf.rfc_homo, pred.clave_catastral, d.calle, d.numero_exterior
FROM pc_sujetos_objetos so
JOIN pr_predios pred on pred.pc_obje_identificador = so.pc_obje_identificador
JOIN pc_personas_fisicas pf on pf.no_persona = so.pc_pefi_no_persona
JOIN pc_domicilios d on d.pc_obje_identificador = so.pc_obje_identificador
WHERE so.pc_obje_serie = 2007
AND so.estatus = 'AC'
AND so.pc_pefi_no_persona is not null
AND length(pred.clave_catastral) = 40
AND pf.rfc_alfa is not null
AND pf.rfc_fecha is not null
AND pf.rfc_homo is not null
AND length(pf.rfc_alfa) = 4
AND REGEXP_LIKE(d.numero_exterior, '^[[:digit:]]+$');
FETCH FIRST 500 ROWS ONLY