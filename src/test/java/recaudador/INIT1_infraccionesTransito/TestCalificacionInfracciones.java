package recaudador.INIT1_infraccionesTransito;

import com.github.javafaker.Faker;
import fusor.Fusor;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;
import recaudador.INIT1_infraccionesTransito.pageObjects.CalificacionInfracciones;
import recaudador.INIT1_infraccionesTransito.pageObjects.DatosEntradaInfracciones;
import recaudador.INLO1_login.pageObjects.Login;
import recaudador.INLO1_login.pageObjects.Menu;

import java.io.IOException;
import java.text.ParseException;

@Test(groups = {"TAB"})
public class TestCalificacionInfracciones extends DatosEntradaInfracciones {

    WebDriver driver;
    Fusor fusor;
    Faker faker;
    CalificacionInfracciones objCalificacion;
    Login objLogin;
    Menu objMenu;

    /**
     * Nombre: Calificación de Infracción de Tránsito.
     * Descripción: Calificar una infracción de tránsito.
     * Objetivos de la prueba:
     *   1. Buscar infracciones por tipo
     *   2. Buscar infracciones por folio
     *   3. Buscar infracciones por placa
     *   4. Buscar infracciones por propietario
     *   5. Buscar usando todos los campos
     *   6. Intentar dar un descuento numérico mayor al importe, debe de fallar
     *   7. Intentar dar un descuento numérico igual al importe, debe fallar
     *   8. Intentar dar un descuento porcentual mayor del 100% debe fallar
     *   9. Intentar dar un descuento porcentual del 100%, debe fallar
     *   10. Dar un descuento numérico del 20% o menos, debe funcionar
     *   11. Dar un descuento porcentual del 20% o menos, debe funcionar
     *   12. Dar descuentos negativos, debe fallar
     *   13. Dar descuentos en cero (cantidad y porcentaje), deben funcionar
     * Autor: Luis Cossío Ramírez.
     * Fecha de creación: 23-02-2021.
     * Modificado por: N/A.
     * Fecha de modificación: N/A.
     */
    @BeforeTest
    public void setup() throws IOException {

        cargaVariablesInfracciones();

        faker = new Faker();
        fusor = new Fusor(cliente);
        driver = initializerDriver();
        driver.manage().window().maximize();
        objLogin = new Login(driver);
        objMenu = new Menu(driver);
        objCalificacion = new CalificacionInfracciones(driver);

        driver.get(url);

        objLogin.login(userName, password);

        objMenu.clickInfraccionesTransito();
        objCalificacion.clickCalificacion();
    }

    @Test(priority = 1)
    public void buscarInfracciones() throws IOException {
        // Buscar infracciones por tipo
        objCalificacion.buscarPorTipo();
        objCalificacion.clickLimpiar();

        // Buscar infracciones por folio
        String folio = getRandomRow(infracciones).split(",")[0];
        objCalificacion.buscarPorFolio(folio);
        objCalificacion.clickLimpiar();

        // Buscar infracciones por placa
        String placa = getRandomRow(infracciones).split(",")[1];
        objCalificacion.buscarPorPlaca(placa);
        objCalificacion.clickLimpiar();

        // Buscar infracciones por propietario
        String propietario = getRandomRow(infracciones).split(",")[2];
        objCalificacion.buscarPorPropietario(propietario);
        objCalificacion.clickLimpiar();

        // Buscar usando todos los campos
        String[] s = getRandomRow(infracciones).split(",");
        objCalificacion.buscarPorTodosLosCampos(s[0], s[1], s[2]);
        objCalificacion.clickLimpiar();
    }

    @Test(priority = 2)
    public void hacerDescuentos() throws IOException, ParseException, InterruptedException {
        double maxValid = 20.0;

        // Ingresar a un folio
        objCalificacion.buscarPorFolio(getRandomRow(foliosExistentes));
        objCalificacion.clickSiguiente();

        // Intentar dar un descuento numérico mayor al importe, debe de fallar
        objCalificacion.descuento((Math.random() * 100) + 100,true, false);

        // Intentar dar un descuento numérico igual al importe, debe fallar
        objCalificacion.descuento(100, true, false);

        // Intentar dar un descuento porcentual mayor del 100% debe fallar
        objCalificacion.descuento((Math.random() * 100) + 100, false, false);

        // Intentar dar un descuento porcentual del 100%, debe fallar
        objCalificacion.descuento(100, false, false);

        // Dar un descuento numérico del 30% o menos, debe funcionar
        objCalificacion.descuento(Math.random() * maxValid, true, true);

        // Dar un descuento porcentual del 30% o menos, debe funcionar
        objCalificacion.descuento(Math.random() * maxValid, false, true);

        // Intentar dar descuento numérico negativo, debe fallar
        objCalificacion.descuento( Math.random() * (-maxValid), true, false);

        // Intentar dar descuento porcentual negativo, debe fallar
        objCalificacion.descuento( Math.random() * (-maxValid), false, false);

        // Intentar dar descuento numérico en cero, debe fallar
        objCalificacion.descuento(0.0, true, true);

        // Intentar dar descuento porcentual en cero, debe fallar
        objCalificacion.descuento(0.0, false, true);

    }

    @AfterTest
    public void cerrarTodo() {
        driver.quit();
    }
}