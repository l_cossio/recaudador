package recaudador.INIT1_infraccionesTransito;

import com.github.javafaker.Faker;
import fusor.Fusor;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;
import recaudador.INIT1_infraccionesTransito.pageObjects.DatosEntradaInfracciones;
import recaudador.INIT1_infraccionesTransito.pageObjects.Infracciones;
import recaudador.INIT1_infraccionesTransito.pageObjects.MantenimientoInfracciones;
import recaudador.INLO1_login.pageObjects.Login;
import recaudador.INLO1_login.pageObjects.Menu;

import java.io.IOException;
import java.util.ArrayList;

@Test(groups = {"TAB"})
public class TestMantenimientoInfracciones extends DatosEntradaInfracciones {
    WebDriver driver;
    Fusor fusor;
    Faker faker;
    Infracciones objInfracciones;
    MantenimientoInfracciones objMantenimiento;
    Login objLogin;
    Menu objMenu;


    @BeforeTest
    public void setup() throws IOException {

        cargaVariablesInfracciones();
        faker = new Faker();
        fusor = new Fusor(cliente);
        driver = initializerDriver();
        driver.manage().window().maximize();
        objLogin = new Login(driver);
        objMenu = new Menu(driver);
        objInfracciones = new Infracciones(driver);
        objMantenimiento = new MantenimientoInfracciones(driver);

        driver.get(url);

        objLogin.login(userName, password);
    }

    @BeforeMethod
    public void ingresarAInfracciones() {
        objMenu.clickInfraccionesTransito();
    }


    /**
     * Nombre: Mantenimiento de Infracción de Tránsito.
     * Descripción: Modificar una infracción de tránsito.
     * Objetivos de la prueba:
     *   - Consulta de una infracción activa
     *   - Modificar todos los datos de la infracción
     *   - Ingresar una nueva placa de un vehiculo activo
     *   - Modificar los datos del conductor
     *   - Ingresar y/o modificar los datos de garantía
     *   - Ingresar varios motivos
     *   - Eliminar un motivo aleatorio
     *   - Guardar la infracción
     *   - Volver a consultar la misma infracción y validar que se guardaran los datos nuevos
     * Autor: Luis Cossío Ramírez.
     * Fecha de creación: 29-01-2021.
     * Modificado por: N/A.
     * Fecha de modificación: N/A.
     */
    @Test()
    public void mantenimientoDeInfraccion() throws IOException {

        // Ingresar a la pantalla de mantenimiento de infracciones
        objMantenimiento.clicklnkMantenimiento();

        // Consulta de una infracción activa
        String folio = getRandomRow(foliosExistentes);
        objMantenimiento.ingresarFolio(folio);
        objInfracciones.validarFolio(folio);

        // Modificar todos los datos de la infracción
        objInfracciones.llenarDatosDeInfraccion(fusor);

        // Ingresar nuevos datos de un vehiculo activo
        objInfracciones.llenarDatosDelVehiculo(fusor);

        // Modificar los datos del conductor
        objInfracciones.llenarDatosDelConductor(fusor);

        // Ingresar y/o modificar los datos de garantía
        objInfracciones.llenarDatosDeLaGarantia(fusor);

        // Ingresar varios motivos
        objInfracciones.agregarMotivoAleatorio(25);
        objInfracciones.agregarMotivoAleatorio(25);
        objInfracciones.agregarMotivoAleatorio(25);
        objInfracciones.agregarObservaciones();

        // Eliminar un motivo aleatorio
        objInfracciones.eliminarMotivoAleatorio();

        // Guardar la infracción
        ArrayList<String> datos = objInfracciones.recolectarDatosEnPantalla();
        objInfracciones.clickGuardar();
        objInfracciones.validarActualizacionExitosa();

        // Volver a consultar la misma infracción y validar que se guardaran los datos nuevos
        regresarAlInicio();
        ingresarAInfracciones();
        objMantenimiento.clicklnkMantenimiento();
        objMantenimiento.ingresarFolio(folio);
        objInfracciones.corroborarDatos(datos);
    }


    @AfterMethod
    public void regresarAlInicio() {
        objInfracciones.clickHome();
    }

    @AfterTest
    public void cerrar() {
        driver.quit();
    }
}
