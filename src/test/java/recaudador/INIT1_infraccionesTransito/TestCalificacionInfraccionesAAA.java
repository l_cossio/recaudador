package recaudador.INIT1_infraccionesTransito;

import com.codeborne.selenide.SelenideElement;
import com.github.javafaker.Faker;
import fusor.Fusor;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import recaudador.INIT1_infraccionesTransito.pageObjects.CalificacionInfracciones;
import recaudador.INIT1_infraccionesTransito.pageObjects.CalificacionInfraccionesAAA;
import recaudador.INIT1_infraccionesTransito.pageObjects.DatosEntradaInfracciones;
import recaudador.INLO1_login.pageObjects.Login;
import recaudador.INLO1_login.pageObjects.Menu;

import java.io.IOException;
import java.text.ParseException;

import static com.codeborne.selenide.Condition.appear;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class TestCalificacionInfraccionesAAA extends DatosEntradaInfracciones {

    WebDriver driver;
    Fusor fusor;
    Faker faker;
    CalificacionInfraccionesAAA objCalificacion;
    Login objLogin;
    Menu objMenu;

    double maxValid = 20.0;
    /**
     * Nombre: Calificación de Infracción de Tránsito.
     * Descripción: Calificar una infracción de tránsito.
     * Objetivos de la prueba:
     *   1. Buscar infracciones por tipo
     *   2. Buscar infracciones por folio
     *   3. Buscar infracciones por placa
     *   4. Buscar infracciones por propietario
     *   5. Buscar usando todos los campos
     *   6. Intentar dar un descuento numérico mayor al importe, debe de fallar
     *   7. Intentar dar un descuento numérico igual al importe, debe fallar
     *   8. Intentar dar un descuento porcentual mayor del 100% debe fallar
     *   9. Intentar dar un descuento porcentual del 100%, debe fallar
     *   10. Dar un descuento numérico del 20% o menos, debe funcionar
     *   11. Dar un descuento porcentual del 20% o menos, debe funcionar
     *   12. Dar descuentos negativos, debe fallar
     *   13. Dar descuentos en cero (cantidad y porcentaje), deben funcionar
     * Autor: Luis Cossío Ramírez.
     * Fecha de creación: 23-02-2021.
     * Modificado por: N/A.
     * Fecha de modificación: N/A.
     */
    @BeforeTest
    public void setup() throws IOException {

        cargaVariablesInfracciones();

        faker = new Faker();
        fusor = new Fusor(cliente);
        driver = initializerDriver();
        driver.manage().window().maximize();
        objLogin = new Login(driver);
        objMenu = new Menu(driver);
        objCalificacion = new CalificacionInfraccionesAAA(driver);

        driver.get(url);

        objLogin.login(userName, password);

        objMenu.clickInfraccionesTransito();
        objCalificacion.clickCalificacion();
    }

    @Test(priority = 1)
    public void buscarInfraccionesPorTipo() {
        objCalificacion.clickLimpiar();
        // Buscar infracciones por tipo
        objCalificacion.buscarPorTipo();
        validarBusqueda();
    }

    @Test(priority = 1)
    public void buscarInfraccionesPorFolio() throws IOException {
        objCalificacion.clickLimpiar();

        // Buscar infracciones por folio
        String folio = getRandomRow(infracciones).split(",")[0];
        objCalificacion.buscarPorFolio(folio);

        validarBusqueda();
    }

    @Test(priority = 1)
    public void buscarInfraccionesPorPlaca() throws IOException {
        objCalificacion.clickLimpiar();

        // Buscar infracciones por placa
        String placa = getRandomRow(infracciones).split(",")[1];
        objCalificacion.buscarPorPlaca(placa);

        validarBusqueda();
    }

    @Test(priority = 1)
    public void buscarInfraccionesPorPropietario() throws IOException {
        objCalificacion.clickLimpiar();

        // Buscar infracciones por propietario
        String propietario = getRandomRow(infracciones).split(",")[2];
        objCalificacion.buscarPorPropietario(propietario);

        validarBusqueda();
    }

    @Test(priority = 1)
    public void buscarInfraccionesPorTodosLosCampos() throws IOException {
        objCalificacion.clickLimpiar();

        // Buscar usando todos los campos
        String[] s = getRandomRow(infracciones).split(",");
        objCalificacion.buscarPorTodosLosCampos(s[0], s[1], s[2]);

        validarBusqueda();
    }

    @Test(priority = 2)
    public void IngresarAFolioExistente() throws IOException, InterruptedException {
        // Ingresar a un folio

        // Arrange
        objCalificacion.clickLimpiar();
        objCalificacion.buscarPorFolio(getRandomRow(foliosExistentes));

        // Act
        objCalificacion.clickSiguiente();

        // Assert
        $(objCalificacion.btn_aplicarDescuento).should(appear);
    }

    @Test(priority = 3)
    public void hacerDescuentoNuméricoMayorAlImporte() throws IOException, ParseException, InterruptedException {
        // Intentar dar un descuento numérico mayor al importe, debe de fallar

        //Arrange
        //Act
        SelenideElement txtDescuento = objCalificacion.descuento((Math.random() * 100) + 100, true);

        //Assert
        validarDescuentoInvalido();
        txtDescuento.clear();
    }

    @Test(priority = 3)
    public void hacerDescuentoNuméricoIgualAlImporte() throws IOException, ParseException, InterruptedException {
        // Intentar dar un descuento numérico igual al importe, debe fallar

        // Arrange
        // Act
        SelenideElement txtDescuento = objCalificacion.descuento(100, true);

        //Assert
        validarDescuentoInvalido();
        txtDescuento.clear();
    }

    @Test(priority = 3)
    public void hacerDescuentoPorcentualMayorDel100() throws IOException, ParseException, InterruptedException {
        // Intentar dar un descuento porcentual mayor del 100% debe fallar

        // Arrange
        // Act
        SelenideElement txtDescuento = objCalificacion.descuento((Math.random() * 100) + 100, false);

        //Assert
        validarDescuentoInvalido();
        txtDescuento.clear();
    }
    @Test(priority = 3)
    public void hacerDescuentoPorcentualDel100() throws IOException, ParseException, InterruptedException {
        // Intentar dar un descuento porcentual del 100%, debe fallar

        // Arrange
        // Act
        SelenideElement txtDescuento = objCalificacion.descuento(100, false);

        //Assert
        validarDescuentoInvalido();
        txtDescuento.clear();
    }

    @Test(priority = 3)
    public void hacerDescuentoNumericoDel30oMenor() throws IOException, ParseException, InterruptedException {
        // Dar un descuento numérico del 30% o menos, debe funcionar

        // Arrange
        // Act
        objCalificacion.descuento(Math.random() * maxValid, true);

        // Assert
        validarDescuentoValido();
    }

    @Test(priority = 3)
    public void hacerDescuentoPorcentualDel30oMenor() throws IOException, ParseException, InterruptedException {
        // Dar un descuento porcentual del 30% o menos, debe funcionar

        // Arrange
        // Act
        objCalificacion.descuento(Math.random() * maxValid, false);

        // Assert
        validarDescuentoValido();
    }
    @Test(priority = 3)
    public void hacerDescuentoNumericoNegativo() throws IOException, ParseException, InterruptedException {
        // Intentar dar descuento numérico negativo, debe fallar

        // Arrange
        // Act
        SelenideElement txtDescuento = objCalificacion.descuento(Math.random() * (-maxValid), true);

        //Assert
        validarDescuentoInvalido();
        txtDescuento.clear();
    }

    @Test(priority = 3)
    public void hacerDescuentoPorcentualNegativo() throws IOException, ParseException, InterruptedException {
        // Intentar dar descuento porcentual negativo, debe fallar

        // Arrange
        // Act
        SelenideElement txtDescuento = objCalificacion.descuento(Math.random() * (-maxValid), false);

        //Assert
        validarDescuentoInvalido();
        txtDescuento.clear();
    }

    @Test(priority = 3)
    public void hacerDescuentoNumericoEn0() throws IOException, ParseException, InterruptedException {
        // Intentar dar descuento numérico en cero, no debe fallar

        // Arrange
        // Act
        objCalificacion.descuento(0.0, true);

        // Assert
        validarDescuentoValido();
    }
    @Test(priority = 3)
    public void hacerDescuentoPorcentualEn0() throws IOException, ParseException, InterruptedException {
        // Intentar dar descuento porcentual en cero, no debe fallar

        // Arrange
        // Act
        objCalificacion.descuento(0.0, false);

        // Assert
        validarDescuentoValido();
    }


    @AfterTest
    public void cerrarTodo() {
        driver.quit();
    }

    private void validarBusqueda() {
        assertTrue($$(objCalificacion.cel_folios).size() > 0);
        assertFalse($(objCalificacion.img_error).isDisplayed());
    }

    private void validarDescuentoValido() {
        $(objCalificacion.pop_ventanaInfo).shouldNot(appear);
    }

    private void validarDescuentoInvalido() {
        //Assert
        $(objCalificacion.pop_ventanaInfo).shouldHave(objCalificacion.descuentoInvalido);
        objCalificacion.cerrarVentanaInfo();
    }
}