package recaudador.INIT1_infraccionesTransito;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;
import recaudador.INIT1_infraccionesTransito.pageObjects.ConsultaDeInfracciones;
import recaudador.INIT1_infraccionesTransito.pageObjects.DatosEntradaInfracciones;
import recaudador.INIT1_infraccionesTransito.pageObjects.DetallesDeInfraccion;
import recaudador.INLO1_login.pageObjects.Login;
import recaudador.INLO1_login.pageObjects.Menu;

import java.io.IOException;
import java.text.ParseException;
import java.util.Arrays;

@Test(groups = {"TAB"})
public class TestConsultaInfracciones extends DatosEntradaInfracciones {
    WebDriver driver;
    ConsultaDeInfracciones objConsultaInfracciones;
    DetallesDeInfraccion objDetallesInfraccion;
    Login objLogin;
    Menu objMenu;

    @BeforeTest
    public void setup() throws IOException {

        cargaVariablesConsultaInfracciones();

        driver = initializerDriver();
        driver.manage().window().maximize();
        objLogin = new Login(driver);
        objMenu = new Menu(driver);
        objConsultaInfracciones = new ConsultaDeInfracciones(driver);
        objDetallesInfraccion = new DetallesDeInfraccion(driver);
        driver.get(url);

        objLogin.login(userName, password);
    }

    @BeforeMethod
    public void ingresarAConsultaDeInfracciones() {
        objMenu.clickInfraccionesTransito();
        objConsultaInfracciones.clickConsultaInfracciones();
    }

    /**
     * Nombre: Consulta de infracciones por campos individuales.
     * Descripción: Consultar predios a través de los diferentes parámetros de Predio.
     * Objetivos de la prueba:
     *   - Consultar por folio
     *   - Consultar por placa
     *   - Consultar por propietario
     *   - Consultar por conductor
     *   - Consultar por domicilio
     *   - Consultar por numero de identificación
     *   - Consultar por folio de la garantía
     *   - Consultar por fecha de infracción
     * Autor: Luis Cossío Ramírez.
     * Fecha de creación: 08-03-2021.
     * Modificado por: N/A.
     * Fecha de modificación: N/A.
     */
    @Test(priority = 1)
    public void consultasPorCamposIndividuales() throws IOException {

        String[] row = getRandomRow(consultasInfracciones).split("\t");
        // Consultar por tipo de infracción
        objConsultaInfracciones.consultarPorTipoInfraccionAleatoria();
        objConsultaInfracciones.clickLimpiar();

        // Consultar por folio
        String folio = row[0];
        objConsultaInfracciones.consultaPorFolio(folio);
        objConsultaInfracciones.clickLimpiar();

        // Consultar por placa
        String placa = row[1];
        objConsultaInfracciones.consultaPorPlaca(placa);
        objConsultaInfracciones.clickLimpiar();

        // Consultar por propietario
        String propietario = row[2];
        objConsultaInfracciones.consultaPorPropietario(propietario);
        objConsultaInfracciones.clickLimpiar();

        // Consultar por conductor
        String conductor = row[3];
        objConsultaInfracciones.consultaPorConductor(conductor);
        objConsultaInfracciones.clickLimpiar();

        // Consultar por domicilio
        String domicilio = row[4];
        objConsultaInfracciones.consultaPorDomicilio(domicilio);
        objConsultaInfracciones.clickLimpiar();

        // Consultar por numero de identificación
        String id = row[5];
        objConsultaInfracciones.consultaPorId(id);
        objConsultaInfracciones.clickLimpiar();

//        // Consultar por folio de la garantía
//        String folioGarantia = row[6];
//        objConsultaInfracciones.consultaPorGarantia(folioGarantia);
//        objConsultaInfracciones.clickLimpiar();


        // Consultar por fecha de infracción
        String fecha = row[7];
        objConsultaInfracciones.consultaPorFecha(fecha);
        objConsultaInfracciones.clickLimpiar();

    }
    /**
     * Nombre: Consulta y verificación de datos de infracción.
     * Descripción: Consultar predios a través de los diferentes parámetros de Predio.
     * Objetivos de la prueba:
     *   - Consultar por folio, placa, propietario, conductor, domicilio, numero de identificación, folio de garantía y folio de infracción
     *   - Verificar esos campos en la siguiente pantalla
     *   - Verificar que tenga al menos un motivo
     *   - Verificar que el total del adeudo sea mayor a cero
     * Autor: Luis Cossío Ramírez.
     * Fecha de creación: 08-03-2021.
     * Modificado por: N/A.
     * Fecha de modificación: N/A.
     */
    @Test(priority = 2)
    public void consultaYVerificacionDeDatosDeInfraccion() throws IOException, ParseException {

        // Consultar por folio, placa, propietario, conductor, domicilio, numero de identificación, folio de garantía y folio de infracción
        String[] row = getRandomRow(consultasInfracciones).split("\t");
        objConsultaInfracciones.consultaPorTodosLosCampos(Arrays.copyOfRange(row, 0, 6));
        objConsultaInfracciones.clickSiguiente();

        // Verificar esos campos en la siguiente pantalla
        objDetallesInfraccion.verificarTodosLosCampos(row);

        // Verificar que tenga al menos un motivo
        objDetallesInfraccion.verificarCantidadDeMotivos(1);

        // Verificar que el total del adeudo sea mayor a cero
        objDetallesInfraccion.verificarAdeudoMayorA(0);

    }

    @AfterMethod
    public void regresar() {
        objDetallesInfraccion.clickHome();
    }

    @AfterTest
    public void salir() {
        driver.quit();
    }
}
