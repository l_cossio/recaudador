package recaudador.INIT1_infraccionesTransito.pageObjects;

import com.codeborne.selenide.*;
import com.github.javafaker.Faker;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import recaudador.Pagina;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;

public class CalificacionInfracciones extends Pagina {

    private final WebDriver driver;
    private final Faker faker = new Faker(new Locale("es-MX"));

    public CalificacionInfracciones(WebDriver driver) {
        WebDriverRunner.setWebDriver(driver);
        this.driver = driver;
        Configuration.timeout = 5000; // millis
        Configuration.screenshots = false;
    }

    //// Acceso
    private By lnk_calificacionInfracciones = new By.ByLinkText("Calificación de infracciones");

    //// Busqueda Infracciones
    private By lst_tipoInfraccion = new By.ById("pt1:smc1::content");
    private By txt_folio = new By.ById("pt1:it1::content");
    private By txt_placa = new By.ById("pt1:it2::content");
    private By txt_propietario = new By.ById("pt1:it3::content");
    private By btn_buscar = new By.ById("pt1:cb1");
    private By btn_limpiar = new By.ById("pt1:cb2");
    private By btn_siguiente = new By.ById("pt1:cb3");

    // Resultados
    private By div_resultados = new By.ById("pt1:t1::db");
    private By tbl_resultados = new By.ByXPath("//div[@id='pt1:t1::db']/table[@class='xza x102']");
    private By row_primerRenglon = new By.ByXPath("//div[@id='pt1:t1::db']/table/tbody/tr");
    private By cel_folios = new By.ByXPath("//div[@id='pt1:t1::db']/table/tbody/tr/td[2]");

    //// Calificacion
    private By pop_ventanaInfo = new By.ById("pt1:d4::_ttxt");
    private By btn_aceptarInfo = new By.ById("pt1:d4::ok");
    private By img_error = new By.ByXPath("//img[contains(@title,'Error')]");

    // Motivos
    private By row_Motivos = new By.ByCssSelector("#pt1\\:t5\\:\\:db > table > tbody > tr");
    private By tbl_motivos = new By.ByCssSelector("#pt1\\:t5\\:\\:db > table");
    private By lbltxt_fechaVencimiento = new By.ById("pt1:id1::content");
    private By txt_explicacion = new By.ById("pt1:it3::content");

    private Condition descuentoInvalido = new Condition("some error displayed") {
        @Override
        public boolean apply(Driver driver, WebElement webElement) {
            return ($(pop_ventanaInfo).isDisplayed()
                    && $(pop_ventanaInfo).getText().equals("Descuento inválido"))
                    || $(img_error).isDisplayed();
        }
    };

    public void clickCalificacion() {
        $(lnk_calificacionInfracciones).click();
        $(lnk_calificacionInfracciones).should(disappear);
        Assert.assertEquals(title(),"Calificación de infracciones");
    }

    public void clickLimpiar() {
        $(btn_limpiar).click();
    }

    private void validarBusqueda() {
        $$(cel_folios).shouldHave(CollectionCondition.sizeGreaterThan(0));
        $(img_error).shouldNot(appear);
    }

    private void buscar(By element, String input){
        $(element).setValue(input);
        $(btn_buscar).click();
        validarBusqueda();
    }
    public void buscarPorTipo() {
        seleccionarOpcionAleatoria(lst_tipoInfraccion);
        $(btn_buscar).click();
        validarBusqueda();
    }

    public void buscarPorPlaca(String placa) {
        buscar(txt_placa, placa);
    }

    public void buscarPorPropietario(String propietario) {
        buscar(txt_propietario, propietario);
    }

    public void buscarPorFolio(String folio) {
        buscar(txt_folio, folio);
    }

    public void buscarPorTodosLosCampos(String folio, String placa, String propietario) {
        $(lst_tipoInfraccion).selectOption("TRANSITO");
        $(txt_folio).setValue(folio);
        $(txt_placa).setValue(placa);
        $(txt_propietario).setValue(propietario);
        $(btn_buscar).click();
        validarBusqueda();

        // valida el dato exacto
        $(row_primerRenglon).find("td:nth-child(1) > span > span").shouldHave(text("TRANSITO"));
        $(row_primerRenglon).find("td:nth-child(2)").shouldHave(text(folio));
        $(row_primerRenglon).find("td:nth-child(3)").shouldHave(text(placa));
        $(row_primerRenglon).find("td:nth-child(4)").shouldHave(text(propietario));

    }

    public void clickSiguiente() throws InterruptedException {
        Thread.sleep(1000);
        $(btn_siguiente).click();
        Thread.sleep(1000);
    }

    private SelenideElement celMotivoAleatorio() throws InterruptedException {
        Thread.sleep(200);
//        int randIdx = faker.number().numberBetween(0, Integer.parseInt($(tbl_motivos).getAttribute("_rowcount")));
//        SelenideElement row_motivo = $("#pt1\\:t5\\:\\:db > table > tbody > tr:nth-child(" + (randIdx + 1) +")");
//        row_motivo.find("td:first-child").scrollIntoView(true).click();
        SelenideElement row_motivo = $("#pt1\\:t5\\:\\:db > table > tbody > tr:nth-child(1)");
        row_motivo.find("td:first-child").scrollIntoView(true).click();
        return row_motivo;
    }


    private void validarDescuentoInvalido() {
        $(pop_ventanaInfo).shouldHave(descuentoInvalido);
    }

    private void validarDescuentoValido() {
        $(pop_ventanaInfo).shouldNot(appear);
    }

    public void descuento(double porcentaje, boolean numerico, boolean valido ) throws ParseException, InterruptedException {
        SelenideElement rowMotivo = celMotivoAleatorio();
        Thread.sleep(200);
        SelenideElement txtDescuento = $(rowMotivo).find("td:nth-child(5) > span > input");
        $(txtDescuento).scrollIntoView(true);
        Thread.sleep(500);
        $(txtDescuento).clear();
        $(txtDescuento).click();

        if (numerico) {
            SelenideElement lblImporte = rowMotivo.find("td:nth-child(6) > span");
            NumberFormat format = NumberFormat.getInstance(Locale.US);
            double importe = format.parse($(lblImporte).getText()).doubleValue();
            double descuento = importe * (porcentaje / 100.0);
            $(txtDescuento).sendKeys(String.format("%.2f", descuento));
            $(txtDescuento).pressEnter();
        } else {
            $(txtDescuento).sendKeys(String.format("%.2f", porcentaje) + "%");
            $(txtDescuento).pressEnter();
        }

        if (valido) {
            validarDescuentoValido();
        } else {
            validarDescuentoInvalido();
            if ($(pop_ventanaInfo).isDisplayed()) {
                $(btn_aceptarInfo).click();
            }
            rowMotivo.find("td:first-child").click();
            txtDescuento.clear();
        }

        if ($(lbltxt_fechaVencimiento).getTagName().equals("input")) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-mm-yyyy");
            $(lbltxt_fechaVencimiento).setValue(sdf.format(faker.date().future(10, TimeUnit.DAYS)));
        }

        $(txt_explicacion).setValue(faker.regexify("[A-Za-z0-9 &.,\"'/\\-_:;#()]{50,100}"));

    }


}
