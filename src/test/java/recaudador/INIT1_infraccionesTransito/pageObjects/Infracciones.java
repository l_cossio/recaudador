package recaudador.INIT1_infraccionesTransito.pageObjects;

import com.codeborne.selenide.*;
import com.github.javafaker.Faker;
import fusor.Fusor;
import fusor.PersonaFisica;
import org.openqa.selenium.*;
import recaudador.Pagina;

import org.testng.Assert;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;

public class Infracciones extends Pagina {

    private final WebDriver driver;

    private Faker faker;

    public Infracciones(WebDriver driver) {
        WebDriverRunner.setWebDriver(driver);
        this.driver = driver;
        Configuration.timeout = 2000; // millis
        Configuration.screenshots= true;
        Configuration.savePageSource = false;
        Configuration.holdBrowserOpen = true;
        faker = new Faker(new Locale("es-MX"));
    }

    // ELEMENTOS
    // Link Entrada
    By lnk_altaDeInfraccionesDeTransito = new By.ByLinkText("Alta de infracciones de tránsito");
    By lnk_home = new By.ById("pt1:i1");

    // Datos de la Infracción
    By lst_tipoInfraccion = new By.ById("pt1:soc32::content");
    By txt_folioBoleta = new By.ById("pt1:it25::content");
    By lbl_folioBoleta = new By.ByCssSelector("#pt1\\:pgl11 > div:nth-child(4)");
    By txt_fechaInfraccion = new By.ById("pt1:id1::content");
    By txt_horaInfraccion = new By.ById("pt1:id2::content");
    By txt_lugarInfraccion = new By.ById("pt1:it2::content");
    By lst_estatusInfraccion = new By.ById("pt1:soc5::content");
    By txt_agente = new By.ById("pt1:it1::content");
    By span_txtAgente = new By.ById("pt1:it1");
    By auf_agente = new By.ByXPath("//ul[@id='pt1:it1::_afrautosuggestpopup']/li");
    By lbl_agente = new By.ById("pt1:ot1");
    By lst_turno = new By.ById("pt1:smc1::content");
    By lst_municipio = new By.ById("pt1:smc2::content");
    By txt_clavePatrulla = new By.ById("pt1:it4::content");

    // Datos del Vehiculo
    By txt_placa = new By.ById("pt1:it5::content");
    By txt_descripcionVehiculo = new By.ById("pt1:it12::content");
    By txt_propietarioVehiculo = new By.ById("pt1:it9::content");

    // Datos del Propietario
    By chk_copiarPropietario = new By.ById("pt1:sbc1::content");
    By txt_conductor = new By.ById("pt1:it22::content");
    By txt_curp = new By.ById("pt1:it213::content");
    By txt_domicilio = new By.ById("pt1:it14::content");
    By lst_idConductor = new By.ById("pt1:soc9::content");
    By txt_numeroId = new By.ById("pt1:it18::content");
    By lst_tipoLicencia = new By.ById("pt1:soc10::content");

    // Datos de la Garantía
    By lst_tipoDeGarantia = new By.ById("pt1:soc4::content");
    By txt_numeroDeParte = new By.ById("pt1:it26::content");
    By txt_folioDeLaGarantia = new By.ById("pt1:it17::content");
    By lst_forma = new By.ById("pt1:it6::content");
    By txt_serie = new By.ById("pt1:it20::content");
    By txt_observacionesGarantia = new By.ById("pt1:it13::content");
    By lst_pension = new By.ById("pt1:soc2::content");

    // Infracciones
    By txt_codigoMotivo = new By.ById("pt1:it23::content");
    By auf_codigoMotivo = new By.ByXPath("//ul[@id='pt1:it23::_afrautosuggestpopup']/li[1]");
    By tbl_motivos = new By.ByXPath("//table[@class='xza x102']/tbody/tr");
    By btn_agregar = new By.ById("pt1:cb7");
    By btn_eliminar = new By.ById("pt1:cb1");
    By btn_guardar = new By.ById("pt1:cb2");
    By btn_limpiar = new By.ById("pt1:cb3");
    By btn_cancelar = new By.ById("pt1:cb4");
    By txt_observacionesInfraccion = new By.ById("pt1:it3::content");
    By cel_codigosMotivos = new By.ByXPath("//div[@class='xza x102']/table/tbody/tr/td[1]"); // 0 o más motivos


    // Popup información
    By btn_aceptarInfo = new By.ById("d1::msgDlg::cancel");
    By lbl_info = new By.ByXPath("//div[@class='x15j']");

    // CONDICIONES
    private Condition autofillLoading = attributeMatching("class", ".*p_AFBusy.*");
    private Condition autofillDisplayed = and("autofill displayed",
            not(attributeMatching("class",".*p_AFBusy.*")),
            attributeMatching("class",".* x1u.*"));

    // MÉTODOS
    public void clickAltaInfracciones() {
        System.out.println("Entrando a alta de infracciones");
        $(lnk_altaDeInfraccionesDeTransito).click();
        $(lnk_altaDeInfraccionesDeTransito).should(disappear);
        Assert.assertEquals(title(),"Alta de infracción");
    }

    public void clickHome() {
        $(lnk_home).click();
    }

    /**
     * Llena los datos de la infraccion
     * @param folioBoleta el folio de la boleta
     * @param fusor el fusor a usar para los datos aleatorios
     */
    public void llenarDatosDeInfraccion(String folioBoleta, Fusor fusor) {
        $(txt_folioBoleta).setValue(folioBoleta);
        llenarDatosDeInfraccion(fusor);
    }
    /**
     * Llena los datos de la infraccion
     * @param fusor el fusor a usar para los datos aleatorios
     */
    public void llenarDatosDeInfraccion(Fusor fusor) {

        System.out.println("Llenando datos de infracción");

        Faker faker = new Faker();

        // Generar fecha y definir formatos
        SimpleDateFormat sdfFecha = new SimpleDateFormat("ddMMyyyy");
        SimpleDateFormat sdfFecha2 = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat sdfHora = new SimpleDateFormat("hhmma");
        Date date = faker.date().past(365, TimeUnit.DAYS);

        // Generar datos
        String fechaInfraccion = sdfFecha.format(date);
        String horaInfraccion = sdfHora.format(date);
        String lugarInfraccion = fusor.direccion().calle;
        String clavePatrulla = faker.bothify("??###");

        // Llenar campos
        $(lst_tipoInfraccion).selectOption("TRANSITO");
        $(lst_tipoInfraccion).shouldHave(text("TRANSITO"));

        $(txt_fechaInfraccion).setValue(fechaInfraccion);
        $(txt_fechaInfraccion).shouldHave(value(sdfFecha2.format(date)));

        $(txt_horaInfraccion).setValue(horaInfraccion).sendKeys(Keys.BACK_SPACE);
        $(txt_lugarInfraccion).setValue(lugarInfraccion);

        for (int i=0; i<100; i++) {
            // Repetir hasta que se seleccione uno existente
            String agente = faker.regexify("A[0-9]{1,3}");
            $(txt_agente).setValue(agente);

            try {
                // Esperar a que salgan las opciones
                $(auf_agente).shouldBe(visible);
                Thread.sleep(100);

                // Elegir la primera
                $(txt_agente).sendKeys(Keys.ARROW_DOWN, Keys.ENTER);

                // Verificar que se eligió
                $(lbl_agente).shouldHave(text($(txt_agente).getValue()));
                break;

            } catch (Throwable ignored) { }
        }

        $(txt_agente).sendKeys(Keys.TAB);
        seleccionarOpcionAleatoria(lst_turno);
        seleccionarOpcionAleatoria(lst_municipio);
        $(txt_clavePatrulla).setValue(clavePatrulla);
    }

    public void llenarDatosDelVehiculo(Fusor fusor) {

        System.out.println("Llenando datos del vehículo");

        String tipoPlaca = "";

        // Ingresar una placa
        String[] campoPlaca = getCampoPlaca(fusor,0.5);
        tipoPlaca = campoPlaca[0];
        $(txt_placa).setValue(campoPlaca[1]).sendKeys(Keys.TAB);

        // Esperar a que salga un mensaje de error (o no)
        try {
            $(btn_aceptarInfo).should(appear).click();
        } catch (Throwable ignored) {
            // No apareció error
        }

        if (tipoPlaca.equals("real")) {
            $(txt_descripcionVehiculo).shouldBe(readonly);
            $(txt_propietarioVehiculo).shouldBe(readonly);
        } else {
            $(txt_descripcionVehiculo).setValue(faker.regexify("[A-Za-z &.,'\"/\\-_:;#()]{100,200}"));
            $(txt_propietarioVehiculo).setValue(fusor.personaFisica().nombreCompleto);
        }
    }

    public void llenarDatosDelConductor(Fusor fusor) {

        System.out.println("Llenando datos del conductor");

        Configuration.timeout = 3000;
        PersonaFisica persona = fusor.personaFisica();
        $(txt_conductor).setValue(persona.nombreCompleto);

        $(txt_curp).setValue(persona.curp);
        $(txt_domicilio).setValue(fusor.direccion().domicilio);

        seleccionarOpcionAleatoria(lst_idConductor);

        String idConductor = $(lst_idConductor).getSelectedText();

        switch (idConductor) {
            case "INE":
                $(txt_numeroId).setValue(faker.numerify("##########"));
                break;

            case "LICENCIA":
                $(txt_numeroId).setValue(faker.numerify("#########"));
                seleccionarOpcionAleatoria(lst_tipoLicencia);
                break;

            case "PASAPORTE":
                $(txt_numeroId).setValue(faker.numerify("############"));
                break;

            default:
        }
    }

    public void llenarDatosDeLaGarantia(Fusor fusor) {

        System.out.println("Llenando datos de la garantía");

        seleccionarOpcionAleatoria(lst_tipoDeGarantia);

        String tipoGarantia = $(lst_tipoDeGarantia).getAttribute("title");

        switch (tipoGarantia) {
            case "LICENCIA":
                $(txt_folioDeLaGarantia).setValue(faker.regexify("[A-Z0-9]{4,6}"));
                seleccionarOpcionAleatoria($(lst_forma));
                $(txt_serie).setValue(faker.regexify("[A-Z0-9]{10}"));
                break;
            case "1 PLACA":
                break;
            case "2 PLACAS":
                break;
            case "TARJETA DE CIRCULACIÓN":
                $(txt_folioDeLaGarantia).setValue(faker.numerify("######"));
                break;
            case "VEHÍCULO":
                seleccionarOpcionAleatoria($(lst_pension));
                break;
            case "OTRAS":
                $(txt_folioDeLaGarantia).setValue(faker.numerify("#######"));
                break;

        }
        $(txt_observacionesGarantia).setValue(faker.regexify("[A-Za-z &.,'\"/\\-_:;#()]{100,200}"));
        $(txt_numeroDeParte).setValue(faker.numerify("#####"));
    }

    public void agregarMotivoAleatorio(int maxIntentos) {

        System.out.println("Agregando motivo aleatorio");

        Configuration.timeout = 1000;

        int cantidadActual = $$(tbl_motivos).size();
        boolean agregado = false;

        for (int i=0; i < maxIntentos; i++) {
            $(txt_codigoMotivo).setValue(faker.regexify("[0-9]{1,3}"));
            try {
                $(auf_codigoMotivo).shouldBe(enabled);
                Thread.sleep(250);
                $(txt_codigoMotivo).sendKeys(Keys.ARROW_DOWN, Keys.ENTER);
                $(btn_agregar).click();

                // Esperar a que salga un mensaje de error (o no)
                try {
                    $(btn_aceptarInfo).should(appear).click();
                } catch (Throwable ignored) {
                    // No aparece error
                    agregado = true;
                    break;
                }
            } catch (Throwable ignored) { }
        }

        if (agregado) $$(tbl_motivos).shouldHave(CollectionCondition.size(cantidadActual + 1));
        else System.out.println("No se pudo agregar motivo en " + maxIntentos + " intentos");

    }

    public void agregarObservaciones() {

        System.out.println("Agregando observaciones");

        $(txt_observacionesInfraccion).setValue(faker.regexify("[A-Za-z &.,'\"/\\-_:;#()]{100,200}"));
    }

    public void clickGuardar() {

        System.out.println("Guardando");

        $(btn_guardar).click();
    }

    public void validarAltaExitosa() {

        System.out.println("Validando alta exitosa");

        Configuration.timeout = 10000;
        $(lbl_info).shouldHave(text("El alta de infracción se realizó exitosamente."));
    }
    public void validarActualizacionExitosa() {

        System.out.println("Validando actualización exitosa");

        Configuration.timeout = 5000;
        $(lbl_info).shouldHave(text("Actualización exitosa."));
    }

    /**
     * Elimina uno de los motivos de infracción aleatoriamente
     */
    public void eliminarMotivoAleatorio() {

        System.out.println("Eliminando motivo aleatorio");

        // Obtiene la cantidad de renglones
        ElementsCollection motivos = $$(cel_codigosMotivos);
        int cantidad = motivos.size();

        if (cantidad == 0) {
            return;
        }
        // Obtiene un indice aleatorio
        int randIdx = faker.number().numberBetween(0,cantidad-1);

        // Borra ese motivo
        driver.findElements(cel_codigosMotivos).get(randIdx).click();
        $$(tbl_motivos).get(randIdx).shouldHave(attributeMatching("class", "p_AFSelected.*"));
        $(btn_eliminar).shouldBe(enabled);
        $(btn_eliminar).hover().click();
        $(btn_eliminar).shouldBe(disabled);

        // Verificar que se haya borrado
        $$(tbl_motivos).shouldHave(CollectionCondition.size(cantidad-1));
    }

    /**
     * Recolecta todos los datos introducidos
     * @return un ArrayList con todos los datos en el orden en el que fueron recolectados
     */
    public ArrayList<String> recolectarDatosEnPantalla() {

        System.out.println("Recolectando datos en pantalla");

        ArrayList<String> datos = new ArrayList<>();
        datos.add(getAnyElementText($(lst_tipoInfraccion)));
        datos.add(getAnyElementText($(txt_fechaInfraccion)));
        datos.add(getAnyElementText($(txt_horaInfraccion)));
        datos.add(getAnyElementText($(txt_lugarInfraccion)));
        datos.add(getAnyElementText($(txt_agente)));
        datos.add(getAnyElementText($(lbl_agente)));
        datos.add(getAnyElementText($(lst_turno)));
        datos.add(getAnyElementText($(lst_municipio)));
        datos.add(getAnyElementText($(txt_clavePatrulla)));
        datos.add(getAnyElementText($(txt_placa)));
        datos.add(getAnyElementText($(txt_descripcionVehiculo)));
        datos.add(getAnyElementText($(txt_propietarioVehiculo)));
        datos.add(getAnyElementText($(txt_conductor)));
        datos.add(getAnyElementText($(txt_curp)));
        datos.add(getAnyElementText($(txt_domicilio)));
        String idConductor = getAnyElementText($(lst_idConductor));
        datos.add(idConductor);
        switch (idConductor) {
            case "INE":

            case "PASAPORTE":
                datos.add(getAnyElementText($(txt_numeroId)));
                break;

            case "LICENCIA":
                datos.add(getAnyElementText($(txt_numeroId)));
                datos.add(getAnyElementText($(lst_tipoLicencia)));
                break;

            default:
        }

        String tipoGarantia = getAnyElementText($(lst_tipoDeGarantia));
        datos.add(tipoGarantia);
        switch (tipoGarantia) {
            case "LICENCIA":
                datos.add(getAnyElementText($(txt_folioDeLaGarantia)));
                datos.add(getAnyElementText($(lst_forma)));
                datos.add(getAnyElementText($(txt_serie)));
                break;
            case "1 PLACA":
                break;
            case "2 PLACAS":
                break;
            case "TARJETA DE CIRCULACIÓN":
            case "OTRAS":
                datos.add(getAnyElementText($(txt_folioDeLaGarantia)));
                break;
            case "VEHÍCULO":
                datos.add(getAnyElementText($(txt_folioDeLaGarantia)));
                datos.add(getAnyElementText($(lst_pension)));
                break;

        }
        datos.add(getAnyElementText($(txt_observacionesGarantia)));
        datos.add(getAnyElementText($(txt_numeroDeParte)));

        $$(cel_codigosMotivos).forEach(motivo -> datos.add(motivo.getAttribute("textContent")));

        return datos;
    }

    /**
     * Corrobora que los datos en pantalla sean los mismos que los introducidos
     * @param datos los datos en el orden en el que fueron recolectados
     */
    public void corroborarDatos(ArrayList<String> datos) {

        System.out.println("Corroborando datos en pantalla");

        $(lst_tipoInfraccion).shouldHave(text(datos.remove(0)));
        $(txt_fechaInfraccion).shouldHave(value(datos.remove(0)));
        $(txt_horaInfraccion).shouldHave(value(datos.remove(0)));
        $(txt_lugarInfraccion).shouldHave(value(datos.remove(0)));
        $(txt_agente).shouldHave(value(datos.remove(0)));
        $(lbl_agente).shouldHave(text(datos.remove(0)));
        $(lst_turno).shouldHave(text(datos.remove(0)));
        $(lst_municipio).shouldHave(text(datos.remove(0)));
        $(txt_clavePatrulla).shouldHave(value(datos.remove(0)));
        $(txt_placa).shouldHave(value(datos.remove(0)));
        $(txt_descripcionVehiculo).shouldHave(value(datos.remove(0)));
        $(txt_propietarioVehiculo).shouldHave(value(datos.remove(0)));
        $(txt_conductor).shouldHave(value(datos.remove(0)));
        $(txt_curp).shouldHave(value(datos.remove(0)));
        $(txt_domicilio).shouldHave(value(datos.remove(0)));

        // Se comporta diferente para cada tipo de id de conductor
        String idConductor = datos.remove(0);
        $(lst_idConductor).shouldHave(text(idConductor));
        switch (idConductor) {
            case "INE":

            case "PASAPORTE":
                $(txt_numeroId).shouldHave(value(datos.remove(0)));
                break;

            case "LICENCIA":
                $(txt_numeroId).shouldHave(value(datos.remove(0)));
                $(lst_tipoLicencia).shouldHave(text(datos.remove(0)));
                break;

            default:
        }

        // Se comporta diferente para cada tipo de id de garantía
        String tipoGarantia = datos.remove(0);
        $(lst_tipoDeGarantia).shouldHave(text(tipoGarantia));
        switch (tipoGarantia) {
            case "LICENCIA":
                $(txt_folioDeLaGarantia).shouldHave(value(datos.remove(0)));
                $(lst_forma).shouldHave(text(datos.remove(0)));
                $(txt_serie).shouldHave(value(datos.remove(0)));
                break;
            case "1 PLACA":
                break;
            case "2 PLACAS":
                break;
            case "TARJETA DE CIRCULACIÓN":
            case "OTRAS":
                $(txt_folioDeLaGarantia).shouldHave(value(datos.remove(0)));
                break;
            case "VEHÍCULO":
                $(txt_folioDeLaGarantia).shouldHave(value(datos.remove(0)));
                $(lst_pension).shouldHave(text(datos.remove(0)));
                break;

        }
        $(txt_observacionesGarantia).shouldHave(value(datos.remove(0)));
        $(txt_numeroDeParte).shouldHave(value(datos.remove(0)));

        ElementsCollection motivos = $$(cel_codigosMotivos);
        motivos.forEach(motivo -> motivo.shouldHave(attribute("textContent", datos.remove(0))));
    }

    /**
     * Obtiene el texto de cualquier elemento
     * @param element el elemento a observar
     * @return el texto del elemento
     */
    public String getAnyElementText(SelenideElement element) {
        switch (element.getTagName()) {
            case "input":
            case "textarea":
                return element.getValue();
            case "select":
                return element.getSelectedText();
            default:
                return element.getText();
        }
    }

    /**
     * Verifica que cualquier elemento tenga cierto texto
     * @param text el texto a verificar
     * @return la condición
     */
    public static Condition anyElementText(String text) {
        return new Condition("has text") {
            @Override
            public boolean apply(Driver driver, WebElement webElement) {
                SelenideElement element = $(webElement);
                if (element.getTagName().equals("input")) {
                    return element.getValue().equals(text);
                } else if (element.getTagName().equals("select")) {
                    return element.getSelectedText().equals(text);
                } else {
                    return element.getText().equals(text);
                }
            }
        };
    }

    /**
     * Elige entre placas inventadas o existentes y activas
     * @param fusor el fusor para placas existentes
     * @param chance la probabilidad de que sea existente
     * @return la placa o serie
     */
    private String[] getCampoPlaca(Fusor fusor, double chance) {
        Fusor fusorDefault = new Fusor();
        Fusor fusor1;
        String[] output = new String[2];
        if (Math.random() < chance) {
            fusor1 = fusor;
            output[0] = "real";
        } else {
            fusor1 = fusorDefault;
            output[0] = "inventada";
        }

        if (Math.random() < 0.5) {
            output[1] = fusor1.vehiculo().matricula;
        } else {
            output[1] = fusor1.vehiculo().serie;
        }

        return output;
    }

    public void validarFolio(String folio) {
        $(lbl_folioBoleta).shouldHave(text(folio));
    }
}
