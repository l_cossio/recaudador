package recaudador.INIT1_infraccionesTransito.pageObjects;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import static com.codeborne.selenide.CollectionCondition.sizeGreaterThanOrEqual;
import static com.codeborne.selenide.Condition.disappear;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.title;

public class DetallesDeInfraccion {

    private final WebDriver driver;

    public DetallesDeInfraccion(WebDriver driver) {
        WebDriverRunner.setWebDriver(driver); // Para usar Selenide con el WebDriver previamente inicializado
        this.driver = driver;
        Configuration.timeout = 10000; // millis
        Configuration.screenshots = false;
    }

    private By img_home = new By.ById("pt1:i1");

    //// Detalles
    private By lbl_folioBoleta = new By.ById("pt1:r1:0:ot1");
    private By lbl_placa = new By.ById("pt1:r1:0:ot5");
    private By lbl_propietario = new By.ById("pt1:r1:0:ot8::content");
    private By lbl_conductor = new By.ById("pt1:r1:0:ot20");
    private By lbl_domicilio = new By.ById("pt1:r1:0:ot25::content");
    private By lbl_id = new By.ById("pt1:r1:0:ot26");
    private By lbl_folioGarantia = new By.ById("pt1:r1:0:ot27");
    private By lbl_fecha = new By.ById("pt1:r1:0:ot2");

    private By div_motivos = new By.ById("pt1:r1:0:t4::db");
    private By lbl_totalAdeudo = new By.ByCssSelector("span.xdi:nth-child(18)");

    public void verificarTodosLosCampos(String[] row) {
        System.out.println("Verificando campos en la pantalla de detalles");
        $(lbl_folioBoleta).shouldHave(text(row[0]));
        $(lbl_placa).shouldHave(text(row[1]));
        $(lbl_propietario).shouldHave(text(row[2]));
        $(lbl_conductor).shouldHave(text(row[3]));
        $(lbl_domicilio).shouldHave(text(row[4]));
        $(lbl_id).shouldHave(text(row[5]));
//        $(lbl_folioGarantia).shouldHave(text(row[6]));
        $(lbl_fecha).shouldHave(text(row[7]));
    }

    public void verificarCantidadDeMotivos(int minimoInclusivo) {
        System.out.println("Verificando que la cantidad de motivos sea de por lo menos " + minimoInclusivo);
        $(div_motivos).findAll("table > tbody > tr").shouldHave(sizeGreaterThanOrEqual(minimoInclusivo));
    }

    public void verificarAdeudoMayorA(double minimo) throws ParseException {
        System.out.println("Verificando que el adeudo sea mayor a " + minimo);
        String adeudo = $(lbl_totalAdeudo).getText();
        NumberFormat nf = NumberFormat.getInstance(Locale.US);
        double total = (double) nf.parse(adeudo.substring(9));
        Assert.assertTrue(total > minimo);
    }

    public void clickHome() {
        $(img_home).click();
        $(div_motivos).should(disappear);
        Assert.assertEquals(title(), "Recaudador");
    }
}
