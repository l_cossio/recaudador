package recaudador.INIT1_infraccionesTransito.pageObjects;

import recaudador.Base;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class DatosEntradaInfracciones extends Base {

    public String foliosNoExistentes;
    public String foliosExistentes;
    public String infracciones;
    public String cliente;
    public String consultasInfracciones;
    /**
     * Carga de variables para la prueba de Mantenimiento de Infracciones
     *
     * @throws IOException when file not found
     */
    public void cargaVariablesInfracciones() throws IOException {

        // Preparar archivo
        Properties prop = new Properties();
        FileInputStream fis = new FileInputStream
                ("src\\test\\java\\recaudador\\INIT1_infraccionesTransito\\dataBank\\Infracciones.properties");

        prop.load(fis);

        // Cargar variables
        foliosExistentes = prop.getProperty("foliosExistentes");
        foliosNoExistentes = prop.getProperty("foliosNoExistentes");
        infracciones = prop.getProperty("infracciones");
        cliente = getSystemProperty("env", "TAB");
    }

    public void cargaVariablesConsultaInfracciones() throws IOException {

        // Preparar archivo
        Properties prop = new Properties();
        FileInputStream fis = new FileInputStream
                ("src\\test\\java\\recaudador\\INIT1_infraccionesTransito\\dataBank\\Infracciones.properties");

        prop.load(fis);

        // Cargar variables
        consultasInfracciones = prop.getProperty("consultasInfracciones");

    }
}