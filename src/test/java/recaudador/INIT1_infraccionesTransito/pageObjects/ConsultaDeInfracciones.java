package recaudador.INIT1_infraccionesTransito.pageObjects;

import com.codeborne.selenide.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import recaudador.Pagina;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

public class ConsultaDeInfracciones extends Pagina {

    private final WebDriver driver;

    public ConsultaDeInfracciones(WebDriver driver) {
        WebDriverRunner.setWebDriver(driver); // Para usar Selenide con el WebDriver previamente inicializado
        this.driver = driver;
        Configuration.timeout = 15000; // millis
        Configuration.screenshots = false;
    }

    //// Acceso
    private By lnk_accesoConsulta = new By.ByLinkText("Consulta de infracciones de tránsito");


    //// Consulta
    private By lst_tipoInfraccion = new By.ById("pt1:smc1::content");
    private By txt_folioBoleta = new By.ById("pt1:it1::content");
    private By txt_placa = new By.ById("pt1:it2::content");
    private By txt_propietario = new By.ById("pt1:it3::content");
    private By txt_conductor = new By.ById("pt1:it4::content");
    private By txt_domicilio = new By.ById("pt1:it5::content");
    private By txt_identificacion = new By.ById("pt1:it6::content");
    private By txt_folioGarantia = new By.ById("pt1:it7::content");
    private By txt_fechaInfraccion = new By.ById("pt1:id1::content");

    private By div_resultados = new By.ById("pt1:t1::db");

    // Botones
    private By btn_buscar = new By.ById("pt1:cb1");
    private By btn_limpiar = new By.ById("pt1:cb2");
    private By btn_siguiente = new By.ById("pt1:cb7");

    // Errores
    private By img_mensajesError = new By.ByXPath("//*[contains(@title,'Error')]");
    private By img_mensajesInformacion = new By.ByXPath("//*[contains(@title,'Información')]");

    //// Detalles
    private By div_motivos = new By.ById("pt1:r1:0:t4::db");


    /**
     * Verifica que la tabla haya respondido con datos o con falta de datos
     * @return la condición
     */
    public static Condition showingResults(String whatKind) {
        return new Condition("showing some result") {
            @Override
            public boolean apply(Driver driver, WebElement webElement) {

                WebElement tbl_resultados = webElement.findElement(By.xpath("table"));
                int rowCount;
                switch (whatKind) {
                    case "any":
                        rowCount = Integer.parseInt(tbl_resultados.getAttribute("_rowcount"));
                        return (webElement.getText().equals("No se encontró ningún resultado con los datos ingresados.")
                                ||  rowCount > 0 || rowCount == -1);

                    case "some":
                        rowCount = Integer.parseInt(tbl_resultados.getAttribute("_rowcount"));
                        return Integer.parseInt(tbl_resultados.getAttribute("_rowcount")) > 0 || rowCount == -1;

                    case "none":
                        return webElement.getText().equals("No se encontró ningún resultado con los datos ingresados.");

                    default:
                        return false;
                }
            }
        };
    }

    private Condition showingAnyResults = showingResults("any");
    private Condition showingResults = showingResults("some");
    private Condition showingNoResults = showingResults("none");

    /**
     * Verifica que se desplieguen datos sin errores
     * @param condition La condición a revisar (showingResults, showingNoResults o showingAnyResults)
     */
    public void verificarConsulta(Condition condition){
        $(div_resultados).shouldBe(condition);
        $$(img_mensajesError).filterBy(Condition.visible).shouldHaveSize(0);
        try {
            $$(img_mensajesInformacion).filterBy(Condition.visible).shouldHaveSize(0);
        } catch (Throwable e) {
            System.out.println(e);
            System.out.println("Mensaje de información desplegado");
        }
    }

    private void consulta(String input, By field, boolean debeExistir) {
        consulta(new String[]{input}, new By[]{field}, debeExistir);
    }

    /**
     * Realiza una consulta, asegurar que todos los campos se encuentren disponibles antes de llamar al método
     * @param inputs los valores a ingresar. El arreglo debe ser paralelo con fields
     * @param fields los campos a llenar, en caso de ser select, se ignora el input correspondiente y se hace aleatorio.
     *               El arreglo debe ser paralelo con inputs.
     * @param debeExistir true si debería desplegar resultados
     */
    private void consulta(String[] inputs, By[] fields, boolean debeExistir) {
        if (inputs.length != fields.length) {
            throw new ArrayIndexOutOfBoundsException("Las listas de inputs y fields son de diferente longitud");
        }
        for(int i=0; i<inputs.length; i++){
            String input = inputs[i];
            By field = fields[i];
            if ($(field).getTagName().equals("select")) {
                seleccionarOpcionAleatoria(field);
            } else {
                $(field).setValue(input);
            }
        }
        $(btn_buscar).click();
        if (debeExistir) {
            verificarConsulta(showingResults);
        } else {
            verificarConsulta(showingAnyResults);
        }
    }

    public void clickLimpiar() {
        $(btn_limpiar).click();
        $(div_resultados).shouldHave(text("No hay registros que mostrar."));
    }

    public void clickConsultaInfracciones() {
        $(lnk_accesoConsulta).click();
    }

    public void consultarPorTipoInfraccionAleatoria() {
        System.out.println("Consultando por tipo de infracción aleatoria");
        consulta("", lst_tipoInfraccion, true);
    }

    public void consultaPorFolio(String folio) {
        System.out.println("Consultando por folio");
        consulta(folio, txt_folioBoleta, true);
    }

    public void consultaPorPlaca(String placa) {
        System.out.println("Consultando por placa");
        consulta(placa, txt_placa, true);
    }


    public void consultaPorPropietario(String propietario) {
        System.out.println("Consultando por propietario");
        consulta(propietario, txt_propietario, true);
    }

    public void consultaPorConductor(String conductor) {
        System.out.println("Consultando por conductor");
        consulta(conductor, txt_conductor, true);
    }

    public void consultaPorDomicilio(String domicilio) {
        System.out.println("Consultando por domicilio");
        consulta(domicilio, txt_domicilio, true);
    }

    public void consultaPorId(String id) {
        System.out.println("Consultando por numero de identificación");
        consulta(id, txt_identificacion, true);
    }

    public void consultaPorGarantia(String folioGarantia) {
        System.out.println("Consultando por folio de la garantía");
        consulta(folioGarantia, txt_folioGarantia, true);
    }

    public void consultaPorFecha(String fecha) {
        System.out.println("Consultando por fecha de infracción");
        consulta(fecha, txt_fechaInfraccion, true);
    }

    public void consultaPorTodosLosCampos(String[] row) {
        System.out.println("Consultando por todos los campos");
        consulta(row, new By[]{txt_folioBoleta, txt_placa, txt_propietario,
                txt_conductor, txt_domicilio, txt_identificacion/*, txt_fechaInfraccion*/},
                true);
    }

    public void clickSiguiente() {
        System.out.println("Dando click a siguiente");
        $(btn_siguiente).click();
        $(div_motivos).should(appear);
    }


}
