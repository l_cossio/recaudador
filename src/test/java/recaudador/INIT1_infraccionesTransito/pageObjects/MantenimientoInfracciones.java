package recaudador.INIT1_infraccionesTransito.pageObjects;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;

public class MantenimientoInfracciones {

    private final WebDriver driver;

    public MantenimientoInfracciones(WebDriver driver) {
        WebDriverRunner.setWebDriver(driver);
        this.driver = driver;
        Configuration.timeout = 2000; // millis
        Configuration.screenshots = true;
    }

    // Acceso
    By lnk_mantenimientoInfracciones = new By.ByLinkText("Mantenimiento de infracciones de tránsito");


    By lst_tipoInfraccion = new By.ById("pt1:soc7::content");
    By txt_folioBoleta = new By.ById("pt1:it8::content");
    By btn_buscar = new By.ById("pt1:cb5");

    // METODOS

    public void clicklnkMantenimiento() {
        System.out.println("Ingresando a mantenimiento de infracción");
        $(lnk_mantenimientoInfracciones).click();
        $(lnk_mantenimientoInfracciones).should(disappear);
        Assert.assertEquals(title(),"Mantenimiento de infracción");
    }

    public void ingresarFolio(String folio) {
        System.out.println("Buscando folio");
        $(lst_tipoInfraccion).selectOption("TRANSITO");
        $(txt_folioBoleta).setValue(folio);
        $(btn_buscar).click();
    }
}
