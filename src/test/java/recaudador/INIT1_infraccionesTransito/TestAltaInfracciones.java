package recaudador.INIT1_infraccionesTransito;

import com.github.javafaker.Faker;
import fusor.Fusor;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;
import recaudador.INIT1_infraccionesTransito.pageObjects.DatosEntradaInfracciones;
import recaudador.INIT1_infraccionesTransito.pageObjects.Infracciones;
import recaudador.INIT1_infraccionesTransito.pageObjects.MantenimientoInfracciones;
import recaudador.INLO1_login.pageObjects.Login;
import recaudador.INLO1_login.pageObjects.Menu;

import java.io.IOException;

@Test(groups = {"TAB"})
public class TestAltaInfracciones extends DatosEntradaInfracciones {

    WebDriver driver;
    Fusor fusor;
    Faker faker;
    Infracciones objInfracciones;
    MantenimientoInfracciones objMantenimiento;
    Login objLogin;
    Menu objMenu;


    @BeforeTest
    public void setup() throws IOException {

        cargaVariablesInfracciones();
        faker = new Faker();
        fusor = new Fusor(cliente);
        driver = initializerDriver();
        driver.manage().window().maximize();
        objLogin = new Login(driver);
        objMenu = new Menu(driver);
        objInfracciones = new Infracciones(driver);
        objMantenimiento = new MantenimientoInfracciones(driver);

        driver.get(url);

        objLogin.login(userName, password);
    }

    @BeforeMethod
    public void ingresarAInfracciones() {
        objMenu.clickInfraccionesTransito();
    }

    /**
     * Nombre: Alta de Infracción de Tránsito.
     * Descripción: Dar de alta una infracción.
     * Objetivos de la prueba:
     *   - Capturar todos los datos de la infracción
     *   - Ingresar una placa de un vehiculo activo
     *   - Ingresar los datos del conductor
     *   - Ingresar los datos de garantía
     *   - Ingresar varios motivos
     *   - Eliminar un motivo aleatorio
     *   - Guardar la infracción
     * Autor: Luis Cossío Ramírez.
     * Fecha de creación: 29-01-2021.
     * Modificado por: N/A.
     * Fecha de modificación: N/A.
     * @throws InterruptedException por los waits
     */
    @Test
    public void altaDeInfraccion() throws IOException {

        objInfracciones.clickAltaInfracciones();

        String folio = faker.regexify("PRUEBAS[A-Z0-9]{13}");

        // Capturar todos los datos de la infracción
        objInfracciones.llenarDatosDeInfraccion(folio, fusor);

        // Ingresar una placa de un vehiculo activo
        objInfracciones.llenarDatosDelVehiculo(fusor);

        // Ingresar los datos del conductor
        objInfracciones.llenarDatosDelConductor(fusor);

        // Ingresar los datos de garantía
        objInfracciones.llenarDatosDeLaGarantia(fusor);

        // Ingresar varios motivos
        objInfracciones.agregarMotivoAleatorio(25);
        objInfracciones.agregarMotivoAleatorio(25);
        objInfracciones.agregarMotivoAleatorio(25);
        objInfracciones.agregarObservaciones();

        // Eliminar un motivo aleatorio
        objInfracciones.eliminarMotivoAleatorio();

        // Guardar la infracción
        objInfracciones.clickGuardar();
        objInfracciones.validarAltaExitosa();
    }


    @AfterMethod
    public void regresarAlInicio() {
        objInfracciones.clickHome();
    }

    @AfterTest
    public void cerrar() {
        driver.quit();
    }
}
